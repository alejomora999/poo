/*
*NOMBRE DEL PROGRAMA : MAYOR Y MENOR
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 14 DE OCT DE 2016
 */
package PUNTO1;

import java.util.Scanner;

public class MayorYMenor {
	// CREO ATRIBUTOS
	private int mayor = -999999;
	private int menor = 999999;
	int cMayor = 0;
	int cMenor = 0;
	int posicion = 0;
	int posicion1 = 0;

	private Scanner leer;

	// CREO EL CONSTRUCTOR
	public MayorYMenor(int vect[], int limite) {

	}

	public void Calcular(int vec[], int limite) {

		// CREO UN FOR DONDE LLENARE DESDE LA POSICION 0 HASTA EL LIMITE-1
		for (int i = 0; i < limite; i++) {
			leer = new Scanner(System.in);
			System.out.print("X[" + (i) + "]=");
			vec[i] = leer.nextInt();
			// COMPARO SI EL DATO INGRESADO ES MAYOR
			if (vec[i] > mayor) {
				mayor = vec[i];
				cMayor = 1;

			} else if (vec[i] == mayor) {
				cMayor = cMayor + 1;

				// COMPARO SI EL DATO INGRESADO ES MENOR
			}
			if (vec[i] < menor) {
				menor = vec[i];
				cMenor = 1;

			} else if (vec[i] == menor) {
				cMenor = cMenor + 1;
			}

		}
		// IMPRIMO TANTO EL N�MERO MAYOR COMO EL MENOR
		System.out.println("El n�mero mayor es: " + mayor + " y aparece " + cMayor + " vez (veces)  ");
		System.out.println("El n�mero menor es: " + menor + " y aparece " + cMenor + " vez (veces)  ");
		// CREO UN SEGUNDO VECTOR DONDE COPIO LA INFORMACION DEL PRIMERO PARA
		// SABER LAS POSICIONES D ELAS OCURRENCIAS
		int vec2[] = new int[limite];
		for (int i = 0; i < limite; i++) {
			vec2[i] = vec[i];
			if (mayor == vec2[i]) {
				posicion = i;
				// IMPRIMO LA POSICION DE LA OCURRECNIA PARA EL N�MERO MAYOR
				System.out.println("El n�mero mayor segun ocurrencias  aparece en la posicion: " + posicion);
			}

			if (menor == vec2[i]) {
				posicion1 = i;
				// IMPRIMO LA POSICION DE LA OCURRECNIA PARA EL N�MERO MENOR
				System.out.println("El n�mero menor segun ocurrencias aparece  en la posicion: " + posicion1);
			}
		}

	}

	// GETTERS AND SETTERS
	public int getMayor() {
		return mayor;
	}

	public void setMayor(int mayor) {
		this.mayor = mayor;
	}

	public int getMenor() {
		return menor;
	}

	public void setMenor(int menor) {
		this.menor = menor;
	}

	public int getcMayor() {
		return cMayor;
	}

	public void setcMayor(int cMayor) {
		this.cMayor = cMayor;
	}

	public int getcMenor() {
		return cMenor;
	}

	public void setcMenor(int cMenor) {
		this.cMenor = cMenor;
	}

	public int getPosicion() {
		return posicion;
	}

	public void setPosicion(int posicion) {
		this.posicion = posicion;
	}

	public int getPosicion1() {
		return posicion1;
	}

	public void setPosicion1(int posicion1) {
		this.posicion1 = posicion1;
	}

	public void setLeer(Scanner leer) {
		this.leer = leer;
	}

}
