/*
 *NOMBRE DEL PROGRAMA : MAYOR Y MENOR
 *AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 14 DE OCT DE 2016
 */
package PUNTO1;

import java.util.Scanner;

public class MayorYMenorTest {
	private static Scanner leer;

	public static void main(String arg[]) {
		int reiniciar = 0;
		int limite = 0;

		do {
			// CREO UN ATRIBUTO DE TIPO INT
			leer = new Scanner(System.in);
			System.out.println("le dire el n�mero mayor y el menor ");
			System.out.println("Cuantos n�meros desea ingresar ");
			limite = leer.nextInt();
			int VectorX[] = new int[limite + 1];
			MayorYMenor lista = new MayorYMenor(VectorX, limite);
			lista.Calcular(VectorX, limite);
			System.out.print("\n ");
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = leer.nextInt();
			// El siguiente While, acompa�ado de un If me permite saber si la opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = leer.nextInt();
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
		System.out.println("FIN DEL PROGRAMA");
	}

}
