/*
 *NOMBRE DEL PROGRAMA : PRODUCTO DE MATRICES
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 14 DE OCT DE 2016
 */
package PUNTO4;

import java.util.Scanner;

public class ProductoMatrices {
	private Scanner leer;
	// CREO LOS ATREBUTOS DE TIPO PRIVADO

	// CREO UN METODO DONDE LLENO LAS MATRICES POR TECLADO
	public void llenarMatriz(int M1[][], int filasM1, int columnasM1) {
		leer = new Scanner(System.in);
		for (int i = 0; i < filasM1; ++i) {
			for (int j = 0; j < columnasM1; ++j) {
				System.out.println(" Elemento " + i + " " + j);
				M1[i][j] = leer.nextInt();
			}
		}
	}

	// CREO UN METODO DONDE MUESTRO LAS MATRICES
	public void mostrarMatriz(int M1[][], int filasM1, int columnasM1) {
		for (int i = 0; i < filasM1; i++) {
			for (int j = 0; j < columnasM1; j++) {
				System.out.print(M1[i][j] + "  ");
			}
			System.out.println();
		}
	}

	// CREO UN METODO DONDE RECORRO TRES FOR, CON EL FIN DE CALCULAR EL PRODUCTO
	// DE LAS MATRICES
	public void llenarmatriztres(int M3[][], int M1[][], int M2[][], int filasM1, int columnasM2, int filasM2) {
		for (int i = 0; i < filasM1; i++) {
			for (int j = 0; j < columnasM2; j++) {
				for (int k = 0; k < filasM2; k++) {
					M3[i][j] += M1[i][k] * M2[k][j];
				}
			}
		}
	}

}
