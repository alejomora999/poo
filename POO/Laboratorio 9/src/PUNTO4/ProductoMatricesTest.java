/*
 *NOMBRE DEL PROGRAMA : PRODUCTO DE MATRICES
 * AUTOR:NICOLAS MENESES GUERRERO
 *FECHA: 14 DE OCT DE 2016
 */
package PUNTO4;

import java.util.Scanner;

public class ProductoMatricesTest {

	private static Scanner leer;

	public static void main(String arg[]) {
		// Defino la variable reiniciar, sera la que determine si quiero repetir
		// el programa de nuevo o no
		int reiniciar = 0;
		// Creo atributos donde almaceno los datos ingresado por el usuario
		int filasM1 = 0;
		int columnasM1 = 0;
		int filasM2 = 0;
		int columnasM2 = 0;
		do {
			leer = new Scanner(System.in);
			System.out.println("este programa calcula el producto de dos matrices :");
			// PIDO LOS DATOS QUE NECESITO
			System.out.println("Ingrese el n�mero de filas de la primera matriz:");
			filasM1 = leer.nextInt();
			while (filasM1 <= 0) {
				System.out.println("Ingrese un n�mero de filas valido valido para la primera martiz:");
				filasM1 = leer.nextInt();
			}
			System.out.println("Ingrese el n�mero de columnas de la primera martiz:");
			columnasM1 = leer.nextInt();
			while (columnasM1 <= 0) {
				System.out.println("Ingrese un n�mero de columnas valido para la primera martiz:");
				columnasM1 = leer.nextInt();
			}
			System.out.println("Ingrese el n�mero de filas de la segunda matriz:");
			filasM2 = leer.nextInt();
			while (filasM2 <= 0) {
				System.out.println("Ingrese un n�mero de filas valido para la segunda martiz:");
				filasM2 = leer.nextInt();
			}
			System.out.println("Ingrese el n�mero de columnas de la segunda martiz:");
			columnasM2 = leer.nextInt();
			while (columnasM2 <= 0) {
				System.out.println("Ingrese un n�mero de columnas valido para la segunda matriz :");
				columnasM2 = leer.nextInt();
			}
			// CREO MI OBJETO DE LA CLASE VALIDACION
			Validacion validacion = new Validacion();
			// CREO MI OBJETO DE LA CLASE PRODUCTO MATRICES
			ProductoMatrices matriz = new ProductoMatrices();
			// CREO LAS TRES MATRICES PARA EL EJERCICIO
			int M1[][] = new int[filasM1][columnasM1];
			int M2[][] = new int[filasM2][columnasM2];
			int M3[][] = new int[filasM1][columnasM2];
			// EN ESTE IF LLAMO LA CLASE VALIDACION
			if (validacion.validar(columnasM1, filasM2)) {
				// SI LA VALIDACION ES VERDADERA LLAMO A LA CLASE PRODUCTO
				// MATRICES A TRAVEZ DE MI OBJETO
				System.out.println("Va a llenar la primera Matriz");
				matriz.llenarMatriz(M1, filasM1, columnasM1);
				System.out.println("Va a llenar la segunda Matriz");
				matriz.llenarMatriz(M2, filasM2, columnasM2);
				System.out.println("************ A*************");
				matriz.mostrarMatriz(M1, filasM1, columnasM1);
				System.out.println("************ B*************");
				matriz.mostrarMatriz(M2, filasM2, columnasM2);
				matriz.llenarmatriztres(M3, M1, M2, filasM1, columnasM2, filasM2);
				// MUESTRO MI MATRIZ RESULTADO
				System.out.println("************ C*************");
				matriz.mostrarMatriz(M3, filasM1, columnasM2);
				System.out.println("La Matriz resultado es de orden " + filasM1 + "x" + columnasM2);
			} else {
				System.out.println(
						"El n�mero de columnas de la primera matriz no coincide con el n�mero de filas de la segunda.");
				System.out.println("No se puede realizar el producto de Matrices.");
			}
			System.out.print("\n ");
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = leer.nextInt();
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = leer.nextInt();
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
		System.out.println("FIN DEL PROGRAMA");

	}
}
