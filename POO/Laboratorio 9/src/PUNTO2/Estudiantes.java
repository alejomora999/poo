package PUNTO2;

/*
 *NOMBRE DEL PROGRAMA : ESTUDIANTES
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 14 DE OCT DE 2016
 */
public class Estudiantes {
	// CREO ATRIBUTOS DE TIPO PRIVATE
	private String nombre;
	private String apellido;
	private double nota1;
	private double nota2;
	private double nota_final;

	// CONSTRUCTOR POR DEFECTO
	public Estudiantes() {
		nombre = (" ");
		apellido = (" ");
		nota1 = 0;
		nota2 = 0;
		nota_final = 0;
	}

	// SOBRE CARGA DE CONSTRUCTORES
	public Estudiantes(String n, String a, double n1, double n2, double nf) {
		nombre = n;
		apellido = a;
		nota1 = n1;
		nota2 = n2;
		nota_final = nf;
	}

	// CREO UN METODO DONDE CALCULARE LA NOTA DEFINITIVA DE CADA ALUMNO
	public double definitiva(double nota1, double nota2, double nota_final) {
		double definitiva = 0;
		this.nota1 = (nota1 * 0.3);
		this.nota2 = (nota2 * 0.3);
		this.nota_final = (nota_final * 0.4);
		definitiva = (this.nota1 + this.nota2 + this.nota_final);
		return definitiva;
	}

	// CREO GETTERS AND SETTERS

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public double getNota1() {
		return nota1;
	}

	public void setNota1(double nota1) {
		this.nota1 = nota1;
	}

	public double getNota2() {
		return nota2;
	}

	public void setNota2(double nota2) {
		this.nota2 = nota2;
	}

	public double getNota_final() {
		return nota_final;
	}

	public void setNota_final(double nota_final) {
		this.nota_final = nota_final;
	}

}
