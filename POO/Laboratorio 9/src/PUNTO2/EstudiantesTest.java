/*
 *NOMBRE DEL PROGRAMA : ESTUDIANTES
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 14 DE OCT DE 2016
 */
package PUNTO2;

import java.util.Scanner;

public class EstudiantesTest {
	private static Scanner leer;

	public static void main(String arg[]) {
		int reiniciar = 0;
		String nombre = (" ");
		String apellido = (" ");
		double nota1 = 0;
		double nota2 = 0;
		double nota_final = 0;
		double promedio_grupo = 0;
		do {
			System.out.println("se tomanan nombres y apellidos de 3 estudiantes");
			// CREO UN ATRIBUTO DE TIPO INT
			leer = new Scanner(System.in);
			System.out.println("tomare los datos de los estudiantes :");
			Estudiantes alumnos[] = new Estudiantes[3];
			Estudiantes Alumno = new Estudiantes();
			// CREO UN FOR DONDE PIDO LOS DATOS
			for (int i = 0; i < alumnos.length; i++) {

				System.out.println("Ingrese el Nombre del estudiante n�mero " + (i + 1));
				nombre = leer.nextLine();

				System.out.println("Ingrese el Apellido del estudiante  ");
				apellido = leer.nextLine();
				System.out.println("Ingrese la primera nota ");
				nota1 = leer.nextDouble();
				while (nota1 < 0 || nota1 > 5) {
					System.out.println("Ingrese una nota valida ");
					nota1 = leer.nextDouble();
				}
				System.out.println("Ingrese la segunda nota ");
				nota2 = leer.nextDouble();
				while (nota2 < 0 || nota2 > 5) {
					System.out.println("Ingrese una nota valida ");
					nota2 = leer.nextDouble();
				}
				System.out.println("Ingrese la  nota final ");
				nota_final = leer.nextDouble();
				while (nota_final < 0 || nota_final > 5) {
					System.out.println("Ingrese una nota valida ");
					nota_final = leer.nextDouble();
				}
				leer.nextLine();
				// LOS DATOS CREADOS VAN AL CONSTRUCTOR DE LA CLASE ESTUDIANTES
				// POR MEDIO DEL VECTOR
				alumnos[i] = new Estudiantes(nombre, apellido, nota1, nota2, nota_final);

			}
			// IMPRIMO LOS ESTUDIANTES CON SUS RESPECTIVAS NOTAS
			System.out.println("NOMBRE-APELIIDO-NOTA1-NOTA2-EXAMEN FINAL-DEFINITIVA");
			System.out.println(" ");
			for (int i = 0; i < alumnos.length; i++) {
				System.out.println(alumnos[i].getNombre() + "   " + alumnos[i].getApellido() + "   "
						+ alumnos[i].getNota1() + "   " + alumnos[i].getNota2() + "   " + alumnos[i].getNota_final()
						+ "   "
						+ Alumno.definitiva(alumnos[i].getNota1(), alumnos[i].getNota2(), alumnos[i].getNota_final()));

				promedio_grupo = promedio_grupo
						+ Alumno.definitiva(alumnos[i].getNota1(), alumnos[i].getNota2(), alumnos[i].getNota_final());
			}
			System.out.println("El promedio del grupo es de: " + (promedio_grupo / 3));

			System.out.print("\n ");
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = leer.nextInt();
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = leer.nextInt();
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
		System.out.println("FIN DEL PROGRAMA");

	}

}
