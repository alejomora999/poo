/*
 *NOMBRE DEL PROGRAMA : MESES
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 14 DE OCT DE 2016
 */
package PUNTO3;

import java.util.Scanner;

public class MesesTest {
	private static Scanner leer;

	public static void main(String arg[]) {
		// Defino la variable reiniciar, sera la que determine si quiero repetir
		// el programa de nuevo o no
		int reiniciar = 0;
		// Creo atributos donde almaceno los datos ingresado por el usuario
		double mes = 0;
		do {

			System.out.println("tomare el n�mero del mes deseado :");
			// RECIBO EL N�MERO
			leer = new Scanner(System.in);
			mes = leer.nextDouble();
			while (mes < 1 || mes > 12) {
				System.out.println("Ingrese un n�mero valido :");
				mes = leer.nextDouble();
			}
			if (mes == 2) {
				System.out.println("Su mes es febrero y tiene 28 d�as, 29 si el a�o es bisciesto");
			}
			// CREO LOS VECTORES QUE VOY A MANDAR DE TIPO INT Y STRING
			int vector[] = new int[12];
			String vec[] = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre",
					"Octubre", "Noviembre", "Diciembre" };
			// CREO MI OBJETO
			Meses Mes = new Meses();
			// LLAMO EL M�TODO CON MI OBJETO
			Mes.decirmes(vec, vector, mes);
			System.out.print("\n ");
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = leer.nextInt();
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = leer.nextInt();
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
		System.out.println("FIN DEL PROGRAMA");

	}

}
