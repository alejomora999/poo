/*
 * NOMBRE PROGRAMA: CONVERSION DE MONEDA
 * AUTOR: NICOLAS MENESES GUERRERO
 * FECHA: AGO 24 DE 2016
 */
package PUNTO1;

public class ConversionDeMoneda {
	public static void main(String arg[]){
	    // Valor del Euro hoy = $ 3.300,87977 COP
        // Valor del Euro hoy = $ 1,1256 USD
	    // Declarar variable para las tres cantidades.
	    double dolar=1.1256;
	    double cop=3300.87977;
	    // Declare la cantidad de euros a convertir
	    int cantidad_euro=10;
	    // Declaro variables de resultado de conversion 
	    double resultado_dolar=0;
	    double resultado_cop=0;
	    // Realiza operaciones para generar la conversion de moneda
	    resultado_dolar=(dolar*cantidad_euro);
	    resultado_cop=(cop*cantidad_euro);
	    //Impresion de pantalla con la cantidad de euros como el igual en dolares y pesos colombianos
	    System.out.println("La cantidad de euros a convertir es 10");
	    System.out.println("euros " + cantidad_euro);
	    System.out.println("euros " + resultado_dolar);
	    System.out.println("euros " + resultado_cop);
	}
}