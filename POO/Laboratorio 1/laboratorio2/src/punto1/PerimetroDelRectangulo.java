/*
 * NOMBRE PROGRAMA: PERIMETRO DEL RECTANGULO
 * AUTOR: NICOLAS MENESES GUERRERO
 * FECHA: AGO 26 DE 2016
 */
package punto1;
import java.io.*;
public class PerimetroDelRectangulo {
	public static void main(String arg[]) throws IOException{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		double largo;
		double alto;
		double resultado=0;
		System.out.println("Ingresa el numero del largo: ");
		largo=Double.parseDouble(in.readLine());
		System.out.println("Ingresa el numero del alto: ");
		alto=Double.parseDouble(in.readLine());
		//System.out.println(largo);
		if (largo<0|alto<0){
			System.out.println("Los datos no pueden ser negativos");
		}else{
			resultado=((2*largo)+(2*alto));
			System.out.println ("El perimetro es: "+ resultado + " unidades");
		}
	}

}
// poner 2 ifs 1 para el largo y otro para el ancho