/*
 * NOMBRE PROGRAMA: NUMERO PRIMO
 * AUTOR: NICOLAS MENESES GUERRERO
 * FECHA: AGO 30 2016
 */

package punto5;
import java.io.*;
public class NumeroPrimo {
	public static void main (String arg[]) throws IOException {
		BufferedReader in= new BufferedReader(new InputStreamReader( System.in));
		double n;
		double a=0;
		System.out.println("Digite el numero para saber si es primo o no ");
		n=Double.parseDouble(in.readLine());
	    //crea una variable para saber si el numero es primo o no
		int parte_entera =(int)(n);
		if (parte_entera==n){
			//con el for se mirara el comportamiento de la variable para ver como se comporta
			for (int i=1; i<=n+1;i++){
				if (n%i==0){
					a++;
				}
			}
		//con este if reviso si la variable cuenta primo o no
		if (a!=2){
			System.out.println("El numero "+ n + " No es primo");
		}else {
			System.out.println("El numero " + n + " Si es primo");
		}
	} else {
		System.out.println(n +"Digite por favor solo numeros enteros");
   }
  }
}

