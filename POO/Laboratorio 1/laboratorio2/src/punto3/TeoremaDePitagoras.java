/*
 * NOMBRE PROGRAMA: PERIMETRO DEL RECTANGULO
 * AUTOR: NICOLAS MENESES GUERRERO
 * FECHA: AGO 26 DE 2016
 */
package punto3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TeoremaDePitagoras {
	public static void main(String arg[]) throws IOException{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		double catetoA;
		double catetoO;
		double hipotenusa=0;
		System.out.println("Ingresa el numero del cateto adyacente: ");
		catetoA=Double.parseDouble(in.readLine());
		System.out.println("Ingresa el numero del cateto opuesto : ");
		catetoO=Double.parseDouble(in.readLine());
		if (catetoA<0){
			System.out.println("El catateto adyacente no puede ser negativo : ");
		}else if(catetoO<0){
			System.out.println("El cateto opuesto no puede ser negativo: ");
		} else {
			hipotenusa = Math.sqrt((Math.pow(catetoA, 2))+(Math.pow(catetoO, 2)));
			System.out.println("El resultado de la hipotenusa es : "+ hipotenusa);
		}
	}

}
