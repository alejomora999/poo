/*
 * NOMBRE PROGRAMA: PERIMETRO DEL RECTANGULO
 * AUTOR: NICOLAS MENESES GUERRERO
 * FECHA: AGO 26 DE 2016
 */
package punto2;
import java.io.*;
public class AreaDelCirculo {
	public static void main(String arg[]) throws IOException{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		double radio;
		double resultado=0;
		System.out.println("Ingresa el numero del radio: ");
		radio=Double.parseDouble(in.readLine());
		if (radio<0){
			System.out.println("Los datos no pueden ser negativos");
		}else{
			resultado= (Math.PI*(Math.pow(radio, 2)));
			System.out.println ("El area del circulo  es: "+ resultado + " unidades");
		}
	}
		

}

//librerias averiguar
//java.io.BufferReader
//java.io.IOException
//java.io:InputStreamReader