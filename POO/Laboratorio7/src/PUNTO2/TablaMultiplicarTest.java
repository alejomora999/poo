/*
 *NOMBRE DEL PROGRAMA : TABLA 
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 5 OCT DE 2016
 */
package PUNTO2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TablaMultiplicarTest {
	public static void main(String arg[]) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int reiniciar = 1;
		int multiplicando = 0;
		while (reiniciar != 0) {
			System.out.print("Digite hasta qu� n�mero desea que vaya la tabla de multiplicar: ");
			multiplicando = Integer.parseInt(in.readLine());
			while (validarDato(multiplicando) == false) {
				System.out.print("Ingrese un dato valido Dig�telo de nuevo ");
				multiplicando = Integer.parseInt(in.readLine());
			}
			TablaMultiplicar tabla = new TablaMultiplicar(multiplicando);
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = Integer.parseInt(in.readLine());
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = Integer.parseInt(in.readLine());
					
				} else {
					System.out.println("FIN DEL PROGRAMA");
					break;
					
				}
			}
		}
	}

	public static boolean validarDato(int numero) {
		if (numero <= 0) {
			return false;
			
		} else {
			return true;
			
		}
	}
}
