/*
 *NOMBRE DEL PROGRAMA : TABLA 
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 5  OCT DE 2016
 */
package PUNTO2;

public class TablaMultiplicar {
	int Tablamatriz[][];

	public TablaMultiplicar(int filayColumna) {
		this.Tablamatriz = new int[filayColumna + 1][filayColumna + 1];
		Tablamatriz[0][0] = 0;
		calcularTabla(filayColumna);
	}

	public void calcularTabla(int filayColumna) {
		for (int i = 1; i <= filayColumna; i++) {
			Tablamatriz[i][0] = i;
			for (int j = 1; j <= filayColumna; j++) {
				Tablamatriz[0][j] = j;
				Tablamatriz[i][j] = i * j;
			}
		}
		imprimirMatriz(filayColumna);
	}

	public void imprimirMatriz(int filayColumna) {
		System.out.println("LA TABLA DE MULTIPLICAR ESPECIFICADA ES ");
		for (int i = 0; i < filayColumna + 1; i++) {
			for (int j = 0; j < filayColumna + 1; j++) {
				System.out.print(Tablamatriz[i][j] + " ");
			}
			System.out.println();
		}
	}
}
