/*
 *NOMBRE DEL PROGRAMA : ORDEN 
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 5 OCT DE 2016
 */
package PUNTO1;

import java.util.Scanner;

public class OrdenAscendenteTest {
	private static Scanner leer;

	public static void main(String arg[]) {
		leer = new Scanner(System.in);

		// AQUI SE DEFINE LA VARIABLE PARA SABER SI SE REINICIARA EL PROGRAMA
		int reiniciar = 0;
		// Creo atributos donde almaceno los datos ingresado por el usuario
		int decision = 0;
		int limite = 0;
		int auxiliar = 0;

		do { 
			System.out.println("Bienvenido,  es un programa para ordenar enteros ");
			System.out.println("Ingrese un numero para  tomar una decision ");
			System.out.println("Si ingresa 1 se ordenara ascendentemente ");
			System.out.println("Si ingresa 2 se ordenara de descendentemente ");

			decision = leer.nextInt();

			OrdenAscendente MiVector = new OrdenAscendente();
			switch (decision) {
			case 1:
				System.out.println("Ha elegido la opcion 1 se ordenara ascendentemente ");
				System.out.println("Ingrese la cantidad de n�meros a ingresar ");
				// pido el limite del vector
				limite = leer.nextInt();
				// declaro y creo el vector
				int Vect[] = new int[limite + 1];
				// llamo los metodos de la clase
				MiVector.LlenarVector(Vect, limite);
				MiVector.OrdenAscendentes(Vect, limite, auxiliar);

				break;
			case 2:
				System.out.println("Ha elegido la opcion 1 se ordenara de descendentemente ");
				System.out.println("Ingrese la cantidad de n�meros a ingresar  ");
				// pido el limite del vector
				limite = leer.nextInt();
				// declaro y creo el vector
				int Vect1[] = new int[limite + 1];
				// llamo a los metodos de la clase
				MiVector.LlenarVector(Vect1, limite);
				MiVector.OrdenDescendente(Vect1, limite, auxiliar);

				break;
			default:
				System.out.println("Ha elegido una opcion invalida o no perteneciente a la lista de opciones ");
				break;
			}
			System.out.print("\n ");
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = leer.nextInt();
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = leer.nextInt();
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
		System.out.println("FIN DEL PROGRAMA");
	}

}
