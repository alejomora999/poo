/*
 *NOMBRE DEL PROGRAMA : ORDEN 
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 5 OCT DE 2016
 */
package PUNTO1;

import java.util.Scanner;

public class OrdenAscendente {
	// CREO LOS ATREBUTOS DE TIPO PRIVADO
	private int limite = 0;
	private int auxiliar = 0;
	private int vect[];
	private Scanner leer;

	// creo un metodo donde lleno el vector por teclado
	public void LlenarVector(int vec[], int limite) {
		this.limite = limite;
		// creo el for donde llenare desdela posicion 0 hasta la posicion -1
		for (int i = 0; i < limite; i++) {
			leer = new Scanner(System.in);
			System.out.print("X[" + (i + 1) + "=]");
			vec[i] = leer.nextInt();
		}
	}

	// creo el metodo donde se ordenaran los datos de forma ascendente
	public void OrdenAscendentes(int vec[], int limite, int auxiliar) {
		this.limite = limite;
		this.auxiliar = auxiliar;
		this.vect = vec;
		// creo 2 for uno para recorrer el vector respecto a I
		for (int i = 0; i < limite; i++) {
			// para el segundo for, evaluo desde J=I+1
			// ocupara el espacio de [I], si este es mayor
			for (int j = i + 1; j < limite; j++) {
				// este if evaluara si el siguiente dato es menor o mayor
				if (vec[i] > vec[j]) {
					auxiliar = vec[i];
					vec[i] = vec[j];
					// en el atributo auxiliar se guarda el dato cambiado

					vec[j] = auxiliar;
				}
			}
		}
		// una vez recorrido el arreglo imprimo el vector organizado
		System.out.print("El vector organizado de forma ascendente es ( ");
		for (int i = 0; i < limite; i++) {
			System.out.print(vec[i] + ",");

		}
		System.out.print(")");
	}

	// creo el mismo metodo pero en sentido contrario o sea descendente
	public void OrdenDescendente(int vec[], int limite, int auxiliar) {
		this.limite = limite;
		this.auxiliar = auxiliar;
		this.vect = vec;
		// creo 2 for uno para recorrer el vector respecto a I
		for (int i = 0; i < limite; i++) {
			// para el segundo for, evaluo desde J=I+1
			// ocupara el espacio de [I], si este es mayor
			for (int j = i + 1; j < limite; j++) {
				// este if evaluara si el siguiente dato es menor o mayor
				// DIFERENTE AL IF DEL METODO ANTERIOR diferente al if del
				// metodo anterior
				if (vec[i] < vec[j]) {
					auxiliar = vec[i];
					vec[i] = vec[j];
					vec[j] = auxiliar;
				}
			}
		}
		// una vez recorrido el arreglo imprimo el vector organizado
		System.out.print("El vector organizado de forma descendente es ( ");
		for (int i = 0; i < limite; i++) {

			System.out.print(vec[i] + ",");

		}
		System.out.print(")");
	}

	// CREO GETTERS Y SETTERS DE LOS ATRIBUTOS CREADOS AL INICIO DE LA CLASE
	public int getLimite() {
		return limite;
	}

	public void setLimite(int limite) {
		this.limite = limite;
	}

	public int getAuxiliar() {
		return auxiliar;
	}

	public void setAuxiliar(int auxiliar) {
		this.auxiliar = auxiliar;
	}

	public int[] getVect() {
		return vect;
	}

	public void setVect(int[] vect) {
		this.vect = vect;
	}
}
