/*
 *NOMBRE DEL PROGRAMA : NOTAS CURSO
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 9 SEP DE 2016
 */
package PUNTO4;
import java.io.*;
public class NotasCursoTest {
	public static void main(String arg[]) throws IOException{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		//AQUI SE DEFINE LA VARIABLE PARA SABER SI SE REINICIARA EL PROGRAMA
		int reiniciar;
		double nota1;
		double nota2;
		double nota3;
		do{
			//Creo mi objeto de tipo NotasCurso
			NotasCurso nota = new NotasCurso();
			//Pido al usuario su primera nota y la almaceno en el atributo que le corresponda
			System.out.println("Ingrese la nota 1 que tiene valor de 30%");
			nota1=Double.parseDouble(in.readLine());
			//El while validara si el dato es correcto o no y si no lo es lo pedira otra vez
			while(nota1<0 || nota1>5){
				System.out.println( "El valor de la nota no es valido, ingreselo otra vez");
				nota1=Double.parseDouble(in.readLine());
			}
			//Pido al usuario su segunda nota y la almaceno en el atributo que le corresponda
			System.out.println("Ingrese la nota 2 que tiene valor de  30%");
			nota2=Double.parseDouble(in.readLine());
			//El while validara si el dato es correcto o no y si no lo es lo pedira otra vez
			while(nota2<0 || nota2>5){
				System.out.println("El valor de la nota no es valido, ingreselo otra vez");
				nota2=Double.parseDouble(in.readLine());
			}
			//Pido al usuario su tercera nota y la almaceno en el atributo que le corresponda
			System.out.println("Ingrese la nota 3 que tiene valor de  40%");
			nota3=Double.parseDouble(in.readLine());
			//El while validara si el dato es correcto o no y si no lo es lo pedira otra vez
			while(nota3<0 || nota3>5){
				System.out.println( "El valor de la nota no es valido, ingreselo otra vez");
				nota3=Double.parseDouble(in.readLine());
			}
			//LLamo a la clase NotasCurso, en cada atributo
			nota.parcial1(nota1);
			double nota12=nota.parcial1(nota1);
			nota.parcial2(nota2);
			double nota21=nota.parcial2(nota2);
			nota.examenfinal(nota3);
			double nota3_1=nota.examenfinal(nota3);
			nota.resultadocurso( nota3_1,  nota12,  nota21);
			double resultadofinal=nota.resultadocurso( nota3_1,  nota12,  nota21);
			System.out.println("la nota definitiva es: "+resultadofinal);
			//Este else if revisara los parametros de la nota final y dara un reconocimiento  
			if(resultadofinal>4.5){
				System.out.println("Felicitaciones, Usted llegar� lejos");
			}else if(resultadofinal>=4){
				if(resultadofinal<=4.5)
				System.out.println("Puede mejorar");
			}else if(resultadofinal>=3){
				if(resultadofinal<=4)
				System.out.println("Debe esforzarse un poco m�s");
			}else if(resultadofinal<3){
				System.out.println("Debe repetir la materia");
			}
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = Integer.parseInt(in.readLine());
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no");
					reiniciar = Integer.parseInt(in.readLine());
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
		System.out.println("FIN DEL PROGRAMA");
	}
}
