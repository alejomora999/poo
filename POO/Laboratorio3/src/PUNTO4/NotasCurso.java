/*
 *NOMBRE DEL PROGRAMA : NOTAS CURSO
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA:  9 SEP DE 2016
 */
package PUNTO4;
public class NotasCurso {
	//Creo un metodo llamado parcial2 donde la nota tendra un valor del 30%
	double parcial1 (double nota1){
		//Devuelvo el valor de la nota segun el porcentaje que corresponda
		return nota1*=0.3; 
	}
	//Creo un metodo  llamado parcial donde la nota tendra un valor del 30%
	double parcial2 (double nota2){
		//Devuelvo el valor de la nota segun el porcentaje que le corresponda
		return nota2*=0.3; 
	}
	//Creo un metodo  llamado examen final, donde dicha nota tendra un valor del 40%
	double examenfinal (double nota3){
		//Devuelvo el valor de la nota segun el  porcentaje que corresponda
		return nota3*=0.4; 
	}
	//Creo un metodo  llamado resultadocurso, donde se obtendra la  nota final
	double resultadocurso(double examenfinal, double parcial1, double parcial2){
		//Creo un atributo tipo double donde se va a almacenar la suma de las notas 
		double sumanotas= examenfinal+ parcial1+parcial2;
		//Devuelvo el valor de la nota definitiva con el total de su porcentaje
		return sumanotas;
	}

}
