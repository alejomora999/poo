/*
 *NOMBRE DEL PROGRAMA : DIVISORES POSITIVOS
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 9 SEP DE 2016
 */
package PUNTO3;
import java.io.*;
public class DivisoresPostivosTest {
	public static void main(String arg[]) throws IOException{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		//AQUI SE DEFINE LA VARIABLE PARA SABER SI SE REINICIARA EL PROGRAMA
		int reiniciar;
		double numero;
		do{
			//Creo mi objeto de tipo DivisoresPositivos
			DivisoresPositivos numdiv = new DivisoresPositivos();
			//Pido al usuario el valor de las variables creadas y las almaceno
			System.out.println("ingrese su numero para saber los divisores positivos");
			numero=Double.parseDouble(in.readLine());
			int parte_entera= (int)(numero);
			//Reviso si el dato ingresado es un entero
			while(parte_entera!=numero){
				System.out.println( "Por favor solo digita numero enteros, si no lo haces el programa no funcionara, vuelve a ingresar el valor");
				numero=Double.parseDouble(in.readLine());
				parte_entera= (int)(numero);
				}
			if(parte_entera==numero){
				//Si obtengo un dato se validara llamando a la clase
				numdiv.numdivisor(parte_entera);
			}
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = Integer.parseInt(in.readLine());
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no");
					reiniciar = Integer.parseInt(in.readLine());
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
		System.out.println("FIN DEL PROGRAMA");
		
	}
}
