/*
 *NOMBRE DEL PROGRAMA : NUMERO PAR
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 9 SEP DE 2016
 */
package PUNTO2;

public class NumeroPar {
	//Creo un metodo booleano llamado numpar
	boolean numpar(int numero){
		//Creo una sentencia donde se va a determinar
		//si el n�mero con el que se va a trabajar es par o no
		if(numero%2==0){
			//si el numero es par retornara un valor true
			return true;
			
		}else{
			//si el numero si es impar retornara un valor false
			return false;
		}
	}

}
