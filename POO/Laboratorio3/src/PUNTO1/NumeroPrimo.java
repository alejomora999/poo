/*
 *NOMBRE DEL PROGRAMA : NUMERO PRIMO
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 9 SEP DE 2016
 */
package PUNTO1;

public class NumeroPrimo {
	//Creo un contador de tipo double para la variable 
	double valor=0;
	//Se Crea una sentencia donde determino si el n�mero con el que se trabajara 
	//es primo o no
	//Creo un metodo booleano llamado numprimo
	boolean numprimo (int num) {
		//El for valida si el valor del numero es primo 
		for (int i=1;i<=num+1;i++){
			if(num%i==0){
				valor++;
			}
		}
		//Con este if if revalido  si el contador me devuelve un n�mero primo o no
		if(valor!=2){
			//Si el numero no es primo me retornara el valor false
			return false;
		}else{
			//si el numero es primo retornara el valor true
			return true;
		}
	}
	
}
