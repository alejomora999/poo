/*
 *NOMBRE DEL PROGRAMA : DIVISORES POSITIVOS
 * AUTOR: JHON ALEJANDRO MORALES MOJICA
 *FECHA: 07 DE SEP DE 2016
 */
package punto_3;
import java.io.*;
public class DivisoresPostivosTest {
	public static void main(String arg[]) throws IOException{
		// Tras haber llamado la libreria java.io.* le indico al programa que se
		// ingresara algo por teclado
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		// Defino la variable reiniciar, sera la que determine si quiero repetir
		// el programa de nuevo o no
		int reiniciar;
		//Creo una variable donde almaceno el dato ingresado por el usuario
		double num1;
		do{
			//Creo mi objeto de tipo DivisoresPositivos
			DivisoresPositivos numerito = new DivisoresPositivos();
			//Pido al usuario los valores de las variables ya creadas y las almaceno
			System.out.println("ingrese su numero para saber sus divisores positivos :");
			num1=Double.parseDouble(in.readLine());
			int parte_entera= (int)(num1);
			//Rectifico si el dato ingresado es un entero
			while(parte_entera!=num1){
				System.out.println( " No es un Entero, vuelve a ingresar el n�mero");
				num1=Double.parseDouble(in.readLine());
				parte_entera= (int)(num1);
				}
			if(parte_entera==num1){
				//Si obtengo un dato valido llamo a la clase
				numerito.esdivisor(parte_entera);
			}
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = Integer.parseInt(in.readLine());
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = Integer.parseInt(in.readLine());
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
		
	}
}
