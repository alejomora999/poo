/*
 *NOMBRE DEL PROGRAMA : DIVISORES POSITIVOS
 * AUTOR: JHON ALEJANDRO MORALES MOJICA
 *FECHA: 07 DE SEP DE 2016
 */
package punto_3;

public class DivisoresPositivos {
	//Creo un metodo  llamado esdivisor
	void esdivisor(int num) {
		//comparo si mi variable es igual, menor o mayor a 0
		if(num==0){
			System.out.println( "Todos los reales son divisores del 0 excepto el mismo 0:");
		}
		//Como solo necesito divisores positivos, en el caso de que mi variable sea negativa la multiplico por -1
		//Realizo un for donde evaluare que terminos son divisores de mi variable
		if (num<0){
			num=(num*-1);
			System.out.println( "Tus Divisores positivos son:");
			for(int i=1;i<=num; i++){
				
				if (num%i==0){
					System.out.println(i);
					}
			}
			
		}
		//Realizo un for donde evaluare que terminos son divisores de mi variable
		else {
			System.out.println( "Tus Divisores positivos  son:");
			for(int i=1;i<=num; i++){
				if (num%i==0){
					if(i>0){
						System.out.println(i);
						}
					}
			}
		}
	}
}
	

