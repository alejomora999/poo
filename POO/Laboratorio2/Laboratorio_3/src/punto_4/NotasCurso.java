/*
 *NOMBRE DEL PROGRAMA : NOTAS CURSO
 * AUTOR: JHON ALEJANDRO MORALES MOJICA
 *FECHA: 07 DE SEP DE 2016
 */
package punto_4;
public class NotasCurso {
	//Creo un metodo  llamado parcial1 donde dicha nota tendra un valor del 30%
	double parcial1(double nota_1){
		//Devuelvo el valor de la nota segun su porcentaje
		return nota_1*=0.3; 
	}
	//Creo un metodo  llamado parcial2 donde dicha nota tendra un valor del 30%
	double parcial2(double nota_2){
		//Devuelvo el valor de la nota segun su porcentaje
		return nota_2*=0.3; 
	}
	//Creo un metodo  llamado examen final, donde dicha nota tendra un valor del 40%
	double examenfinal(double nota_3){
		//Devuelvo el valor de la nota segun su porcentaje
		return nota_3*=0.4; 
	}
	//Creo un metodo  llamado resultado_curso, donde obtendre la nota final
	double resultado_curso(double examenfinal, double parcial1, double parcial2){
		//Creo un atributo tipo double donde almacenare la suma de las notas ingresadas
		double suma_de_notas= examenfinal+ parcial1+parcial2;
		//Devuelvo el valor de la nota definitiva con el total de su porcentaje
		return suma_de_notas;
	}

}
