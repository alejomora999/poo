/*
 *NOMBRE DEL PROGRAMA : NOTAS CURSO
 * AUTOR: JHON ALEJANDRO MORALES MOJICA
 *FECHA: 07 DE SEP DE 2016
 */
package punto_4;
import java.io.*;
public class NotasCursoTest {
	public static void main(String arg[]) throws IOException{
		// Tras haber llamado la libreria java.io.* le indico al programa que se
		// ingresara algo por teclado
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		// Defino la variable reiniciar, sera la que determine si quiero repetir
		// el programa de nuevo o no
		int reiniciar;
		//Creo una variable donde almaceno los datos ingresado por el usuario
		double nota1;
		double nota2;
		double nota3;
		do{
			//Creo mi objeto de tipo NotasCurso
			NotasCurso notica = new NotasCurso();
			//Pido al usuario su primera nota y la almaceno en su respecctivo atributo
			System.out.println("Ingrese la primera nota, correspondiente al 30% :");
			nota1=Double.parseDouble(in.readLine());
			//Este while comprueba si el dato ingresado es correcto o no, si no lo es lo volvera a pedir
			while(nota1<0 || nota1>5){
				System.out.println( " El valor de la nota es incorrecto, ingreselo nuevamente");
				nota1=Double.parseDouble(in.readLine());
			}
			//Pido al usuario su primera nota y la almaceno en su respecctivo atributo
			System.out.println("Ingrese la segunda nota, correspondiente al 30% :");
			nota2=Double.parseDouble(in.readLine());
			//Este while comprueba si el dato ingresado es correcto o no, si no lo es lo volvera a pedir
			while(nota2<0 || nota2>5){
				System.out.println( " El valor de la nota es incorrecto, ingreselo nuevamente");
				nota2=Double.parseDouble(in.readLine());
			}
			//Pido al usuario su primera nota y la almaceno en su respecctivo atributo
			System.out.println("Ingrese la tercera nota, correspondiente al 40% :");
			nota3=Double.parseDouble(in.readLine());
			//Este while comprueba si el dato ingresado es correcto o no, si no lo es lo volvera a pedir
			while(nota3<0 || nota3>5){
				System.out.println( " El valor de la nota es incorrecto, ingreselo nuevamente");
				nota3=Double.parseDouble(in.readLine());
			}
			//LLamo a la clase NotasCurso, en cada atributo
			notica.parcial1(nota1);
			double nota1_1=notica.parcial1(nota1);
			notica.parcial2(nota2);
			double nota2_1=notica.parcial2(nota2);
			notica.examenfinal(nota3);
			double nota3_1=notica.examenfinal(nota3);
			notica.resultado_curso( nota3_1,  nota1_1,  nota2_1);
			double resultadofinal=notica.resultado_curso( nota3_1,  nota1_1,  nota2_1);
			System.out.println("Su nota definitiva es: "+resultadofinal);
			//Este else if revisara los parametros de la nota final y asignara una condicion propuesta
			if(resultadofinal>4.5){
				System.out.println("Felicitaciones, Usted llegar� lejos");
			}else if(resultadofinal>=4){
				if(resultadofinal<=4.5)
				System.out.println("Puede hacerlo mejor");
			}else if(resultadofinal>=3){
				if(resultadofinal<=4)
				System.out.println("Debe esforzarse m�s");
			}else if(resultadofinal<3){
				System.out.println("Debe repetir la asignatura");
			}
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = Integer.parseInt(in.readLine());
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = Integer.parseInt(in.readLine());
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
	}
}
