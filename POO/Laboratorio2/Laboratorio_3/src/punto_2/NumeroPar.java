/*
 *NOMBRE DEL PROGRAMA : NUMERO PAR O IMPAR
 * AUTOR: JHON ALEJANDRO MORALES MOJICA
 *FECHA: 07 DE SEP DE 2016
 */
package punto_2;

public class NumeroPar {
	//Creo un metodo booleano llamado espar
	boolean espar(int num){
		//Creo una sentencia donde determino si el n�mero que se va a trabajar es par o no
		if(num%2==0){
			//si el numero si es par retornara true
			return true;
			
		}else{
			//si el numero si es impar retornara false
			return false;
		}
	}

}
