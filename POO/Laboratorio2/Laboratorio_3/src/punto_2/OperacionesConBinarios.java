/*
 *NOMBRE DEL PROGRAMA : OPERACIONES CON BINARIOS
 * AUTOR: JHON ALEJANDRO MORALES MOJICA
 *FECHA: 03 DE SEP DE 2016
 */
package punto_2;
import java.io.*;
public class OperacionesConBinarios {
	public static void main(String arg[]) throws IOException {
		// Tras haber llamado la libreria java.io.* le indico al programa que se
		// ingresara algo por teclado
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		// Defino la variable reiniciar, sera la que determine si quiero repetir
		// el programa de nuevo o no, por otra parte la varible desicion sera
		// usado en un Switch
		int reiniciar;
		// Defino la variable decision, sera la que determine que �rea sera
		// realizada
		int decision;
		//Defino las variables String donde almacenare las conversiones
		String numero_binario1, numero_binario2, resultado_binario;
		//Defino la variables num1 y num2 donde convertire las diferentes operaciones
		int num1, num2;
		//Defino la variable resultado donde almacenare el resultado de la operacion
		int resultado_operacion;
		// Este Do While es el que decide si se repite o no el programa
		do {
			// Imprimo por consola un saludo, y las diferentes opciones de
			// conversion
			System.out.println("Bienvenido, esta es una lista de conversion entre diversas unidades:");
			System.out.println("Ingrese un numero para  tomar una decision :");
			System.out.println("Si ingresa 1 se sumaran dos n�meros binarios :");
			System.out.println("Si ingresa 2 se restaran dos n�meros binarios :");						System.out.println("Si ingresa 3 se convertira un n�mero de binario a hexadecimal :");
			System.out.println("Si ingresa 3 se multiplicaran dos n�meros binarios:");
			System.out.println("Si ingresa 4 se dividiran dos n�meros binarios :");
			// Le pido al usuario una respuesta y la almaceno en la variable
			// decision
			decision = Integer.parseInt(in.readLine());
			// Procedo a crear sentencias segun la variable dcision, en un
			// Switch
			switch (decision) {
						
			case 1:
				System.out.println("Ha elegido la opcion 1, sumar dos binarios :");
				System.out.println("ingrese el primer binario :");
				// El valor lo almacenare en la siguiente variable
				numero_binario1 = (in.readLine());
				// los siguientes While determinaran si el dato ingresado es
				// correcto, si no lo es, pedira uno valido hasta que sea
				// ingresado
				while (numero_binario1.matches("[2-9]*") || numero_binario1.matches("[a-z]*")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_binario1 = (in.readLine());
				}
				while (numero_binario1.matches("[2-9]+[a-z]")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_binario1 = (in.readLine());
				}				
				System.out.println("ingrese el segundo binario :");
				// El valor lo almacenare en la siguiente variable
				numero_binario2 = (in.readLine());
				// los siguientes While determinaran si el dato ingresado es
				// correcto, si no lo es, pedira uno valido hasta que sea
				// ingresado
				while (numero_binario2.matches("[2-9]*") || numero_binario2.matches("[a-z]*")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_binario2 = (in.readLine());
				}
				while (numero_binario2.matches("[2-9]+[a-z]")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_binario2 = (in.readLine());
				}

				if (numero_binario1.matches("[0-1]*")) {
					if(numero_binario2.matches("[0-1]*")){
						// Si tengo un dato valido realizo la respectiva conversion
						num1 = Integer.parseInt(numero_binario1, 2);
						num2 = Integer.parseInt(numero_binario2, 2);
						resultado_operacion=num1+num2;
						resultado_binario= Integer.toBinaryString(resultado_operacion);
						System.out.println("tu suma es: "+resultado_binario);
					}
					break;
				}
				break;
			case 2:
				System.out.println("Ha elegido la opcion 2, restar dos binarios :");
				System.out.println("ingrese el primer binario :");
				// El valor lo almacenare en la siguiente variable
				numero_binario1 = (in.readLine());
				// los siguientes While determinaran si el dato ingresado es
				// correcto, si no lo es, pedira uno valido hasta que sea
				// ingresado
				while (numero_binario1.matches("[2-9]*") || numero_binario1.matches("[a-z]*")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_binario1 = (in.readLine());
				}
				while (numero_binario1.matches("[2-9]+[a-z]")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_binario1 = (in.readLine());
				}				
				System.out.println("ingrese el segundo binario :");
				// El valor lo almacenare en la siguiente variable
				numero_binario2 = (in.readLine());
				// los siguientes While determinaran si el dato ingresado es
				// correcto, si no lo es, pedira uno valido hasta que sea
				// ingresado
				while (numero_binario2.matches("[2-9]*") || numero_binario2.matches("[a-z]*")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_binario2 = (in.readLine());
				}
				while (numero_binario2.matches("[2-9]+[a-z]")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_binario2 = (in.readLine());
				}

				if (numero_binario1.matches("[0-1]*")) {
					if(numero_binario2.matches("[0-1]*")){
						// Si tengo un dato valido realizo la respectiva conversion
						num1 = Integer.parseInt(numero_binario1, 2);
						num2 = Integer.parseInt(numero_binario2, 2);
						resultado_operacion=num1-num2;
						resultado_binario= Integer.toBinaryString(resultado_operacion);
						System.out.println("tu resta es: "+resultado_binario);
					}
					break;
				}
				break;
			case 3:
				System.out.println("Ha elegido la opcion 3, multiplicar dos binarios :");
				System.out.println("ingrese el primer binario :");
				// El valor lo almacenare en la siguiente variable
				numero_binario1 = (in.readLine());
				// los siguientes While determinaran si el dato ingresado es
				// correcto, si no lo es, pedira uno valido hasta que sea
				// ingresado
				while (numero_binario1.matches("[2-9]*") || numero_binario1.matches("[a-z]*")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_binario1 = (in.readLine());
				}
				while (numero_binario1.matches("[2-9]+[a-z]")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_binario1 = (in.readLine());
				}				
				System.out.println("ingrese el segundo binario :");
				// El valor lo almacenare en la siguiente variable
				numero_binario2 = (in.readLine());
				// los siguientes While determinaran si el dato ingresado es
				// correcto, si no lo es, pedira uno valido hasta que sea
				// ingresado
				while (numero_binario2.matches("[2-9]*") || numero_binario2.matches("[a-z]*")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_binario2 = (in.readLine());
				}
				while (numero_binario2.matches("[2-9]+[a-z]")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_binario2 = (in.readLine());
				}

				if (numero_binario1.matches("[0-1]*")) {
					if(numero_binario2.matches("[0-1]*")){
						// Si tengo un dato valido realizo la respectiva conversion
						num1 = Integer.parseInt(numero_binario1, 2);
						num2 = Integer.parseInt(numero_binario2, 2);
						resultado_operacion=num1*num2;
						resultado_binario= Integer.toBinaryString(resultado_operacion);
						System.out.println("tu producto es: "+resultado_binario);
					}
					break;
				}
				break;
			case 4:
				System.out.println("Ha elegido la opcion 4, dividir dos binarios :");
				System.out.println("ingrese el primer binario :");
				// El valor lo almacenare en la siguiente variable
				numero_binario1 = (in.readLine());
				// los siguientes While determinaran si el dato ingresado es
				// correcto, si no lo es, pedira uno valido hasta que sea
				// ingresado
				while (numero_binario1.matches("[2-9]*") || numero_binario1.matches("[a-z]*")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_binario1 = (in.readLine());
				}
				while (numero_binario1.matches("[2-9]+[a-z]")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_binario1 = (in.readLine());
				}				
				System.out.println("ingrese el segundo binario :");
				// El valor lo almacenare en la siguiente variable
				numero_binario2 = (in.readLine());
				// los siguientes While determinaran si el dato ingresado es
				// correcto, si no lo es, pedira uno valido hasta que sea
				// ingresado
				while (numero_binario2.matches("[2-9]*") || numero_binario2.matches("[a-z]*")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_binario2 = (in.readLine());
				}
				while (numero_binario2.matches("[2-9]+[a-z]")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_binario2 = (in.readLine());
				}
				while(numero_binario2.matches("[0-1]*")){
					num2 = Integer.parseInt(numero_binario2, 2);
					if(num2==0){
						System.out.println("Su segundo binario no puede ser cero, ingreselo nuevamente :");
						numero_binario2 = (in.readLine());
						num2 = Integer.parseInt(numero_binario2, 2);
					}
					break;
				}

				if (numero_binario1.matches("[0-1]*")) {
					if(numero_binario2.matches("[0-1]*")){
						// Si tengo un dato valido realizo la respectiva conversion
						num1 = Integer.parseInt(numero_binario1, 2);
						num2 = Integer.parseInt(numero_binario2, 2);
						resultado_operacion=num1/num2;
						resultado_binario= Integer.toBinaryString(resultado_operacion);
						System.out.println("tu division es: "+resultado_binario);
					}
					break;
				}
				break;
			default:
				// En caso de haber seleccionado una opcion que no est� en el
				// menu, imprimo este aviso y salgo del Switch
				System.out.println("Ha elegido una opcion invalida o no perteneciente a la lista de opciones :");
				break;
			
			}
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = Integer.parseInt(in.readLine());
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = Integer.parseInt(in.readLine());
				} else {
					break;
				}
			}
		}while (reiniciar != 0);
		
		
	}
}
