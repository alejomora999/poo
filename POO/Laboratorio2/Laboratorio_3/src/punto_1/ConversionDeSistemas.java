/*
 *NOMBRE DEL PROGRAMA : CONVERSION DE SISTEMAS
 * AUTOR: JHON ALEJANDRO MORALES MOJICA
 *FECHA: 03 DE SEP DE 2016
 */
package punto_1;
import java.io.*;
public class ConversionDeSistemas {
	public static void main(String arg[]) throws IOException {
		// Tras haber llamado la libreria java.io.* le indico al programa que se
		// ingresara algo por teclado
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		// Defino la variable reiniciar, sera la que determine si quiero repetir
		// el programa de nuevo o no, por otra parte la varible desicion sera
		// usado en un Switch
		int reiniciar;
		// Defino la variable decision, sera la que determine que �rea sera
		// realizada
		int decision;
		//Defino las variables String donde almacenare las conversiones
		String numero_binario, numero_octal, numero_hexadecimal;
		//Defino la variable num donde convertire los diferetes sistemas
		int num;
		// Este Do While es el que decide si se repite o no el programa
		do {
			// Imprimo por consola un saludo, y las diferentes opciones de
			// conversion
			System.out.println("Bienvenido, esta es una lista de conversion entre diversas unidades:");
			System.out.println("Ingrese un numero para  tomar una decision :");
			System.out.println("Si ingresa 1 se convertira un n�mero de binario a octal :");
			System.out.println("Si ingresa 2 se convertira un n�mero de octal a hexadecimal :");
			System.out.println("Si ingresa 3 se convertira un n�mero de binario a hexadecimal :");
			System.out.println("Si ingresa 4 se convertira un n�mero de octal a binario:");
			System.out.println("Si ingresa 5 se convertira un n�mero de hexadecimal a octal :");
			System.out.println("Si ingresa 6 se convertira un n�mero de hexadecimal a binario :");
			// Le pido al usuario una respuesta y la almaceno en la variable
			// decision
			decision = Integer.parseInt(in.readLine());
			// Procedo a crear sentencias segun la variable dcision, en un
			// Switch
			switch (decision) {
			case 1:
				// Le pido al usuario que ingrese un numero en binario
				System.out.println("Ha elegido la opcion 1 de binario a octal :");
				System.out.println("Ingrese el numero en binario :");
				// El valor lo almacenare en la siguiente variable
				numero_binario = (in.readLine());
				// los siguientes While determinaran si el dato ingresado es
				// correcto, si no lo es, pedira uno valido hasta que sea
				// ingresado
				while (numero_binario.matches("[2-9]*") || numero_binario.matches("[a-z]*")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_binario = (in.readLine());
				}
				while (numero_binario.matches("[2-9]+[a-z]")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_binario = (in.readLine());
				}

				if (numero_binario.matches("[0-1]*")) {
					// Si tengo un dato valido realizo la respectiva conversion
					num = Integer.parseInt(numero_binario, 2);
					numero_octal = Integer.toOctalString(num);
					System.out.println(" el numero en octal es :" + numero_octal);
					break;
				}
				break;
			case 2:
				// Le pido al usuario que ingrese un numero en octal
				System.out.println("Ha elegido la opcion 2 de octal a hexadecimal:");
				System.out.println("Ingrese el numero en octal :");
				// El valor lo almacenare en la siguiente variable
				numero_octal = (in.readLine());
				// los siguientes While determinaran si el dato ingresado es
				// correcto, si no lo es, pedira uno valido hasta que sea
				// ingresado
				while (numero_octal.matches("[8-9]*") || numero_octal.matches("[a-z]*")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_octal = (in.readLine());
				}
				while (numero_octal.matches("[0-9]+[a-z]")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_octal = (in.readLine());
				}

				if (numero_octal.matches("[0-7]*")) {
					// Si tengo un dato valido realizo la respectiva conversion
					num = Integer.parseInt(numero_octal, 8);
					numero_hexadecimal = Integer.toHexString(num);
					System.out.println(" el numero en hexadecimal es :" + numero_hexadecimal);
					break;
				}
				break;
			case 3:
				// Le pido al usuario que ingrese un numero en binario
				System.out.println("Ha elegido la opcion 3 de binario a hexadecimal :");
				System.out.println("Ingrese el numero en binario :");
				// El valor lo almacenare en la siguiente variable
				numero_binario = (in.readLine());
				// los siguientes While determinaran si el dato ingresado es
				// correcto, si no lo es, pedira uno valido hasta que sea
				// ingresado
				while (numero_binario.matches("[2-9]*") || numero_binario.matches("[a-z]*")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_binario = (in.readLine());
				}
				while (numero_binario.matches("[2-9]+[a-z]")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_binario = (in.readLine());
				}
				if (numero_binario.matches("[0-1]*")) {
					// Si tengo un dato valido realizo la respectiva conversion
					num = Integer.parseInt(numero_binario, 2);
					numero_hexadecimal = Integer.toHexString(num);
					System.out.println(" el numero en octal es :" + numero_hexadecimal);
					break;
				}
				break;
			case 4:
				// Le pido al usuario que ingrese un numero en octal
				System.out.println("Ha elegido la opcion 4 de octal a binario:");
				System.out.println("Ingrese el numero en octal :");
				// El valor lo almacenare en la siguiente variable
				numero_octal = (in.readLine());
				// los siguientes While determinaran si el dato ingresado es
				// correcto, si no lo es, pedira uno valido hasta que sea
				// ingresado
				while (numero_octal.matches("[8-9]*") || numero_octal.matches("[a-z]*")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_octal = (in.readLine());
				}
				while (numero_octal.matches("[0-9]+[a-z]")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_octal = (in.readLine());
				}

				if (numero_octal.matches("[0-7]*")) {
					// Si tengo un dato valido realizo la respectiva conversion
					num = Integer.parseInt(numero_octal, 8);
					numero_binario = Integer.toBinaryString(num);
					System.out.println(" el numero en hexadecimal es :" + numero_binario);
					break;
				}
				break;
			case 5:
				// Le pido al usuario que ingrese un numero en octal
				System.out.println("Ha elegido la opcion 5 de hexadecimal a octal:");
				System.out.println("Ingrese el numero en hexadecimal :");
				// El valor lo almacenare en la siguiente variable
				numero_hexadecimal = (in.readLine());
				// los siguientes While determinaran si el dato ingresado es
				// correcto, si no lo es, pedira uno valido hasta que sea
				// ingresado
				while (numero_hexadecimal.matches("[g-z]*") || numero_hexadecimal.matches("[G-Z]*")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_hexadecimal = (in.readLine());
				}
				while (numero_hexadecimal.matches("[0-9]+[g-z]") || numero_hexadecimal.matches("[0-9]+[G-Z]")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_hexadecimal = (in.readLine());
				}

				if (numero_hexadecimal.matches("[0-9]+[a-f]") || numero_hexadecimal.matches("[0-9]*")
						|| numero_hexadecimal.matches("[a-f]*") || numero_hexadecimal.matches("[A-F]*")||numero_hexadecimal.matches("[0-9]+[A-F]")) {
					// Si tengo un dato valido realizo la respectiva conversion
					num = Integer.parseInt(numero_hexadecimal, 16);
					numero_octal = Integer.toOctalString(num);
					System.out.println(" el numero en octal es :" + numero_octal);
					break;
				}
				break;
			case 6:
				// Le pido al usuario que ingrese un numero en octal
				System.out.println("Ha elegido la opcion 5 de hexadecimal a binario:");
				System.out.println("Ingrese el numero en hexadecimal :");
				// El valor lo almacenare en la siguiente variable
				numero_hexadecimal = (in.readLine());
				// los siguientes While determinaran si el dato ingresado es
				// correcto, si no lo es, pedira uno valido hasta que sea
				// ingresado
				while (numero_hexadecimal.matches("[g-z]*") || numero_hexadecimal.matches("[G-Z]*")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_hexadecimal = (in.readLine());
				}
				while (numero_hexadecimal.matches("[0-9]+[g-z]") || numero_hexadecimal.matches("[0-9]+[G-Z]")) {
					System.out.println("Su dato es invalido, ingreselo nuevamente :");
					numero_hexadecimal = (in.readLine());
				}

				if (numero_hexadecimal.matches("[0-9]+[a-f]") || numero_hexadecimal.matches("[0-9]*")
						|| numero_hexadecimal.matches("[a-f]*") || numero_hexadecimal.matches("[A-F]*")||numero_hexadecimal.matches("[0-9]+[A-F]")) {
					// Si tengo un dato valido realizo la respectiva conversion
					num = Integer.parseInt(numero_hexadecimal, 16);
					numero_binario = Integer.toBinaryString(num);
					System.out.println(" el numero en octal es :" + numero_binario);
					break;
				}
				break;
			default:
				// En caso de haber seleccionado una opcion que no est� en el
				// menu, imprimo este aviso y salgo del Switch
				System.out.println("Ha elegido una opcion invalida o no perteneciente a la lista de opciones :");
				break;
			}
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = Integer.parseInt(in.readLine());
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = Integer.parseInt(in.readLine());
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
	}

}
