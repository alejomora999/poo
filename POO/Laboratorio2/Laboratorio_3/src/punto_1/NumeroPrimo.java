/*
 *NOMBRE DEL PROGRAMA : NUMERO PRIMO
 * AUTOR: JHON ALEJANDRO MORALES MOJICA
 *FECHA: 07 DE SEP DE 2016
 */
package punto_1;
//Llamo a mi clase NumeroPrimo
public class NumeroPrimo {
	//Creo un contador de tipo double
	double a=0;
	//Creo una sentencia donde determino si el n�mero que se va a trabajar es primo o no
	//Creo un metodo booleano llamado esimpar
	boolean esprimo(int num) {
		//Este for me dice valida que n�mero e sprimo y cual no
		for (int i=1;i<=num+1;i++){
			if(num%i==0){
				a++;
			}
		}
		//Con el siguiente if revalido con otra prueba si el contador me tra un n�mero primo o no
		if(a!=2){
			//Si el numero no es primo me retornara false
			return false;
		}else{
			//si el numero si es primo retornara true
			return true;
		}
	}
	
}
