/*
 *NOMBRE DEL PROGRAMA : NUMERO PRIMO
 * AUTOR: JHON ALEJANDRO MORALES MOJICA
 *FECHA: 07 DE SEP DE 2016
 */
package punto_1;
import java.io.*;
public class NumeroPrimoTest {
	public static void main(String arg[]) throws IOException{
		// Tras haber llamado la libreria java.io.* le indico al programa que se
		// ingresara algo por teclado
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		// Defino la variable reiniciar, sera la que determine si quiero repetir
		// el programa de nuevo o no
		int reiniciar;
		//Creo una variable donde almaceno el dato ingresado por el usuario
		double num1;
		do{
			//Creo mi objeto de tipo NumeroPrimo
			NumeroPrimo numerito = new NumeroPrimo();
			//Pido al usuario los valores de las variables ya creadas y las almaceno
			System.out.println("ingrese su numero para saber si es Primo o no :");
			num1=Double.parseDouble(in.readLine());
			int parte_entera= (int)(num1);
			//Rectifico si el dato ingresado es un entero
			while(parte_entera!=num1){
				System.out.println( " No es un Entero, te dire si tu n�mero es primo o no solo con enteros, vuelvelo a ingresar");
				num1=Double.parseDouble(in.readLine());
				parte_entera= (int)(num1);
				}
			if(parte_entera==num1){
				//Si obtengo un dato valido llamo a la clase
				if(numerito.esprimo(parte_entera)){
					System.out.println("Su n�mero es primo");
				}else{
					System.out.println("Su n�mero no es primo");
				}
			}
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = Integer.parseInt(in.readLine());
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = Integer.parseInt(in.readLine());
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
		
	}
}
