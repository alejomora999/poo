/*
 * NOMBRE PROGRAMA: AREA DEL CIRCULO
 * AUTOR: NICOLAS MENESES GUERRERO
 * FECHA: AGO 26 DE 2016
 */
package punto2;
import java.io.*;
public class AreaDelCirculo {
	public static void main(String arg[]) throws IOException{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		double radio;
		double resultado=0;
		System.out.println("Ingresa el numero del radio: ");
		radio=Double.parseDouble(in.readLine());
		if (radio<0){
			System.out.println("El radio no puede ser negativo");
		}else{
			resultado= (Math.PI*(Math.pow(radio, 2)));
			System.out.println ("El area del circulo  es: "+ resultado + " unidades");
		}
	}
		

}

