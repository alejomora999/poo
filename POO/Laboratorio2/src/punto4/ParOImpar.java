/*
 * NOMBRE PROGRAMA: PAR O IMPAR
 * AUTOR: NICOLAS MENESES GUERRERO
 * FECHA: AGO 28 DE 2016
 */
package punto4;

import java.io.*;
import java.io.IOException;
import java.io.InputStreamReader;

public class ParOImpar {
	public static void main(String arg[]) throws IOException{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		double n;
		System.out.println("Digita el numero ");
		n=Double.parseDouble(in.readLine());
		if (n%2==0) {
			System.out.println("El numero " + n +" Es Par");
		} else {
			System.out.println("El numero "+ n +" Es Impar");
		}
	}


}

