/*
 * NOMBRE PROGRAMA: PERIMETRO DEL RECTANGULO
 * AUTOR: NICOLAS MENESES GUERRERO
 * FECHA: AGO 26 DE 2016
 */
package punto1;
import java.io.*;
public class PerimetroDelRectangulo {
	public static void main(String arg[]) throws IOException{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		double largo;
		double alto;
		double resultado=0;
		System.out.println("Ingresa el numero del largo: ");
		largo=Double.parseDouble(in.readLine());
		System.out.println("Ingresa el numero del alto: ");
		alto=Double.parseDouble(in.readLine());
		//System.out.println(largo);
		//System.out.println(alto);
		if (largo<0){
			System.out.println("El largo no puede ser negativo");
		} else if (alto<0){
			System.out.println("El alto no puede ser negativo");
		}
		else{
			resultado=((2*largo)+(2*alto));
			System.out.println ("El perimetro es: "+ resultado + " unidades");
		}
	}

}
