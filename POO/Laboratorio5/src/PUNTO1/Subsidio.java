/*
 *NOMBRE DEL PROGRAMA : VALOR SUBSIDIO
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 16 SEP DE 2016
 */
package PUNTO1;

public class Subsidio {
	
	private String Nombres;
	private String Apellidos;
	private String Identificacion;
	private double PagoMensual;
	
	public String getNombres(String Nombres) {
		this.Nombres=Nombres;
		return this.Nombres;
	}
	public void setNombres(String nombres) {
		Nombres = nombres;
	}
	public String getApellidos(String Apellidos) {
		this.Apellidos=Apellidos;
		return this.Apellidos;
	}
	public void setApellidos(String apellidos) {
		Apellidos = apellidos;
	}
	public String getIdentificacion(String Identificacion) {
		this.Identificacion=Identificacion;
		return this.Identificacion;
	}
	public void setIdentificacion(String identificacion) {
		Identificacion = identificacion;
	}
	public double getPagoMensual(double PagoMensual) {
		
		this.PagoMensual=PagoMensual;

		if  (this.PagoMensual<=500000){
			
			return	this.PagoMensual=this.PagoMensual+(this.PagoMensual*0.25);
			}else{
				return this.PagoMensual=this.PagoMensual+(this.PagoMensual*0.10);
			}
	}
	public void setPagoMensual(double pagoMensual) {
		PagoMensual = pagoMensual;
	}
	




}