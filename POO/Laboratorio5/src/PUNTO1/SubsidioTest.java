/*
 *NOMBRE DEL PROGRAMA : VALOR SUBSIDIO
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 16 SEP DE 2016
 */
package PUNTO1;

import java.io.*;

public class SubsidioTest {
	public static void main(String arg[]) throws IOException {
		// AQUI SE DEFINE LA VARIABLE PARA SABER SI SE REINICIARA EL PROGRAMA
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int reiniciar;
		String Identificacion;
		String Nombre;
		String Apellidos;
		double PagoSalario;
		// Creo mi objeto de tipo salario
		Subsidio ChequeMensual = new Subsidio();
		do {

			System.out.println("Bienvenido, este programa le dira su cheque mensual");
			// SE IMPRIMIRAN LAS OPCIONES PARA DESARROLLAR LAS OPERACIONES
			System.out.println("Ingrese n�mero de identificacion");
			Identificacion = in.readLine();
			System.out.println("Ingrese Nombre(s)");
			Nombre = in.readLine();
			System.out.println("Ingrese apellidos");
			Apellidos = in.readLine();
			System.out.println("Ingrese su sueldo, sin el signo $");
			PagoSalario = Double.parseDouble(in.readLine());
			while (PagoSalario <= 0) {
				System.out.println("Ingrese un sueldo valido:");
				PagoSalario = Double.parseDouble(in.readLine());
				if (PagoSalario > 0) {
					break;
				}
			}
			// Creo un if, el cual me permitira decidir que tipo de subsidio o
			// bono se debe crear
			if (PagoSalario <= 500000) {
				// IMPRIMO RESULTADOS USANDO EL GETTER DE CADA UNO DE MIS
				// ATRIBUTOS PRIVATE
				System.out.println(
						"El empleado " + ChequeMensual.getNombres(Nombre) + " " + ChequeMensual.getApellidos(Apellidos)
								+ " con numero identificacion " + ChequeMensual.getIdentificacion(Identificacion));
				System.out.println("Tiene un sueldo de $" + PagoSalario + " y con subsidio del 25% , su cheque sera $"
						+ ChequeMensual.getPagoMensual(PagoSalario));
			} else {
				System.out.println(
						"El empleado " + ChequeMensual.getNombres(Nombre) + " " + ChequeMensual.getApellidos(Apellidos)
								+ " con identificacion " + ChequeMensual.getIdentificacion(Identificacion));
				System.out.println("Tiene un sueldo de $" + PagoSalario + " y con bono del 10%  su cheque sera: $"
						+ ChequeMensual.getPagoMensual(PagoSalario));
			}
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = Integer.parseInt(in.readLine());
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = Integer.parseInt(in.readLine());
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
		System.out.println("FIN DEL PROGRAMA");

	}

}
