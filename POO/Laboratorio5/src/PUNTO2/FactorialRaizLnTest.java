/*
 *NOMBRE DEL PROGRAMA : FACTORIAL, RAIZ Y LOGARITMO
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 16 DE SEP DE 2016
 */
package PUNTO2;

import java.io.*;

public class FactorialRaizLnTest {
	public static void main(String arg[]) throws IOException {
		// AQUI SE DEFINE LA VARIABLE PARA SABER SI SE REINICIARA EL PROGRAMA
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int reiniciar;
		double numero;
		double numero1 = 0;
		int parte_entera;
		FactorialRaizLn valornum = new FactorialRaizLn();
		do {
			System.out.println("Bienvenido, este programa le dira el factorial, raiz cuadrada y logaritmo natural, de un n�mero");
			System.out.println("Ingrese el n�mero");
			numero = Double.parseDouble(in.readLine());
			parte_entera = (int) (numero);
			if (parte_entera != numero) {
				System.out.println("El factorial de " + numero + " no existe ");
				System.out.println("La raiz Cuadrada de " + numero + " es " + valornum.getRaiz_cuadrada(numero));
				System.out.println("El logaritmo natural es " + valornum.getLogaritmo_natural(numero));
			} else if (numero <= 0) {
				if (numero == 0) {
					System.out.println("El factorial de " + numero + " es " + valornum.getFactorial(numero));
					System.out.println("La raiz Cuadrada de " + numero + " es " + valornum.getRaiz_cuadrada(numero));
					System.out.println("El logaritmo natural de " + numero + " no existe");
					} else if (numero < 0) {
					// Igualo mi atributo numero al atributo numero1, puesto que
					// si hay raices imaginarias de negativos
					numero1 = numero;
					// Multiplico a numero1 * -1
					numero1 *= -1;
					// Llamo los diferentes gettes segun sea la raiz imaginaria
					System.out.println("El factorial de " + numero + " es " + " no existe");
					System.out.println(	"La raiz Cuadrada de " + numero + " es " + valornum.getRaiz_cuadrada(numero1) + "i");
					System.out.println("El logaritmo natural de " + numero + " no existe");

				}
			} else {
				if (numero > 170) {
					System.out.println("El factorial de " + numero + 
							" existe; pero Java ya no lo puede calcular por ser tan grande, si tu n�mero es igual o menor a 170, Java lo podra calcular");
					System.out.println("La raiz Cuadrada de " + numero + " es " + valornum.getRaiz_cuadrada(numero));
					System.out.println(
							"El logaritmo natural de " + numero + " es " + valornum.getLogaritmo_natural(numero));
				} else {
					System.out.println("El factorial de " + numero + " es " + valornum.getFactorial(numero));
					System.out.println("La raiz Cuadrada de " + numero + " es " + valornum.getRaiz_cuadrada(numero));
					System.out.println(
							"El logaritmo natural de " + numero + " es " + valornum.getLogaritmo_natural(numero));
				}

			}
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = Integer.parseInt(in.readLine());
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = Integer.parseInt(in.readLine());
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
		System.out.println("FIN DEL PROGRAMA");
	}

}
