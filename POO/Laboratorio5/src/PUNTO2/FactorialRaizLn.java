/*
 *NOMBRE DEL PROGRAMA : FACTORIAL, RAIZ Y LOGARITMO
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 16 DE SEP DE 2016
 */
package PUNTO2;

public class FactorialRaizLn {
	private double factorial;
	private double raiz_cuadrada;
	private double logaritmonatural;

	public double getFactorial(double factorial1) {
		double fact = 1;
		this.factorial = fact;
		for (double i = 1; i <= factorial1; i++) {
			this.factorial *= i;
		}
		return this.factorial;
	}

	public double getRaiz_cuadrada(double raiz) {
		this.raiz_cuadrada = Math.sqrt(raiz);
		return raiz_cuadrada;
	}

	public void setRaiz_cuadrada(double raiz_cuadrada) {
		this.raiz_cuadrada = raiz_cuadrada;
	}

	public double getLogaritmo_natural(double log) {
		this.logaritmonatural = Math.log(log);
		return logaritmonatural;
	}

	public void setLogaritmo_natural(double logaritmo_natural) {
		this.logaritmonatural = logaritmo_natural;
	}

}
