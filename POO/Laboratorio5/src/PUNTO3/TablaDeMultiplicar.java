/*
 *NOMBRE DEL PROGRAMA : TABLA DE MULTIPLICAR
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 16 DE SEP DE 2016
 */
package PUNTO3;

public class TablaDeMultiplicar {
	private double tablademultiplicar;

	public double getTablademultiplicar() {
		return tablademultiplicar;
	}

	public void setTabla_multiplicar(double tablademultiplicar, double cantidadfactores) {
		double producto = 1;
		this.tablademultiplicar = tablademultiplicar;
		for (double i = 1; i <= cantidadfactores; i++) {
			producto = this.tablademultiplicar * i;
			System.out.println(this.tablademultiplicar + " x " + i + " = " + producto);

		}
		producto = this.tablademultiplicar;
	}

}
