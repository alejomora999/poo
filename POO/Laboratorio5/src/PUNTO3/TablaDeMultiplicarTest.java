/*
 *NOMBRE DEL PROGRAMA : TABLA DE MULTIPLICAR
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 16 DE SEP DE 2016
 */
package PUNTO3;

import java.io.*;

public class TablaDeMultiplicarTest {
	public static void main(String arg[]) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		// // AQUI SE DEFINE LA VARIABLE PARA SABER SI SE REINICIARA EL PROGRAMA
		int reiniciar;
		double numero;
		double cantidad_factores;
		TablaDeMultiplicar productos = new TablaDeMultiplicar();
		do {
			System.out.println("Bienvenido, este programa calcula una tabla de multiplicar");
			System.out.println("Ingrese el n�mero");
			numero = Double.parseDouble(in.readLine());
			System.out.println("Ahora ingrese la cantidad de factores en la tabla");
			cantidad_factores = Double.parseDouble(in.readLine());
			while (cantidad_factores < 0) {
				System.out.println("Ingrese un numero valido");
				cantidad_factores = Double.parseDouble(in.readLine());
				if (cantidad_factores == 0) {
					break;
				}
			}
			if (cantidad_factores == 0) {
				System.out.println("LA TABLA DE MULTIPLICAR ES");
				System.out.println(numero + " x 0 = 0");
			} else {
				System.out.println("LA TABLA DE MULTIPLICAR ES");
				// Lamo al setter de mi clase, el cual realizara e imprimira mi
				// tabla
				productos.setTabla_multiplicar(numero, cantidad_factores);
			}

			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = Integer.parseInt(in.readLine());
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = Integer.parseInt(in.readLine());
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
		System.out.println("FIN DEL PROGRAMA");
	}

}
