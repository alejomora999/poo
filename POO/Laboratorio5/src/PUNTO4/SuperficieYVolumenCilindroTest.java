/*
 *NOMBRE DEL PROGRAMA : SUPERFICIE Y VOLUMEN TANQUE DE AGUA (CILINDRO)
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 16 DE SEP DE 2016
 */
package PUNTO4;

import java.io.*;

public class SuperficieYVolumenCilindroTest {
	public static void main(String arg[]) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		// // AQUI SE DEFINE LA VARIABLE PARA SABER SI SE REINICIARA EL PROGRAMA
		int reiniciar;
		double radio;
		double altura;
		SuperficieYVolumenCilindro TanqueDeAgua = new SuperficieYVolumenCilindro();
		do {
			System.out.println(
					"Bienvenido, este programa te dara la superficie y el volumen de un tanque de agua con forma cilindrica");
			System.out.println("Ingrese el radio");
			radio = Double.parseDouble(in.readLine());
			while (radio < 0) {
				System.out.println("Ingrese un radio valido, este no puede ser negativo");
				radio = Double.parseDouble(in.readLine());
				if (radio > 0) {
					break;
				}

			}
			System.out.println("Ingrese la altura");
			altura = Double.parseDouble(in.readLine());
			while (altura < 0) {
				System.out.println("Ingrese una altura valida, esta no puede ser negativa");
				altura = Double.parseDouble(in.readLine());
				if (altura > 0) {
					break;
				}

			}
			System.out.println("El  radio ingresado es " + radio + ", la  altura ingresada es " + altura);
			System.out.println("La superficie de su tanque es: " + TanqueDeAgua.getSuperficie(radio, altura)
					+ " unidades cuadradas");
			System.out.println(
					"La volumen de su tanque es: " + TanqueDeAgua.getVolumen(radio, altura) + " unidades cubicas");
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = Integer.parseInt(in.readLine());
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = Integer.parseInt(in.readLine());
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
		System.out.println("FIN DEL PROGRAMA");

	}

}
