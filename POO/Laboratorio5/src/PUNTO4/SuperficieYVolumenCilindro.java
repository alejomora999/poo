/*
 *NOMBRE DEL PROGRAMA : SUPERFICIE Y VOLUMEN CILINDRO TANQUE DE AGUA (CILINDRO)
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 16 DE SEP DE 2016
 */
package PUNTO4;

public class SuperficieYVolumenCilindro {
	private double superficie;
	private double volumen;

	public double getSuperficie(double radio, double altura) {
		this.superficie = ((2 * Math.PI * Math.pow(radio, 2)) + (2 * Math.PI * radio * altura));
		return this.superficie;
	}

	public void setSuperficie(double superficie) {
		this.superficie = superficie;
	}

	public double getVolumen(double radio, double altura) {
		this.volumen = (Math.PI * Math.pow(radio, 2) * altura);
		return this.volumen;
	}

	public void setVolumen(double volumen) {
		this.volumen = volumen;
	}

}
