package PUNTO1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Window.Type;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;

import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class Ventana_1 extends JFrame {
	private String Catch1 = (" ");
	private JPanel contentPane;
	public static JTextField txtEntrada;
	private ButtonGroup botonesentrada = new ButtonGroup();
	private ButtonGroup botonessalida = new ButtonGroup();

	public Ventana_1() {
		setBackground(Color.WHITE);
		setTitle("CONVERSOR DE BASES");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 468, 342);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("BASE DE ENTRADA");
		lblNewLabel.setFont(new Font("Times New Roman", Font.ITALIC, 11));
		lblNewLabel.setForeground(Color.DARK_GRAY);
		lblNewLabel.setBounds(10, 14, 102, 14);
		contentPane.add(lblNewLabel);

		JLabel lblBaseDeSalida = new JLabel("BASE DE SALIDA");
		lblBaseDeSalida.setForeground(Color.DARK_GRAY);
		lblBaseDeSalida.setFont(new Font("Times New Roman", Font.ITALIC, 11));
		lblBaseDeSalida.setBounds(170, 14, 91, 17);
		contentPane.add(lblBaseDeSalida);

		txtEntrada = new JTextField();
		txtEntrada.setBounds(309, 43, 114, 20);
		contentPane.add(txtEntrada);
		txtEntrada.setColumns(10);

		JLabel lblNmeroEnBase = new JLabel("N\u00DAMERO EN BASE");
		lblNmeroEnBase.setForeground(Color.DARK_GRAY);
		lblNmeroEnBase.setFont(new Font("Times New Roman", Font.ITALIC, 11));
		lblNmeroEnBase.setBounds(321, 11, 102, 14);
		contentPane.add(lblNmeroEnBase);

		JLabel lblSeleccionado = new JLabel("SELECCIONADO");
		lblSeleccionado.setForeground(Color.DARK_GRAY);
		lblSeleccionado.setFont(new Font("Times New Roman", Font.ITALIC, 11));
		lblSeleccionado.setBounds(321, 28, 102, 14);
		contentPane.add(lblSeleccionado);

		JRadioButton DecimalEntrada = new JRadioButton("DECIMAL");
		DecimalEntrada.setBounds(10, 42, 109, 23);
		contentPane.add(DecimalEntrada);

		JRadioButton BinarioEntrada = new JRadioButton("BINARIO");
		BinarioEntrada.setBounds(10, 68, 109, 23);
		contentPane.add(BinarioEntrada);

		JRadioButton OctalEntrada = new JRadioButton("OCTAL");
		OctalEntrada.setBounds(10, 94, 109, 23);
		contentPane.add(OctalEntrada);

		JRadioButton HexadecimalEntrada = new JRadioButton("HEXADECIMAL");
		HexadecimalEntrada.setBounds(10, 120, 109, 23);
		contentPane.add(HexadecimalEntrada);
		botonesentrada.add(HexadecimalEntrada);
		botonesentrada.add(BinarioEntrada);
		botonesentrada.add(OctalEntrada);
		botonesentrada.add(DecimalEntrada);
		JRadioButton DecimalSalida = new JRadioButton("DECIMAL");
		DecimalSalida.setBounds(152, 42, 109, 23);
		contentPane.add(DecimalSalida);

		JRadioButton BinarioSalida = new JRadioButton("BINARIO");
		BinarioSalida.setBounds(152, 68, 109, 23);
		contentPane.add(BinarioSalida);

		JRadioButton OctalSalida = new JRadioButton("OCTAL");
		OctalSalida.setBounds(152, 94, 109, 23);
		contentPane.add(OctalSalida);

		JRadioButton HexadecimalSalida = new JRadioButton("HEXADECIMAL");
		HexadecimalSalida.setBounds(152, 120, 109, 23);
		contentPane.add(HexadecimalSalida);
		botonessalida.add(BinarioSalida);
		botonessalida.add(HexadecimalSalida);
		botonessalida.add(OctalSalida);
		botonessalida.add(DecimalSalida);
		JLabel liamegen = new JLabel("");
		liamegen.setBounds(52, 162, 229, 112);
		contentPane.add(liamegen);
		JButton Convertir = new JButton("CONVERTIR");
		Convertir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					SeleccionarImagen seleccionar = new SeleccionarImagen();
					if (DecimalEntrada.isSelected() && BinarioSalida.isSelected()) {
						String decentrada;
						decentrada = txtEntrada.getText();
						if (decentrada.matches("[0-9]*")) {
							Conversor almacenar = new Conversor(decentrada, "z", "z", "z");
							almacenar.revisar(decentrada);
							ConversorBinario bi = new ConversorBinario();
							seleccionar.subcadena(bi.conversorBin());
							if (seleccionar.subcadena(bi.conversorBin()).equals("0")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/0.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);

							} else if (seleccionar.subcadena(bi.conversorBin()).equals("1")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/1.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							}
							JOptionPane.showMessageDialog(null, "El cambio es " + bi.conversorBin());
						} else if (decentrada.equals(null)) {
							JOptionPane.showMessageDialog(null, "La Base de Entrada no es Correcta");
						} else {
							JOptionPane.showMessageDialog(null,
									"Los caracteres ingresados no pueden ser convertidos, ingresalos nuevamente");
						}
					}
					if (DecimalEntrada.isSelected() && OctalSalida.isSelected()) {
						String decentrada;
						decentrada = txtEntrada.getText();
						if (decentrada.matches("[0-9]*")) {
							Conversor almacenar = new Conversor(decentrada, "z", "z", "z");
							almacenar.revisar(decentrada);
							ConversorOctal bi = new ConversorOctal();
							seleccionar.subcadena(bi.conversorOct());
							if (seleccionar.subcadena(bi.conversorOct()).equals("0")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/0.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);

							} else if (seleccionar.subcadena(bi.conversorOct()).equals("1")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/1.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorOct()).equals("2")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/2.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorOct()).equals("3")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/3.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorOct()).equals("4")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/4.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorOct()).equals("5")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/5.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorOct()).equals("6")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/6.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorOct()).equals("7")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/7.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							}
							JOptionPane.showMessageDialog(null, "El cambio es " + bi.conversorOct());
						} else if (decentrada.equals(null)) {
							JOptionPane.showMessageDialog(null, "La Base de Entrada no es Correcta");
						} else {
							JOptionPane.showMessageDialog(null,
									"Los caracteres ingresados no pueden ser convertidos, ingresalos nuevamente");
						}
					}
					if (DecimalEntrada.isSelected() && HexadecimalSalida.isSelected()) {
						String decentrada;
						decentrada = txtEntrada.getText();
						if (decentrada.matches("[0-9]*")) {
							Conversor almacenar = new Conversor(decentrada, "z", "z", "z");
							almacenar.revisar(decentrada);
							ConversorHexadecimal bi = new ConversorHexadecimal();
							seleccionar.subcadena(bi.conversorHex());
							if (seleccionar.subcadena(bi.conversorHex()).equals("0")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/0.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);

							} else if (seleccionar.subcadena(bi.conversorHex()).equals("1")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/1.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("2")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/2.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("3")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/3.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("4")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/4.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("5")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/5.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("6")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/6.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("7")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/7.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("8")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/8.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("9")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/9.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("a")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/A.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("b")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/B.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("c")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/C.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("d")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/D.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("e")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/E.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("f")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/F.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							}
							JOptionPane.showMessageDialog(null, "El cambio es " + bi.conversorHex());
						} else if (decentrada.equals(null)) {
							JOptionPane.showMessageDialog(null, "La Base de Entrada no es Correcta");
						} else {
							JOptionPane.showMessageDialog(null,
									"Los caracteres ingresados no pueden ser convertidos, ingresalos nuevamente");
						}
					}
					if (DecimalEntrada.isSelected() && DecimalSalida.isSelected()) {
						String decentrada;
						decentrada = txtEntrada.getText();
						if (decentrada.matches("[0-9]*")) {
							Conversor almacenar = new Conversor(decentrada, "z", "z", "z");
							almacenar.revisar(decentrada);
							ConversorDecimal bi = new ConversorDecimal();
							seleccionar.subcadena(bi.conversorDec());
							if (seleccionar.subcadena(bi.conversorDec()).equals("0")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/0.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);

							} else if (seleccionar.subcadena(bi.conversorDec()).equals("1")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/1.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("2")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/2.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("3")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/3.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("4")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/4.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("5")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/5.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("6")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/6.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("7")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/7.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("8")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/8.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("9")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/9.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							}
							JOptionPane.showMessageDialog(null, "El cambio es " + bi.conversorDec());
						} else if (decentrada.equals(null)) {
							JOptionPane.showMessageDialog(null, "La Base de Entrada no es Correcta");
						} else {
							JOptionPane.showMessageDialog(null,
									"Los caracteres ingresados no pueden ser convertidos, ingresalos nuevamente");
						}
					}
					if (BinarioEntrada.isSelected() && BinarioSalida.isSelected()) {
						String binentrada;
						binentrada = txtEntrada.getText();
						if (binentrada.matches("[0-1]*")) {
							Conversor almacenar1 = new Conversor("z", binentrada, "z", "z");
							almacenar1.revisar1(binentrada);
							ConversorBinario bi = new ConversorBinario();
							seleccionar.subcadena(bi.conversorBin());
							if (seleccionar.subcadena(bi.conversorBin()).equals("0")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/0.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);

							} else if (seleccionar.subcadena(bi.conversorBin()).equals("1")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/1.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							}
							JOptionPane.showMessageDialog(null, "El cambio es " + bi.conversorBin());
						} else if (binentrada.equals(null)) {
							JOptionPane.showMessageDialog(null, "La Base de Entrada no es Correcta");
						} else {
							JOptionPane.showMessageDialog(null,
									"Los caracteres ingresados no pueden ser convertidos, ingresalos nuevamente");
						}

					}
					if (BinarioEntrada.isSelected() && DecimalSalida.isSelected()) {
						String binentrada;
						binentrada = txtEntrada.getText();
						if (binentrada.matches("[0-1]*")) {
							Conversor almacenar1 = new Conversor("z", binentrada, "z", "z");
							almacenar1.revisar1(binentrada);
							ConversorDecimal bi = new ConversorDecimal();
							seleccionar.subcadena(bi.conversorDec());
							if (seleccionar.subcadena(bi.conversorDec()).equals("0")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/0.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);

							} else if (seleccionar.subcadena(bi.conversorDec()).equals("1")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/1.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("2")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/2.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("3")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/3.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("4")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/4.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("5")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/5.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("6")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/6.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("7")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/7.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("8")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/8.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("9")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/9.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							}
							JOptionPane.showMessageDialog(null, "El cambio es " + bi.conversorDec());
						} else if (binentrada.equals(null)) {
							JOptionPane.showMessageDialog(null, "La Base de Entrada no es Correcta");
						} else {
							JOptionPane.showMessageDialog(null,
									"Los caracteres ingresados no pueden ser convertidos, ingresalos nuevamente");
						}

					}
					if (BinarioEntrada.isSelected() && OctalSalida.isSelected()) {
						String binentrada;
						binentrada = txtEntrada.getText();
						if (binentrada.matches("[0-1]*")) {
							Conversor almacenar1 = new Conversor("z", binentrada, "z", "z");
							almacenar1.revisar1(binentrada);
							ConversorOctal bi = new ConversorOctal();
							seleccionar.subcadena(bi.conversorOct());
							if (seleccionar.subcadena(bi.conversorOct()).equals("0")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/0.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);

							} else if (seleccionar.subcadena(bi.conversorOct()).equals("1")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/1.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorOct()).equals("2")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/2.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorOct()).equals("3")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/3.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorOct()).equals("4")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/4.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorOct()).equals("5")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/5.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorOct()).equals("6")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/6.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorOct()).equals("7")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/7.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							}
							JOptionPane.showMessageDialog(null, "El cambio es " + bi.conversorOct());
						} else if (binentrada.equals(null)) {
							JOptionPane.showMessageDialog(null, "La Base de Entrada no es Correcta");
						} else {
							JOptionPane.showMessageDialog(null,
									"Los caracteres ingresados no pueden ser convertidos, ingresalos nuevamente");
						}

					}
					if (BinarioEntrada.isSelected() && HexadecimalSalida.isSelected()) {
						String binentrada;
						binentrada = txtEntrada.getText();
						if (binentrada.matches("[0-1]*")) {
							Conversor almacenar1 = new Conversor("z", binentrada, "z", "z");
							almacenar1.revisar1(binentrada);
							ConversorHexadecimal bi = new ConversorHexadecimal();
							seleccionar.subcadena(bi.conversorHex());
							if (seleccionar.subcadena(bi.conversorHex()).equals("0")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/0.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);

							} else if (seleccionar.subcadena(bi.conversorHex()).equals("1")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/1.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("2")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/2.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("3")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/3.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("4")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/4.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("5")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/5.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("6")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/6.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("7")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/7.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("8")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/8.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("9")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/9.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("a")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/A.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("b")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/B.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("c")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/C.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("d")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/D.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("e")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/E.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("f")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/F.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							}
							JOptionPane.showMessageDialog(null, "El cambio es " + bi.conversorHex());
						} else if (binentrada.equals(null)) {
							JOptionPane.showMessageDialog(null, "La Base de Entrada no es Correcta");
						} else {
							JOptionPane.showMessageDialog(null,
									"Los caracteres ingresados no pueden ser convertidos, ingresalos nuevamente");
						}

					}
					if (OctalEntrada.isSelected() && BinarioSalida.isSelected()) {
						String octentrada;
						octentrada = txtEntrada.getText();
						if (octentrada.matches("[0-7]*")) {
							Conversor almacenar = new Conversor("z", "z", octentrada, "z");
							almacenar.revisar2(octentrada);

							ConversorBinario bi = new ConversorBinario();
							seleccionar.subcadena(bi.conversorBin());

							if (seleccionar.subcadena(bi.conversorBin()).equals("0")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/0.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);

							} else if (seleccionar.subcadena(bi.conversorBin()).equals("1")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/1.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							}
							JOptionPane.showMessageDialog(null, "El cambio es " + bi.conversorBin());

						} else if (octentrada.equals(null)) {
							JOptionPane.showMessageDialog(null, "La Base de Entrada no es Correcta");
						} else {
							JOptionPane.showMessageDialog(null,
									"Los caracteres ingresados no pueden ser convertidos, ingresalos nuevamente");
						}

					}
					if (OctalEntrada.isSelected() && DecimalSalida.isSelected()) {
						String octentrada;
						octentrada = txtEntrada.getText();
						if (octentrada.matches("[0-7]*")) {
							Conversor almacenar = new Conversor("z", "z", octentrada, "z");
							almacenar.revisar2(octentrada);
							ConversorDecimal bi = new ConversorDecimal();
							seleccionar.subcadena(bi.conversorDec());
							if (seleccionar.subcadena(bi.conversorDec()).equals("0")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/0.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);

							} else if (seleccionar.subcadena(bi.conversorDec()).equals("1")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/1.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("2")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/2.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("3")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/3.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("4")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/4.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("5")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/5.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("6")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/6.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("7")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/7.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("8")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/8.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("9")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/9.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							}
							JOptionPane.showMessageDialog(null, "El cambio es " + bi.conversorDec());
							almacenar.revisar2(octentrada);
						} else if (octentrada.equals(null)) {
							JOptionPane.showMessageDialog(null, "La Base de Entrada no es Correcta");
						} else {
							JOptionPane.showMessageDialog(null,
									"Los caracteres ingresados no pueden ser convertidos, ingresalos nuevamente");
						}

					}
					if (OctalEntrada.isSelected() && OctalSalida.isSelected()) {
						String octentrada;
						octentrada = txtEntrada.getText();
						if (octentrada.matches("[0-7]*")) {
							Conversor almacenar = new Conversor("z", "z", octentrada, "z");
							almacenar.revisar2(octentrada);
							ConversorOctal bi = new ConversorOctal();
							seleccionar.subcadena(bi.conversorOct());
							if (seleccionar.subcadena(bi.conversorOct()).equals("0")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/0.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);

							} else if (seleccionar.subcadena(bi.conversorOct()).equals("1")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/1.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorOct()).equals("2")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/2.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorOct()).equals("3")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/3.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorOct()).equals("4")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/4.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorOct()).equals("5")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/5.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorOct()).equals("6")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/6.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorOct()).equals("7")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/7.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							}
							JOptionPane.showMessageDialog(null, "El cambio es " + bi.conversorOct());
							almacenar.revisar2(octentrada);
						} else if (octentrada.equals(null)) {
							JOptionPane.showMessageDialog(null, "La Base de Entrada no es Correcta");
						} else {
							JOptionPane.showMessageDialog(null,
									"Los caracteres ingresados no pueden ser convertidos, ingresalos nuevamente");
						}

					}
					if (OctalEntrada.isSelected() && HexadecimalSalida.isSelected()) {
						String octentrada;
						octentrada = txtEntrada.getText();
						if (octentrada.matches("[0-7]*")) {
							Conversor almacenar = new Conversor("z", "z", octentrada, "z");
							almacenar.revisar2(octentrada);
							ConversorHexadecimal bi = new ConversorHexadecimal();
							seleccionar.subcadena(bi.conversorHex());
							if (seleccionar.subcadena(bi.conversorHex()).equals("0")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/0.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);

							} else if (seleccionar.subcadena(bi.conversorHex()).equals("1")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/1.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("2")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/2.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("3")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/3.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("4")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/4.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("5")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/5.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("6")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/6.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("7")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/7.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("8")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/8.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("9")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/9.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("a")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/A.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("b")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/B.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("c")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/C.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("d")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/D.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("e")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/E.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("f")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/F.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							}
							JOptionPane.showMessageDialog(null, "El cambio es " + bi.conversorHex());
							almacenar.revisar2(octentrada);
						} else if (octentrada.equals(null)) {
							JOptionPane.showMessageDialog(null, "La Base de Entrada no es Correcta");
						} else {
							JOptionPane.showMessageDialog(null,
									"Los caracteres ingresados no pueden ser convertidos, ingresalos nuevamente");
						}

					}
					if (HexadecimalEntrada.isSelected() && BinarioSalida.isSelected()) {
						String hexentrada;
						hexentrada = txtEntrada.getText();
						if (hexentrada.matches("[0-9]+[a-f]") || hexentrada.matches("[0-9]*")
								|| hexentrada.matches("[a-f]*") || hexentrada.matches("[A-F]*")
								|| hexentrada.matches("[0-9]+[A-F]")) {
							Conversor almacenar = new Conversor("z", "z", "z", hexentrada);
							ConversorBinario bi = new ConversorBinario();
							almacenar.revisar3(hexentrada);
							seleccionar.subcadena(bi.conversorBin());
							if (seleccionar.subcadena(bi.conversorBin()).equals("0")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/0.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);

							} else if (seleccionar.subcadena(bi.conversorBin()).equals("1")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/1.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							}
							JOptionPane.showMessageDialog(null, "El cambio es " + bi.conversorBin());
						} else if (hexentrada.equals(null)) {
							JOptionPane.showMessageDialog(null, "La Base de Entrada no es Correcta");
						} else {
							JOptionPane.showMessageDialog(null,
									"Los caracteres ingresados no pueden ser convertidos, ingresalos nuevamente");
						}

					}
					if (HexadecimalEntrada.isSelected() && DecimalSalida.isSelected()) {
						String hexentrada;
						hexentrada = txtEntrada.getText();
						if (hexentrada.matches("[0-9]+[a-f]") || hexentrada.matches("[0-9]*")
								|| hexentrada.matches("[a-f]*") || hexentrada.matches("[A-F]*")
								|| hexentrada.matches("[0-9]+[A-F]")) {
							Conversor almacenar = new Conversor("z", "z", "z", hexentrada);
							ConversorDecimal bi = new ConversorDecimal();
							almacenar.revisar3(hexentrada);
							seleccionar.subcadena(bi.conversorDec());
							if (seleccionar.subcadena(bi.conversorDec()).equals("0")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/0.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);

							} else if (seleccionar.subcadena(bi.conversorDec()).equals("1")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/1.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("2")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/2.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("3")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/3.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("4")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/4.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("5")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/5.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("6")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/6.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("7")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/7.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("8")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/8.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorDec()).equals("9")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/9.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							}
							JOptionPane.showMessageDialog(null, "El cambio es " + bi.conversorDec());
						} else if (hexentrada.equals(null)) {
							JOptionPane.showMessageDialog(null, "La Base de Entrada no es Correcta");
						} else {
							JOptionPane.showMessageDialog(null,
									"Los caracteres ingresados no pueden ser convertidos, ingresalos nuevamente");
						}
					}
					if (HexadecimalEntrada.isSelected() && OctalSalida.isSelected()) {
						String hexentrada;
						hexentrada = txtEntrada.getText();
						if (hexentrada.matches("[0-9]+[a-f]") || hexentrada.matches("[0-9]*")
								|| hexentrada.matches("[a-f]*") || hexentrada.matches("[A-F]*")
								|| hexentrada.matches("[0-9]+[A-F]")) {
							Conversor almacenar = new Conversor("z", "z", "z", hexentrada);
							ConversorOctal bi = new ConversorOctal();
							almacenar.revisar3(hexentrada);
							seleccionar.subcadena(bi.conversorOct());
							if (seleccionar.subcadena(bi.conversorOct()).equals("0")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/0.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);

							} else if (seleccionar.subcadena(bi.conversorOct()).equals("1")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/1.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorOct()).equals("2")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/2.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorOct()).equals("3")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/3.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorOct()).equals("4")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/4.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorOct()).equals("5")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/5.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorOct()).equals("6")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/6.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorOct()).equals("7")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/7.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							}
							JOptionPane.showMessageDialog(null, "El cambio es " + bi.conversorOct());
						} else if (hexentrada.equals(null)) {
							JOptionPane.showMessageDialog(null, "La Base de Entrada no es Correcta");
						} else {
							JOptionPane.showMessageDialog(null,
									"Los caracteres ingresados no pueden ser convertidos, ingresalos nuevamente");
						}

					}
					if (HexadecimalEntrada.isSelected() && HexadecimalSalida.isSelected()) {
						String hexentrada;
						hexentrada = txtEntrada.getText();
						if (hexentrada.matches("[0-9]+[a-f]") || hexentrada.matches("[0-9]*")
								|| hexentrada.matches("[a-f]*") || hexentrada.matches("[A-F]*")
								|| hexentrada.matches("[0-9]+[A-F]")) {
							Conversor almacenar = new Conversor("z", "z", "z", hexentrada);
							ConversorHexadecimal bi = new ConversorHexadecimal();
							almacenar.revisar3(hexentrada);
							seleccionar.subcadena(bi.conversorHex());
							if (seleccionar.subcadena(bi.conversorHex()).equals("0")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/0.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);

							} else if (seleccionar.subcadena(bi.conversorHex()).equals("1")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/1.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("2")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/2.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("3")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/3.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("4")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/4.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("5")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/5.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("6")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/6.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("7")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/7.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("8")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/8.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("9")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/9.jpg"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("a")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/A.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("b")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/B.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("c")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/C.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("d")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/D.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("e")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/E.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							} else if (seleccionar.subcadena(bi.conversorHex()).equals("f")) {
								ImageIcon caracter = new ImageIcon(getClass().getResource("/imagenes/F.png"));
								ImageIcon icono = new ImageIcon(caracter.getImage().getScaledInstance(
										liamegen.getWidth(), liamegen.getHeight(), Image.SCALE_DEFAULT));
								liamegen.setIcon(icono);
							}
							JOptionPane.showMessageDialog(null, "El cambio es " + bi.conversorHex());
						} else if (hexentrada.equals(null)) {
							JOptionPane.showMessageDialog(null, "La Base de Entrada no es Correcta");
						} else {
							JOptionPane.showMessageDialog(null,
									"Los caracteres ingresados no pueden ser convertidos, ingresalos nuevamente");
						}

					}

				}

				catch (java.lang.NumberFormatException e) {
					// e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Puedes ingresar maximo diez digitos ");

				} catch (java.lang.StringIndexOutOfBoundsException e) {
					// e.printStackTrace();
					JOptionPane.showMessageDialog(null, "El campo de entrada est� vacio ");

				}
			}

		});
		Convertir.setBounds(309, 94, 102, 50);
		contentPane.add(Convertir);

		JButton Salir = new JButton("SALIR");
		Salir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		Salir.setBounds(353, 269, 89, 23);
		contentPane.add(Salir);

	}
}
