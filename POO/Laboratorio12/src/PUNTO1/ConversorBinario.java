package PUNTO1;

public class ConversorBinario extends Conversor {
	private int i;

	public ConversorBinario() {
		super(Base_Entrada, Base_Entrada1, Base_Entrada2, Base_Entrada3);

	}

	String conversorBin() {
		if (Base_Entrada.matches("[0-9]*")) {
			i = Integer.parseInt(Base_Entrada);
			String bin = Integer.toBinaryString(i);
			return bin;
		} else if (Base_Entrada3.matches("[0-9]+[a-f]") || Base_Entrada3.matches("[0-9]*")
				|| Base_Entrada3.matches("[a-f]*") || Base_Entrada3.matches("[A-F]*")
				|| Base_Entrada3.matches("[0-9]+[A-F]")) {
			i = Integer.parseInt(Base_Entrada3, 16);
			String bin1 = Integer.toBinaryString(i);
			return bin1;
		} else if (Base_Entrada1.matches("[0-1]*")) {
			String bin2 = Base_Entrada1;
			return bin2;

		} else {
			i = Integer.parseInt(Base_Entrada2, 8);
			String bin3 = Integer.toBinaryString(i);
			return bin3;
		}

	}
}
