package PUNTO1;

public class ConversorDecimal extends Conversor {
	private int i = 0;

	public ConversorDecimal() {
		super(Base_Entrada, Base_Entrada1, Base_Entrada2, Base_Entrada3);

	}

	String conversorDec() {
		if (Base_Entrada.matches("[0-9]*")) {
			String dec = a;
			return dec;
		} else if (Base_Entrada3.matches("[0-9]+[a-f]") || Base_Entrada3.matches("[0-9]*")
				|| Base_Entrada3.matches("[a-f]*") || Base_Entrada3.matches("[A-F]*")
				|| Base_Entrada3.matches("[0-9]+[A-F]")) {
			i = Integer.parseInt(Base_Entrada3, 16);
			String dec1 = Integer.toString(i);
			return dec1;
		} else if (Base_Entrada1.matches("[0-1]*")) {
			i = Integer.parseInt(Base_Entrada1, 2);
			String dec2 = Integer.toString(i);
			return dec2;

		} else {
			i = Integer.parseInt(Base_Entrada2, 8);
			String dec3 = Integer.toString(i);
			return dec3;
		}

	}
}
