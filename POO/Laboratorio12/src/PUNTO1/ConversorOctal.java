package PUNTO1;

public class ConversorOctal extends Conversor {
	private int i = 0;

	public ConversorOctal() {
		super(Base_Entrada, Base_Entrada1, Base_Entrada2, Base_Entrada3);

	}

	String conversorOct() {
		if (Base_Entrada.matches("[0-9]*")) {
			int i = Integer.parseInt(Base_Entrada);
			String oct = Integer.toOctalString(i);
			return oct;
		} else if (Base_Entrada3.matches("[0-9]+[a-f]") || Base_Entrada3.matches("[0-9]*")
				|| Base_Entrada3.matches("[a-f]*") || Base_Entrada3.matches("[A-F]*")
				|| Base_Entrada3.matches("[0-9]+[A-F]")) {
			i = Integer.parseInt(Base_Entrada3, 16);
			String oct1 = Integer.toOctalString(i);
			return oct1;
		} else if (Base_Entrada1.matches("[0-1]*")) {
			i = Integer.parseInt(Base_Entrada1, 2);
			String oct2 = Integer.toOctalString(i);
			return oct2;

		} else {
			String oct3 = Base_Entrada2;
			return oct3;
		}

	}
}
