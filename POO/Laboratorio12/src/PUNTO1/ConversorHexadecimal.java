package PUNTO1;

public class ConversorHexadecimal extends Conversor {
	private int i = 0;

	public ConversorHexadecimal() {
		super(Base_Entrada, Base_Entrada1, Base_Entrada2, Base_Entrada3);

	}

	String conversorHex() {
		if (Base_Entrada.matches("[0-9]*")) {
			int i = Integer.parseInt(Base_Entrada);
			String hex = Integer.toHexString(i);
			return hex;
		} else if (Base_Entrada3.matches("[0-9]+[a-f]") || Base_Entrada3.matches("[0-9]*")
				|| Base_Entrada3.matches("[a-f]*") || Base_Entrada3.matches("[A-F]*")
				|| Base_Entrada3.matches("[0-9]+[A-F]")) {
			String hex1 = Base_Entrada3;
			return hex1;
		} else if (Base_Entrada1.matches("[0-1]*")) {
			i = Integer.parseInt(Base_Entrada1, 2);
			String hex2 = Integer.toHexString(i);
			return hex2;

		} else {
			i = Integer.parseInt(Base_Entrada2, 8);
			String hex3 = Integer.toHexString(i);
			return hex3;
		}

	}

}
