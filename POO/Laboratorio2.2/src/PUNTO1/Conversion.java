/*
 * NOMBRE: CONVERSION DE BINARIOS 
 * AUTOR: NICOLAS MENESES GUERRERO
 * FECHA: 6 SEP 2026
 */
package PUNTO1;

import java.io.*;

public class Conversion {

public static void main(String arg[]) throws IOException{
	//SE INDICARA QUE SU RECIBIRA LECTURA POR TECLADO
	BufferedReader in= new BufferedReader(new InputStreamReader(System.in));
	//AQUI SE DEFINE LA VARIABLE PARA SABER SI SE REINICIARA EL PROGRAMA
	int reiniciar;
	// SE DEFINE LA VARIABLE PARA SABER QUE CONVERSION SE HARA
	int procedimiento;
	// DEFINO VARIABLES DE ENTRADA PARA ALMACENAR LAS CONVERSIONES
	String numbin, numoct, numhexa;
	// SE DEFINE VARIABLE PARA CONVERTIRSE A VARIOS SISTEMAS
	int num;
	// DO WHILE PARA REPETIR PROGRAMA
	do{
		//SE IMPRIMIRAN LAS OPCIONES PARA DESARROLLAR LAS CONVERSIONES
		System.out.println("Ingrese un numero para saber que procedimiento realizar: ");
		//1 binario a octal 
		System.out.println("Si digita 1 se convertira de binario a octal: ");
		//2 octal a hexadecimal
		System.out.println("Si digita 2 se convertira de octal a hexadecimal: ");
		//3 binario a hexadecimal
		System.out.println("Si digita 3 se convertira de binario a hexadecimal: ");
		//4 octal a binario 
		System.out.println("Si digita 4 se convertira de octal a binario: ");
		//5 hexadecimal a octal
		System.out.println("Si digita 5 se convertira de hexadecimal a octal: ");
		//6 hexadecimal a binario
		System.out.println("Si digita 6 se convertira de hexadecimal a binario: ");
		procedimiento=Integer.parseInt(in.readLine());
		switch (procedimiento){
		case 1:
		// el usuario debe digitar un numero binario para realizar la conversion
		System.out.print("ha elegido la opcion de convertir de binario a octal ");
		System.out.print("ingrese el numero en binario ");
		// el valor se almacenara en una variable
		numbin=(in.readLine());
		// los while determinaran la validez del dato
		while (numbin.matches("[2-9]*") || numbin.matches("[a-z]*")){
			System.out.print("El dato no es valido, ingreselo otra vez ");
			numbin= (in.readLine());
		}
		while (numbin.matches("[2-9+[a-z]]")){
			System.out.print("El dato no es valido, ingreselo otra vez ");
			numbin= (in.readLine());
		}
		if (numbin.matches("[0-1]*")){
			// si el dato es valido se realizara la conversion 
			num =Integer.parseInt(numbin, 2);
			numoct= Integer.toOctalString(num);
			System.out.println("El numero en octal es "+ numoct);
			break;
		}
		break;
		case 2:
		// el usuario debe digitar un numero en octal
	    System.out.println("Ha elegido la opcion de convertir de octal a hexadecimal ");
	    System.out.println("Ingrese el numero en octal");
	    // el valor se almacenara en una variable 
	    numoct=(in.readLine());
	    //los while determinara la validez del dato
	    while(numoct.matches("[8-9]*") || numoct.matches("[a-z]*")){
	    	System.out.print("El dato no es valido, ingreselo otra vez ");
			numoct= (in.readLine());
	    }
	    while (numoct.matches("[0-9]+[a-z]")){
	    	System.out.print("El dato no es valido, ingreselo otra vez ");
			numoct= (in.readLine());
	    }
	    if (numoct.matches("[0-7]*")){
	    	//si el dato es valido se realizara la conversion
	    	num =Integer.parseInt(numoct, 8);
			numhexa= Integer.toOctalString(num);
			System.out.println("El numero en hexadecimal es "+ numhexa);
			break;
	    }
	    break;
		case 3:
			// el usuario debe digitar un numero en binario
			System.out.println("Ha elegido la opcion de convertir de binario a hexadecimal ");
			System.out.println("Ingrese el numero en binario");
			// El valor se almacenara en una variable
			numbin = (in.readLine());
			//los while determinaran la validez del dato
			while (numbin.matches("[2-9]*") || numbin.matches("[a-z]*")) {
				System.out.println("Su dato es invalido, ingreselo otra vez ");
				numbin = (in.readLine());
			}
			while (numbin.matches("[2-9]+[a-z]")) {
				System.out.println("el dato es invalido, ingreselo otra vez ");
				numbin = (in.readLine());
			}
			if (numbin.matches("[0-1]*")) {
				// si el dato es valido realizo la conversion
				num = Integer.parseInt(numbin, 2);
				numhexa = Integer.toHexString(num);
				System.out.println(" el numero en hexadecimal es " + numhexa);
				break;
			}
			break;
		case 4:
			// el usuario debe digitar un numero en octal
			System.out.println("Ha elegido la opcion de convertir de octal a binario ");
			System.out.println("Ingrese el numero en octal ");
			// El valor se almacenara en una variable
			numoct = (in.readLine());
			// los while determinaran la validez del dato
			while (numoct.matches("[8-9]*") || numoct.matches("[a-z]*")) {
				System.out.println("el dato es invalido, ingreselo otra vez ");
				numoct = (in.readLine());
			}
			while (numoct.matches("[0-9]+[a-z]")) {
				System.out.println("el dato es invalido, ingreselo otra vez ");
				numoct = (in.readLine());
			}

			if (numoct.matches("[0-7]*")) {
				// si el dato es valido se realizara la conversion
				num = Integer.parseInt(numoct, 8);
				numbin = Integer.toBinaryString(num);
				System.out.println(" el numero en binario es " + numbin);
				break;
			}
			break;
		case 5:
			//el usuario debe digitar un numero en octal un numero en octal
			System.out.println("Ha elegido la opcion de convertir  de hexadecimal a octal ");
			System.out.println("Ingrese el numero en hexadecimal ");
			// El valor se almacenara en la siguiente variable
			numhexa = (in.readLine());
			// los while determinaran la validez del dato
			while (numhexa.matches("[g-z]*") || numhexa.matches("[G-Z]*")) {
				System.out.println("el dato es invalido, ingreselo otra vez ");
				numhexa = (in.readLine());
			}
			while (numhexa.matches("[0-9]+[g-z]") || numhexa.matches("[0-9]+[G-Z]")) {
				System.out.println("el dato es invalido, ingreselo otra vez ");
				numhexa = (in.readLine());
			}

			if (numhexa.matches("[0-9]+[a-f]") || numhexa.matches("[0-9]*")
					|| numhexa.matches("[a-f]*") || numhexa.matches("[A-F]*")||numhexa.matches("[0-9]+[A-F]")) {
				// si el dato es valido realizo la conversion
				num = Integer.parseInt(numhexa, 16);
				numoct = Integer.toOctalString(num);
				System.out.println(" el numero en octal es " + numoct);
				break;
			}
			break;
		case 6:
			// el usuario debe digitar un numero en octal
			System.out.println("Ha elegido la opcion de convertir de hexadecimal a binario ");
			System.out.println("Ingrese el numero en hexadecimal ");
			// El valor se almacenara en una vaiable 
			numhexa = (in.readLine());
			// los while determinaran la validez del dato
			while (numhexa.matches("[g-z]*") || numhexa.matches("[G-Z]*")) {
				System.out.println("el dato es invalido, ingreselo otra vez ");
				numhexa = (in.readLine());
			}
			while (numhexa.matches("[0-9]+[g-z]") || numhexa.matches("[0-9]+[G-Z]")) {
				System.out.println("el dato es invalido, ingreselo otra vez ");
				numhexa = (in.readLine());
			}

			if (numhexa.matches("[0-9]+[a-f]") || numhexa.matches("[0-9]*")
					|| numhexa.matches("[a-f]*") || numhexa.matches("[A-F]*")||numhexa.matches("[0-9]+[A-F]")) {
				// Si el dato es valido se realizara la conversion
				num = Integer.parseInt(numhexa, 16);
				numbin = Integer.toBinaryString(num);
				System.out.println(" el numero en binario es :" + numbin);
				break;
			}
			break;
		default: 
			// si se selecciono una opcion no citada en el menu, salir der switch eimprimir este aviso
			System.out.println("Ha elegido una opcion diferente a las que aparecen en el menu ");
			break;
			}
		System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
		reiniciar = Integer.parseInt(in.readLine());
		// El siguiente While, acompa�ado de un If me permite saber si la
		// opcion ingresada es correcta, de no serlo la exigira nuevamente
		while (reiniciar != 1) {
			if (reiniciar != 0) {
				System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no ");
				reiniciar = Integer.parseInt(in.readLine());
			} else {
				break;
			}
		}
}   while (reiniciar != 0);    
	System.out.println("FIN DEL PROGRAMA");

}
}


