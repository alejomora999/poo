/*
 * NOMBRE: OPERACION DE BINARIOS 
 * AUTOR: NICOLAS MENESES GUERRERO
 * FECHA: 6 SEP 2026
 */
package PUNTO2;
import java.io.*;
public class OperacionDeBinarios {
	public static void main(String arg[]) throws IOException {
		//SE INDICARA QUE SU RECIBIRA LECTURA POR TECLADO
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		//AQUI SE DEFINE LA VARIABLE PARA SABER SI SE REINICIARA EL PROGRAMA
		int reiniciar;
		// SE DEFINE LA VARIABLE PARA SABER QUE CONVERSION SE HARA
		int procedimiento;
		// DEFINO VARIABLES DE ENTRADA PARA ALMACENAR LAS OPERACIONES
		String numbin1, numbin2, resulbin;
		// SE DEFINEN VARIABLES PARA CONVERTIRSE A VARIOS SISTEMAS
		int num1, num2;
		// SE DEFINEN LAS VARIABLES PARA ALMACENAR RESULTADOS
		int resuloper;
		// DO WHILE PARA REPETIR PROGRAMA
		do {
			//SE IMPRIMIRAN LAS OPCIONES PARA DESARROLLAR LAS OPERACIONES
			System.out.println("Ingrese un numero para realizar una operacion");
			System.out.println("Si ingresa 1 se sumaran dos binarios ");
			System.out.println("Si ingresa 2 se restaran dos binarios ");						
			System.out.println("Si ingresa 3 se multiplicaran dos binarios ");
			System.out.println("Si ingresa 4 se dividiran dos binarios ");
			procedimiento = Integer.parseInt(in.readLine());
			switch (procedimiento) {
			case 1:
				System.out.println("Ha elegido la opcion de sumar dos binarios :");
				System.out.println("ingrese el primer binario :");
				// El valor se almacenara en esta variable
				numbin1 = (in.readLine());
				//los while determinaran la validez del numero
				while (numbin1.matches("[2-9]*") || numbin1.matches("[a-z]*")) {
					System.out.println("Su numero es invalido, ingreselo otra vez ");
					numbin1 = (in.readLine());
				}
				while (numbin1.matches("[2-9]+[a-z]")) {
					System.out.println("Su numero es invalido, ingreselo otra vez");
					numbin1 = (in.readLine());
				}				
				System.out.println("ingrese el segundo binario :");
				// El valor se almacenara en esta variable
				numbin2 = (in.readLine());
				// los while determinaran la validez del numero
				while (numbin2.matches("[2-9]*") || numbin2.matches("[a-z]*")) {
					System.out.println("Su numero es invalido, ingreselo otra vez ");
					numbin2 = (in.readLine());
				}
				while (numbin2.matches("[2-9]+[a-z]")) {
					System.out.println("Su numero es invalido, ingreselo otra vez ");
					numbin2 = (in.readLine());
				}

				if (numbin1.matches("[0-1]*")) {
					if(numbin2.matches("[0-1]*")){
						// Si el dato es valido realizo la  operacion
						num1 = Integer.parseInt(numbin1, 2);
						num2 = Integer.parseInt(numbin2, 2);
						resuloper=num1+num2;
						resulbin= Integer.toBinaryString(resuloper);
						System.out.println("tu suma es: "+resulbin);
					}
					break;
				}
				break;
			case 2:
				System.out.println("Ha elegido la opcion de restar dos binarios :");
				System.out.println("ingrese el primer binario :");
				// El valor se almacenara en una variable
				numbin1 = (in.readLine());
				// los while determinaran la validez del numero
				while (numbin1.matches("[2-9]*") || numbin1.matches("[a-z]*")) {
					System.out.println("Su numero es invalido, ingreselo otra vez ");
					numbin1 = (in.readLine());
				}
				while (numbin1.matches("[2-9]+[a-z]")) {
					System.out.println("Su numero es invalido, ingreselo otra vez ");
					numbin1 = (in.readLine());
				}				
				System.out.println("ingrese el segundo binario :");
				// El valor se almacenar aen una variable
				numbin2 = (in.readLine());
				// los whule determinaran la validez del numero
				while (numbin2.matches("[2-9]*") || numbin2.matches("[a-z]*")) {
					System.out.println("Su numero es invalido, ingreselo otra vez");
					numbin2 = (in.readLine());
				}
				while (numbin2.matches("[2-9]+[a-z]")) {
					System.out.println("Su numero es invalido, ingreselo otra vez");
					numbin2 = (in.readLine());
				}

				if (numbin1.matches("[0-1]*")) {
					if(numbin2.matches("[0-1]*")){
						// Si el dato es valido realizo la operacion
						num1 = Integer.parseInt(numbin1, 2);
						num2 = Integer.parseInt(numbin2, 2);
						resuloper=num1-num2;
						resulbin= Integer.toBinaryString(resuloper);
						System.out.println("tu resta es: "+resulbin);
					}
					break;
				}
				break;
			case 3:
				System.out.println("Ha elegido la opcion de multiplicar dos binarios :");
				System.out.println("ingrese el primer binario :");
				// el valor se almacenara en una variable
				numbin1 = (in.readLine());
				// los while determinaran la validez del numero 
				while (numbin1.matches("[2-9]*") || numbin1.matches("[a-z]*")) {
					System.out.println("Su numero es invalido, ingreselo otra vez");
					numbin1 = (in.readLine());
				}
				while (numbin1.matches("[2-9]+[a-z]")) {
					System.out.println("Su numero es invalido, ingreselo otra vez");
					numbin1 = (in.readLine());
				}				
				System.out.println("ingrese el segundo binario :");
				// el valor se almacenara en una variable
				numbin2 = (in.readLine());
				// los while determinaran la validez del numero
				while (numbin2.matches("[2-9]*") || numbin2.matches("[a-z]*")) {
					System.out.println("Su numero es invalido, ingreselo otra vez ");
					numbin2 = (in.readLine());
				}
				while (numbin2.matches("[2-9]+[a-z]")) {
					System.out.println("Su numero es invalido, ingreselo otra vez ");
					numbin2 = (in.readLine());
				}

				if (numbin1.matches("[0-1]*")) {
					if(numbin2.matches("[0-1]*")){
						// si el dato es valido realizo la operacion
						num1 = Integer.parseInt(numbin1, 2);
						num2 = Integer.parseInt(numbin2, 2);
						resuloper=num1*num2;
						resulbin= Integer.toBinaryString(resuloper);
						System.out.println("tu producto es: "+resulbin);
					}
					break;
				}
				break;
			case 4:
				System.out.println("Ha elegido la opcion de dividir dos binarios :");
				System.out.println("ingrese el primer binario :");
				// el valor se almacenara en una variable
				numbin1 = (in.readLine());
				// los while determinaran la validez del numero
				while (numbin1.matches("[2-9]*") || numbin1.matches("[a-z]*")) {
					System.out.println("Su numero es invalido, ingreselo otra vez");
					numbin1 = (in.readLine());
				}
				while (numbin1.matches("[2-9]+[a-z]")) {
					System.out.println("Su numero es invalido, ingreselo otra vez");
					numbin1 = (in.readLine());
				}				
				System.out.println("ingrese el segundo binario :");
				// el valor se almacenara en una variable
				numbin2 = (in.readLine());
				// los while determinaran la validez del numero
				while (numbin2.matches("[2-9]*") || numbin2.matches("[a-z]*")) {
					System.out.println("Su numero es invalido, ingreselo otra vez ");
					numbin2 = (in.readLine());
				}
				while (numbin2.matches("[2-9]+[a-z]")) {
					System.out.println("Su numero es invalido, ingreselo otra vez");
					numbin2 = (in.readLine());
				}
				while(numbin2.matches("[0-1]*")){
					num2 = Integer.parseInt(numbin2, 2);
					if(num2==0){
						System.out.println("el segundo binario no puede ser cero, ingreselo otra vez ");
						numbin2 = (in.readLine());
						num2 = Integer.parseInt(numbin2, 2);
					}
					break;
				}

				if (numbin1.matches("[0-1]*")) {
					if(numbin2.matches("[0-1]*")){
						// si el dato es valido realizola operacion
						num1 = Integer.parseInt(numbin1, 2);
						num2 = Integer.parseInt(numbin2, 2);
						resuloper=num1/num2;
						resulbin= Integer.toBinaryString(resuloper);
						System.out.println("tu division es: "+resulbin);
					}
					break;
				}
				break;
			default:
				// si se selecciono una opcion no citada en el menu, salir del switch e imprimir este aviso
				System.out.println("Ha elegido una opcion diferente a las que aparecen en el menu ");
				break;
			
			}
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = Integer.parseInt(in.readLine());
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no ");
					reiniciar = Integer.parseInt(in.readLine());
				} else {
					break;
				}
			}
		}while (reiniciar != 0);
		System.out.println("FIN DEL PROGRAMA");
		
	}

}

