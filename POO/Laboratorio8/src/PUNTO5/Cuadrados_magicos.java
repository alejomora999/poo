/*
 *NOMBRE DEL PROGRAMA : CUADRADOS MAGICOS
 * AUTOR: ALEJANDRO MORALES MOJICA
 *FECHA: 7 DE OCT DE 2016
 */
package PUNTO5;

import java.util.Scanner;
import java.util.Random;

public class Cuadrados_magicos {
	private static Scanner entrada;

	public static void main(String arg[]) {
		entrada = new Scanner(System.in);

		// declarando variables locales
		int menu;
		int primervalorc;
		int opc;

		// ciclo para ejecutar el codigo dentro de el segun condicion al final
		do {
			Random rnd = new Random();
			System.out.println(" BIENVENIDO ESTE PROGRAMA RESUELVE CUADRADOS MAGICOS DE 3X3 O 4X4.");
			System.out.println("Ingrese 1 para que elcuadrado sea de 3x3.");
			System.out.println("Ingrese 1 para que elcuadrado sea de 4x4");
			System.out.print("Opci�n: ");
			// ejeplifica variable menu para saber que hacer
			menu = entrada.nextInt();

			// valida que menu este dentro del rango de los mensajes
			// si no lo esta, pide otro valor hasta que lo este
			if (menu < 1 || menu > 2) {
				while (menu < 1 || menu > 2) {
					System.out.println("Opci�n invalida, ingrese otra: ");
					menu = entrada.nextInt();
				}
			}

			// Metdo de seleccion para ver que hacer segun variable menu
			switch (menu) {
			// cuando menu es uno se procede al desarrollo de cuadrados3x3
			case 1:
				System.out.println("Cuadrado m�gico 3x3: ");
				System.out.println("Ingrese el valor menor del cuadrado: ");
				// ejemplifica el valor de menor tama�o del cuadrado

				primervalorc = (int) (rnd.nextDouble() * 20);
				System.out.println("El valor minimo del cuadrado es: " + primervalorc);
				// DECLARANDO OBJETO
				Cuadrado3x3 elcuadrado = new Cuadrado3x3(3);

				// llamando funciones, la primera resuelve el cuadrado, la
				// segunda lo imprime
				elcuadrado.Resolviendo3x3(primervalorc);
				elcuadrado.mostrarcuadrado();
				break;
			case 2:
				System.out.println("Cuadrado magico 4x4: ");
				// System.out.println("Ingrese el valor menor del cuadrado: ");

				// ejemplifica el valor de menor tama�o del cuadrado

				primervalorc = (int) (rnd.nextDouble() * 20);
				System.out.println("El valor minimo del cuadrado es: " + primervalorc);
				// DECLARANDO EL OBJETO
				Cuadrado4x4 elotrocuadrado = new Cuadrado4x4(4);

				// llamando funciones, la primera resuelve el cuadrado, la
				// segunda lo imprime
				elotrocuadrado.Resolviendo4x4(primervalorc);
				elotrocuadrado.mostrar4x4();

				break;
			}
			System.out.println("");
			System.out.println("�Desea crear otro cuadrado m�gico? Si=1 No=2");
			// ejemplifica variable opc para saber si seguir con el programa
			opc = entrada.nextInt();
			if (opc < 1 || opc > 2) {
				while (opc < 1 || opc > 2) {
					System.out.println("Opci�n invalida, ingrese otra: ");
					opc = entrada.nextInt();
				}
			}

			// condicion para volver a ejecutar el codigo dentro del ciclo
		} while (opc == 1);

	}
}
