/*
 *NOMBRE DEL PROGRAMA : CUADRADOS MAGICOS
 * AUTOR: ALEJANDRO MORALES MOJICA
 *FECHA: 7 DE OCT DE 2016
 */
package PUNTO5;

public class Cuadrado4x4 {
	private int[][] matriz;
	private int algo = 0;

	public Cuadrado4x4(int algo) {
		// inicializa el tama�o de la matriz en 4
		this.matriz = new int[4][4];
		// inicializa atributo con el menor valor del cuadrado mandado desde el
		// main
		this.algo = algo;
	}

	// metodo que soluciona el cuadrado

	public void Resolviendo4x4(int algo2) {
		// llena el cuadrado con los numeros normalmente en forma ascendente
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				this.matriz[i][j] = algo2;
				// System.out.println(algo2);
				algo2++;
			}
		}
		// con el cuadrado llenado se empieza a modificar intercambiando los
		// extremos de forma diagonal
		// tambien intercambiando de forma diagonal el cuadrado interior del
		// cuadrado
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (i == 0 && j == 0) {
					this.matriz[i][j] = algo2 - 1;
				} else {
					if (i == 0 && j == this.algo - 1) {
						this.matriz[i][j] += 9;
					} else {
						if (i == this.algo - 1 && j == 0) {
							this.matriz[i][j] -= 9;
						} else {
							if (i == this.algo - 1 && j == this.algo - 1) {
								this.matriz[i][j] -= 15;

							} else {
								if (i == 1 && j == 1) {
									this.matriz[i][j] += 5;
								} else {
									if (i == 1 && j == 2) {
										this.matriz[i][j] += 3;
									} else {
										if (i == 2 && j == 1) {
											this.matriz[i][j] -= 3;
										} else {
											if (i == 2 && j == 2) {
												this.matriz[i][j] -= 5;
											} else {
												this.matriz[i][j] = this.matriz[i][j];
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	// mostrar cuadrado magico
	public void mostrar4x4() {
		System.out.println("Cuadrado m�gico: ");
		for (int i = 0; i < 4; i++) {
			System.out.println("");
			for (int j = 0; j < 4; j++) {
				System.out.print(this.matriz[i][j] + "       ");
			}
		}
	}

}
