/*
 *NOMBRE DEL PROGRAMA : CUADRADOS MAGICOS
 * AUTOR: NALEJANDRO MORALES MOJICA
 *FECHA: 7 DE OCT DE 2016
 */
package PUNTO5;

public class Cuadrado3x3 {
	private int matriz[][];
	private int algo;

	public Cuadrado3x3(int algo) {
		// inicializando el tama�o de la matriz
		this.matriz = new int[3][3];
		// inicializando atributo algo con valor mandado desde el main, menor
		// valor del cuadrado magico
		this.algo = algo;
	}

	// metodo que crea el cuadrado
	public void Resolviendo3x3(int algo) {
		// declarando la mitad de las filas para empezar desde ahi
		int mitad = this.algo / 2;
		// declarando la fila como 0 para empezar desde ahi
		int fila = 0;
		int columna = mitad;

		// inicializando con el valor menor del cuadrado, las dimensiones que se
		// acabaron de definir
		this.matriz[fila][columna] = algo;

		// guardando la ubicacion actual de la fila y la columna
		int filaActual = fila;
		int columnaActual = columna;

		// a partir de las dimensiones anteriormente modificadas
		// este ciclo se mueve retrocediendo una fila y una columna para ir
		// llenando la matriz
		// con los valores a partir del menor valor
		for (int i = (algo + 1); i <= (algo + 8); i++) {
			// algo++;
			fila--;
			columna--;
			// si se sale de la matriz se guarda como la ultima fila
			if (fila < 0) {
				fila = (this.matriz.length) - 1;
			}
			// si se sale de la matriz se guarda como la ultima columna
			if (columna < 0) {
				columna = (this.matriz.length) - 1;
			}
			// si la posicion es 0 la llena normal
			if (this.matriz[fila][columna] == 0) {
				this.matriz[fila][columna] = i;

				// sino, tiene que moverse una fila abajo manteniendo la columna
			} else {
				System.out.println(fila);
				System.out.println(columna);
				System.out.println(matriz[fila][columna]);
				fila = filaActual + 1;
				columna = columnaActual;

				this.matriz[fila][columna] = i;
			}
			// guarda nuevamente la posicion actual de la fila y la columna
			filaActual = fila;
			columnaActual = columna;
		}
	}

	// metodo que imprime la matriz recorriendo con un for
	public void mostrarcuadrado() {
		System.out.println("Cuadrado m�gico: ");
		for (int i = 0; i < 3; i++) {
			System.out.println("");
			for (int j = 0; j < 3; j++) {
				System.out.print(this.matriz[i][j] + "  ");
			}
		}
	}

}
