/*
 *NOMBRE DEL PROGRAMA : PALABRA PALINDROMA
 * AUTOR: ALEJANDRO MORALES MOJICA
 *FECHA: 7 DE OCT DE 2016
 */
package PUNTO2;

public class PalabraPalindroma {
	// CREO MISA TRIBUTOS DE TIPO PRIVATE
	private String palabra = ("");
	private int tamano = 0;
	private String auxiliar = ("");

	// CREO EL METODO DONDE COMPRUEBO SI SE ES PALINDROMA O NO
	boolean isPalindromo(String palabra, int tamano, String auxiliar) {
		this.palabra = palabra;
		this.tamano = tamano;
		this.auxiliar = auxiliar;
		// RECORRO EL TAMANO DE LA PALABRA
		for (int i = 0; i < tamano; i++) {
			// MIRO LAS EXCEPCIONES QUE PUEDEN HABER
			if (!(palabra.substring(i, i + 1).equals(",") || palabra.substring(i, i + 1).equals(".")
					|| palabra.substring(i, i + 1).equals("�") || palabra.substring(i, i + 1).equals("?")
					|| palabra.substring(i, i + 1).equals("�") || palabra.substring(i, i + 1).equals("�")
					|| palabra.substring(i, i + 1).equals("!") || palabra.substring(i, i + 1).equals(" "))) {
				auxiliar = auxiliar + palabra.substring(i, i + 1);

			}
		}
		// RECORRO EL TAMA�O DEL AUXILIAR, QUIEN ES EL QUE POSEE LA PALABRA
		// INVERTIDA
		tamano = auxiliar.length();
		for (int i = 0; i < tamano / 2; i++) {
			// COMPARO SI EL CARACTER DE CADA UNO DE LOS SRTINGS SE ENCUENTRAN
			// EN LA MISMA POSCION
			if (!auxiliar.substring(i, i + 1).equals(auxiliar.substring(tamano - i - 1, tamano - i))) {
				return false;
			}

		}
		return true;
	}

	// CREO METODO QUE IMPRIME LA RESPUESTA FINAL
	public void respuesta(boolean isPalindromo) {
		if (isPalindromo == true) {
			System.out.println("La palabra SI es Palindroma");
		} else {
			System.out.println("La palabra NO es Palindroma");
		}
	}

	public String getPalabra() {
		return palabra;
	}

	public void setPalabra(String palabra) {
		this.palabra = palabra;
	}

	public int getTamano() {
		return tamano;
	}

	public void setTamano(int tamano) {
		this.tamano = tamano;
	}

	public String getAuxiliar() {
		return auxiliar;
	}

	public void setAuxiliar(String auxiliar) {
		this.auxiliar = auxiliar;
	}

}
