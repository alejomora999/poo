/*
 *NOMBRE DEL PROGRAMA : ORDEN DE ENTEROS
 * AUTOR: ALEAJNDRO MORALES MOJICA
 *FECHA: 7 DE OCT DE 2016
 */
package PUNTO1;

import java.util.Scanner;

public class OrdenEnteros {
	private int limite = 0;
	private int auxiliar = 0;
	private int vect[];
	private Scanner leer;

	// CREO UN METODO DONDE LLENO EL VECTOR POR TECLADO
	public void LlenarVector(int vec[], int limite) {
		this.limite = limite;
		// CREO UN FOR DONDE LLENARE DESDE LA POSICION 0 HASTA EL LIMITE n-1
		for (int i = 0; i < limite; i++) {
			leer = new Scanner(System.in);
			System.out.print("X[" + (i + 1) + "=]");
			vec[i] = leer.nextInt();
		}
	}
	public void Orden(int vec[], int limite, int auxiliar) {
		this.limite = limite;
		this.auxiliar = auxiliar;
		this.vect = vec;
		// CREO DOS FOR , EN EL PRIMERO RECORRO EL VECTOR RESPECTO A I
		for (int i = 0; i < limite; i++) {
			// PARA EL SEGUNDO FOR, EVALUO DESDE J=I+1; EL CUAL SE ENCARGARA DE
			// OCUPARA EL ESPACIO DE [I], SI ESTE ES MAYOR
			for (int j = i + 1; j < limite; j++) {
				// ESTE IF DETERMINA SI EL DATO POSTERIOR ES MENOR O MAYOR
				if (vec[i] > vec[j]) {
					auxiliar = vec[i];
					vec[i] = vec[j];
					// EN EL ATRIBUTO AUXILIAR SE ALMACENA EL DATO CAMBIADO DE POSICION
					vec[j] = auxiliar;
				}
			}
		}
		// UNA VEZ RECORRIDO LOS ARREGLOS IMPRIMO EL VECTOR ORDENADO
		System.out.print("El vector organizado de forma ascendente es ( ");
		for (int i = 0; i < limite; i++) {
			System.out.print(vec[i] + ",");

		}
		System.out.print(")");
		System.out.println("");
		System.out.println("Ahora de forma descendente");
		// CREO DOS FOR , EN EL PRIMERO RECORRO EL VECTOR RESPECTO A I
		for (int i = 0; i < limite; i++) {
			// PARA EL SEGUNDO FOR, EVALUO DESDE J=I+1; EL CUAL SE ENCARGARA DE
			// OCUPARA EL ESPACIO DE [I], SI ESTE ES MENOR
			for (int j = i + 1; j < limite; j++) {
				// ESTE IF DETERMINA SI EL DATO POSTERIOR ES MENOR O MAYOR,
				// DIFERENTE AL IF DEL METODO ANTERIOR
				if (vec[i] < vec[j]) {
					auxiliar = vec[i];
					vec[i] = vec[j];
					vec[j] = auxiliar;
				}
			}
		}
		// UNA VEZ RECORRIDO LOS ARREGLOS IMPRIMO EL VECTOR ORGANIZADO
		System.out.print("El vector organizado de forma descendente es ( ");
		for (int i = 0; i < limite; i++) {

			System.out.print(vec[i] + ",");

		}
		System.out.print(")");
	}

	public int getLimite() {
		return limite;
	}

	public void setLimite(int limite) {
		this.limite = limite;
	}

	public int getAuxiliar() {
		return auxiliar;
	}

	public void setAuxiliar(int auxiliar) {
		this.auxiliar = auxiliar;
	}

	public int[] getVect() {
		return vect;
	}

	public void setVect(int[] vect) {
		this.vect = vect;
	}

}
