/*
 *NOMBRE DEL PROGRAMA : ORDEN DE ENTEROS
 *AUTOR: ALEJANDRO MORALES MOJICA
 *FECHA: 7 DE OCT DE 2016
 */
package PUNTO1;

import java.util.Scanner;

public class OrdenEnterosTest {
	private static Scanner leer;

	public static void main(String arg[]) {
		leer = new Scanner(System.in);
		// AQUI SE DEFINE LA VARIABLE PARA SABER SI SE REINICIARA EL PROGRAMA
		int reiniciar = 0;
		int limite = 7;
		int auxiliar = 0;
		do { 
			System.out.println("este es un programa para ordenar 7 enteros:");
			// CREO EL OBJETO DE LA CLASE ORDENASCENDENTE
			OrdenEnteros Vectorx = new OrdenEnteros();

			System.out.println("Se ordenara ascendentemente:");
			// DECLARO Y CREO EL VECTOR
			int Vectorlim[] = new int[limite + 1];
			// LLAMO A LOS METODOS DE LA CLASE ORDENASCENDETE
			// CORRESPONDIENTES
			Vectorx.LlenarVector(Vectorlim, limite);
			Vectorx.Orden(Vectorlim, limite, auxiliar);
			System.out.print("\n");
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = leer.nextInt();
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = leer.nextInt();
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
		System.out.println("FIN DEL PROGRAMA");
	}

}
