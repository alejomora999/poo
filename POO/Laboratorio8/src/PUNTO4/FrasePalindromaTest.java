/*
 *NOMBRE DEL PROGRAMA : FRASE PALINDROMA
 * AUTOR: ALEJANDRO MORALES MOJICA
 *FECHA: 7 DE OCT DE 2016
 */
package PUNTO4;

import java.util.Scanner;

public class FrasePalindromaTest {
	private static Scanner leer;

	public static void main(String arg[]) {

		// AQUI SE DEFINE LA VARIABLE PARA SABER SI SE REINICIARA EL PROGRAMA
		int reiniciar = 0;
		String frase = ("");
		String auxiliar = ("");

		do {
			int tamano = 0;
			leer = new Scanner(System.in);
			System.out.println("Bienvenido,  le dire si una frase es palindroma o no:");
			System.out.println("Ingrese la frase, sin tildes y en minusculas");
			// ALMACENO MI STRING
			frase = leer.nextLine();
			tamano = frase.length();
			// CREO EL OBJETO DE LA CLASE PALABRA PALINDORMA
			FrasePalindroma Fraceinv = new FrasePalindroma();
			// LLAMO A LA CLASE
			Fraceinv.respuesta(Fraceinv.isPalindromo(frase, tamano, auxiliar));
			System.out.print("\n ");
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = leer.nextInt();
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = leer.nextInt();
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
		System.out.println("FIN DEL PROGRAMA");

	}
}
