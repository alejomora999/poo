/*
 *NOMBRE DEL PROGRAMA : ORDEN DE 11 ENTEROS
 * AUTOR: ALEJANDRO MORALES MOJICA
 *FECHA: 7 DE OCT DE 2016
 */
package PUNTO3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class OrdenDiez {
	// CREO MI ARRALIST DE TIPO PRIVAD0
	private ArrayList<Integer> enteros = new ArrayList<Integer>();

	// CREO EL METODO DONDE A�ADO LOS VALORES INGRESADOS
	public void datos(int a) {
		enteros.add(a);
	}

	// ORDENO Y MUESTRO LOS VALORES ASCENDENTEMENTE CON LA HERRAMIENTA
	// COLLECTIONS
	public void mostrar() {
		Collections.sort(enteros);
		System.out.print("(");
		for (Integer item : enteros) {

			System.out.print(item + ",");
		}
		System.out.print(")");
		System.out.println(" ");
	}

	// ORDENO Y MUESTRO LOS VALORES ASCENDENTEMENTE CON LA HERRAMIENTA
	// COLLECTIONS Y COMPARATOR
	public void mostrar1() {
		Comparator<Integer> comparador = Collections.reverseOrder();
		Collections.sort(enteros, comparador);
		System.out.print("(");
		for (Integer item : enteros) {

			System.out.print(item + ",");
		}
		System.out.print(")");
		System.out.println(" ");
	}
}
