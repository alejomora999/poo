/*
 *NOMBRE DEL PROGRAMA : ORDEN DE 11 ENTEROS
 * AUTOR: ALEJANDRO MORALES MOJICA
 *FECHA: 7 DE OCT DE 2016
 */
package PUNTO3;

import java.util.Scanner;

public class OrdenDiezTest {

	private static Scanner leer;

	public static void main(String arg[]) {
		// AQUI SE DEFINE LA VARIABLE PARA SABER SI SE REINICIARA EL PROGRAMA
		int reiniciar = 0;
		int decision = 0;
		do { 
			OrdenDiez ejemplo = new OrdenDiez();
			leer = new Scanner(System.in);
			System.out.println("esta es una lista para ordenar enteros:");
			System.out.println("Ingrese un numero para  tomar una decision :");
			System.out.println("Si ingresa 1 se ordenara ascendentemente :");
			System.out.println("Si ingresa 2 se ordenara descendentemente :");
			decision = leer.nextInt();
			// CREO EL SWITCH DE LA DECISION
			switch (decision) {
			case 1:
				// INGRESO LOS VALORES
				System.out.println("Ingrese los primeros 10 datos :");
				for (int i = 0; i < 4; i++) {
					leer = new Scanner(System.in);
					System.out.print("X[" + (i + 1) + "=]");
					ejemplo.datos(leer.nextInt());
				}
				// LLAMO METODOS
				System.out.println("Sus 10 datos organizados ascendentemente son:");
				ejemplo.mostrar();
				System.out.println("Ahora ingresa el dato numero 11 :");
				// PIDO EL DATO NUMERO 11
				ejemplo.datos(leer.nextInt());
				System.out.println("Sus 11 datos organizados ascendentemente son:");
				ejemplo.mostrar();
				break;
			case 2:
				// INGRESO LOS VALORES
				System.out.println("Ingrese los primeros 10 datos :");
				for (int i = 0; i < 10; i++) {
					leer = new Scanner(System.in);
					System.out.print("X[" + (i + 1) + "=]");
					ejemplo.datos(leer.nextInt());
				}
				// LLAMO METODOS
				System.out.println("Sus 10 datos organizados descendentemente son:");
				ejemplo.mostrar1();
				System.out.println("Ahora ingresa el dato numero 11 :");
				// PIDO EL DATO NUMERO 11
				ejemplo.datos(leer.nextInt());
				System.out.println("Sus 11 datos organizados descendentemente son:");
				ejemplo.mostrar1();
				break;
			default:
				// En caso de haber seleccionado una opcion que no est\E1 en el
				// menu, imprimo este aviso y salgo del Switch
				System.out.println("Ha elegido una opcion invalida o no perteneciente a la lista de opciones :");
				break;
			}

			System.out.print("\n ");
			System.out.println("\BFdesea repetir el programa? 1=SI ; 0=NO");
			reiniciar = leer.nextInt();
			// El siguiente While, acompa\F1ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = leer.nextInt();
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
		System.out.println("FIN DEL PROGRAMA");
	}

}
