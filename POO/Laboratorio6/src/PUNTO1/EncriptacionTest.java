/*
 *NOMBRE DEL PROGRAMA : ENCRIPTACION 
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 5 OCT DE 2016
 */
package PUNTO1;

import java.io.*;

public class EncriptacionTest {
	public static void main(String[] arg) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("�Cu�ntas letras tiene la palabra que va a encriptar? ");
		int reiniciar=0;
		int a = Integer.parseInt(in.readLine());
		String Ending[] = new String[a];

		do{
			System.out.println("Ingrese la palabra letra por letra para encriptar ");
			for (int i = 0; i < a; i++) {
				Ending[i] = in.readLine();
			}

			Encriptacion VALOR = new Encriptacion();
			System.out.println("digite 1 Para Encriptar.");
			int opcion = Integer.parseInt(in.readLine());

			if (opcion == 1) {
				VALOR.encriptar(a, Ending);
			} 

			System.out.println(" �desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = Integer.parseInt(in.readLine());
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = Integer.parseInt(in.readLine());
				} else {
					
					break;
				}
			}

		}while (reiniciar != 0);
		System.out.println("FIN DEL PROGRAMA");
	}
}
