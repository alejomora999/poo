/*
 *NOMBRE DEL PROGRAMA : LEY COSENO 
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 21 SEP DE 2016
 */
package PUNTO2;

import java.io.*;


public class LeyCosenoTest {
	public static void main(String arg[]) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int reiniciar;
		double lado_a;
		double lado_b;
		double angulo_de_a_b;
		double lado_restante;
		do {
			System.out.println("Bienvenido, este programa le dira el lado restante mediante ley del Coseno");
			System.out.println("Ingrese el  lado A");
			lado_a = Double.parseDouble(in.readLine());
			//Este while me dira si el dato ingresado es valido, de no serlo lo exigira de nuevo
			while (lado_a <=0) {
				System.out.println("Ingrese un lado valido:");
				lado_a = Double.parseDouble(in.readLine());
				if (lado_a>0){
					break;
				}
			}
			System.out.println("Ingrese el lado B");
			lado_b = Double.parseDouble(in.readLine());
			//Este while me dira si el dato ingresado es valido, de no serlo lo exigira de nuevo
			while (lado_b <=0) {
				System.out.println("Ingrese un lado valido:");
				lado_b = Double.parseDouble(in.readLine());
				if (lado_b>0){
					break;
				}
			}
			System.out.println("Ingrese el �ngulo entre el lado A y  el lado B");
			angulo_de_a_b = Double.parseDouble(in.readLine());
			//Este while me dira si el dato ingresado es valido, de no serlo lo exigira de nuevo
			while (angulo_de_a_b <=0) {
				System.out.println("Ingrese un �ngulo valido:");
				angulo_de_a_b = Double.parseDouble(in.readLine());
				if (angulo_de_a_b>0){
					break;
				}
			}
			//Creamos nuestro objeto de la clase LeyDeCoseno
			LeyCoseno LadoRestante = new LeyCoseno( lado_a,  lado_b,  angulo_de_a_b);
			//Calculo el lado restante por constructor
			lado_restante= LadoRestante.LeyCosenito(LadoRestante.getLado_a(), LadoRestante.getLado_b(), LadoRestante.getAngulo_a_b());
			//Imprimo la respuesta
			System.out.println("El lado restante del triangulo es: "+lado_restante+ " unidades");
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = Integer.parseInt(in.readLine());
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = Integer.parseInt(in.readLine());
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
		System.out.println("FIN DEL PROGRAMA");
	}
}
