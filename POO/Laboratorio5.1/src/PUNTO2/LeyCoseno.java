/*
 *NOMBRE DEL PROGRAMA : LEY COSENO 
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 21 SEP DE 2016
 */
package PUNTO2;

public class LeyCoseno {
	//Defino mis atributos tipo private
		private double lado_a;
		private double lado_b;
		private double angulo_a_b;
		// DECLARAMOS EL METODO CONSTRUCTOR. POR DEFAULT
			public LeyCoseno() {

			}
			// DECLARAMOS EL METODO CONSTRUCTOR CON ARGUMENTOS
			public LeyCoseno(double lado_a, double lado_b, double angulo_a_b) {
				this.lado_a= lado_a;
				this.lado_b = lado_b;
				this.angulo_a_b= angulo_a_b;
			}
			// DEFINIMOS LOS METODOS GET Y SET DE LAS VARIABLES PRIVADAS
			public double getLado_a() {
				return lado_a;
			}
			public void setLado_a(double lado_a) {
				this.lado_a = lado_a;
			}
			public double getLado_b() {
				return lado_b;
			}
			public void setLado_b(double lado_b) {
				this.lado_b = lado_b;
			}
			public double getAngulo_a_b() {
				return angulo_a_b;
			}
			public void setAngulo_a_b(double angulo_a_b) {
				this.angulo_a_b = angulo_a_b;
			}
			//CREO UN METODO DONDE CALCULO LA LEY DEL SENO, CON LOS ATRIBUTOS RECIBIDOS
			double LeyCosenito (double lado_a, double lado_b, double angulo_a_b){
				double leyCosenito;
				double grados_angulo;
				grados_angulo=Math.toRadians(angulo_a_b);
				leyCosenito=((Math.pow(lado_a, 2)+(Math.pow(lado_b, 2)-(2)*(lado_a)*(lado_b)*(Math.cos(grados_angulo)))));
				leyCosenito=Math.sqrt(leyCosenito);
				return leyCosenito;
			}
}