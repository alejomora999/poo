/*
 *NOMBRE DEL PROGRAMA : AREA  TRIANGULO 
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 21 SEP DE 2016
 */
package PUNTO1;

public class AreaTriangulo {
	private double lado_a;
	private double lado_b;
	private double lado_c;
	// DECLARAMOS EL METODO CONSTRUCTOR. POR DEFAULT
		public AreaTriangulo() {

		}

		// DECLARAMOS EL METODO CONSTRUCTOR CON ARGUMENTOS
		public AreaTriangulo(double lado_a, double lado_b, double lado_c) {
			this.lado_a= lado_a;
			this.lado_b = lado_b;
			this.lado_c= lado_c;
		}
		// DEFINIMOS LOS METODOS GET Y SET DE LAS VARIABLES PRIVADAS

		public double getLado_a() {
			return lado_a;
		}

		public void setLado_a(double lado_a) {
			this.lado_a = lado_a;
		}

		public double getLado_b() {
			return lado_b;
		}

		public void setLado_b(double lado_b) {
			this.lado_b = lado_b;
		}

		public double getLado_c() {
			return lado_c;
		}

		public void setLado_c(double lado_c) {
			this.lado_c = lado_c;
		}
		//CREO UN METODO DONDE CALCULO EL �REA
		double areaheron(double lado_a , double lado_b, double lado_c){
			double perimetro;
			double area;
			//CREO LA "S" DE LA FORMULA DE HERON
			perimetro= (lado_a+lado_b+lado_c)/2;
			//OPERO
			area= ((perimetro)*(perimetro-lado_a)*(perimetro-lado_b)*(perimetro-lado_c));
			area= Math.sqrt(area);
			return area;
		}

}
