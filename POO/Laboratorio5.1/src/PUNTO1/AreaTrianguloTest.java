/*
 *NOMBRE DEL PROGRAMA : AREA  TRIANGULO 
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 21 SEP DE 2016
 */
package PUNTO1;

import java.io.*;

public class AreaTrianguloTest {
	public static void main(String arg[]) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int reiniciar;
		double lado_a;
		double lado_b;
		double lado_c;
		double area;
		do{
		
			System.out.println("este programa le dira el �rea  mediante la formula de Heron");
			System.out.println("Ingrese el lado a");
			lado_a = Double.parseDouble(in.readLine());
			//Este while me dira si el dato ingresado es valido, de no serlo lo exigira de nuevo
			while (lado_a <=0) {
				System.out.println("Ingrese un lado valido:");
				lado_a = Double.parseDouble(in.readLine());
				if (lado_a>0){
					break;
				}
			}
			System.out.println("Ingrese el lado b");
			lado_b = Double.parseDouble(in.readLine());
			//Este while me dira si el dato ingresado es valido, de no serlo lo exigira de nuevo
			while (lado_b <=0) {
				System.out.println("Ingrese un lado valido:");
				lado_b = Double.parseDouble(in.readLine());
				if (lado_b>0){
					break;
				}
			}
			System.out.println("Ingrese el lado c");
			lado_c = Double.parseDouble(in.readLine());
			//Este while me dira si el dato ingresado es valido, de no serlo lo exigira de nuevo
			while (lado_c <=0) {
				System.out.println("Ingrese un lado valido:");
				lado_c = Double.parseDouble(in.readLine());
				if (lado_c>0){
					break;
				}
			}
			//Creamos nuestro objeto de la clase Lados
			Lados Prueba= new Lados();
			//Creamos nuestro objeto de la clase Heron
			AreaTriangulo AreaPorHeron= new AreaTriangulo(lado_a, lado_b, lado_c);
			if (Prueba.isdatos_validos(lado_a, lado_b, lado_c)){
				//Calculo el �rea por constructor
				area= AreaPorHeron.areaheron(AreaPorHeron.getLado_a(), AreaPorHeron.getLado_b(), AreaPorHeron.getLado_c());
				//Imprimo el �rea
				System.out.println("El �rea de su triangulo es: "+area+" unidades cuadradas");
			}else{
				System.out.println("Lo siento el triangulo no existe");
			}

			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = Integer.parseInt(in.readLine());
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = Integer.parseInt(in.readLine());
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
		System.out.println("FIN DEL PROGRAMA");
	}

}
