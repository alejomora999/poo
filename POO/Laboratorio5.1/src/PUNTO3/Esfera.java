/*
 *NOMBRE DEL PROGRAMA : ESFERA 
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 21 SEP DE 2016
 */
package PUNTO3;

public class Esfera {
	//Defino mis atributos tipo private
			private double radio;
			// DECLARAMOS EL METODO CONSTRUCTOR POR DEFAULT
			public Esfera(){
				this.radio=1;
				
			}
			// DECLARAMOS EL METODO CONSTRUCTOR CON PARAMETROS
			public Esfera(double radio) {
			this.radio= radio;
						
			}

			public double getRadio() {
				return radio;
			}

			public void setRadio(double radio) {
				this.radio = radio;
			}
			//CREO UN METODO DONDE CALCULO DEL VOLUMEN
			double 	Volumen(double radio){
				this.radio=radio;
				this.radio=(((1.3333333))*Math.PI*Math.pow(radio, 3));
				return this.radio;
			}
			//CREO UN METODO DONDE CALCULO LA SUPERFICIE
					double 	Superficie(double radio){
						this.radio=radio;
						this.radio=((4)*Math.PI*Math.pow(radio, 2));
						return this.radio;
					}
			//CREO UN METODO DONDE IMPRIMO LA INFORMACION
			public void Imprimir(double Superficie, double volumen, double radio){
				this.radio=radio;
				System.out.println("La Superficie de la esfera de radio "+this.radio+" es: "+Superficie+" unidades cuadradas");
				System.out.println("El Volumen de la esfera de radio "+this.radio+" es: "+volumen+" unidades cubicas ");
			}
			

			
}
