/*
 *NOMBRE DEL PROGRAMA : ESFERA 
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 21 SEP DE 2016
 */
package PUNTO3;

import java.io.*;

public class EsferaTest {
	public static void main(String arg[]) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int reiniciar;
		double radio;
		double radio_1;
		do{
			System.out.println("Bienvenido, este programa le dira el volumen y superficie de una esfera");
			//Pido los datos que necesito
			System.out.println("Ingrese el  Radio");
			radio = Double.parseDouble(in.readLine());
			//Este while me dira si el dato ingresado es valido, de no serlo lo exigira de nuevo
			while (radio <=0) {
				System.out.println("Ingrese un Radio valido:");
				radio = Double.parseDouble(in.readLine());
				if (radio>0){
					break;
				}
			}
			Esfera Esferita= new Esfera(radio);
			Esfera Esferita1= new Esfera();
			Esferita.Imprimir(Esferita.Superficie(radio), Esferita.Volumen(radio), radio);
			System.out.println("Ahora con Radio = 1");
			//Igualo mi atributo al get.Radio, el cual contiene el radio =1
			radio_1=Esferita1.getRadio();
			Esferita1.Imprimir(Esferita1.Superficie(radio_1), Esferita1.Volumen(radio_1), radio_1);
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = Integer.parseInt(in.readLine());
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = Integer.parseInt(in.readLine());
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
		System.out.println("FIN DEL PROGRAMA");
	
	}
}
