/*
 *NOMBRE DEL PROGRAMA : FuncionesTrigonometricas
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 14 SEP DE 2016
 */
package PUNTO1;

import java.io.*;

public class FuncionesTrigonometricasTest {
	public static void main(String arg[]) throws IOException {
		// AQUI SE DEFINE LA VARIABLE PARA SABER SI SE REINICIARA EL PROGRAMA
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int reiniciar;
		double angulo1;
		double conversionrads;
		int procedimiento;
		do {
			System.out.println("Se calculara El Seno, El Coseno y La Tangente de un �ngulo determinado ");
			// Creo mi objeto de tipo FuncionesTrigonometricas
			FuncionesTrigonometricas angulo = new FuncionesTrigonometricas();
			// Creo mi objeto de tipo anguloparimpar
			anguloparimpar valorangulo = new anguloparimpar();
			//SE IMPRIMIRAN LAS OPCIONES PARA DESARROLLAR LAS OPERACIONES
			System.out.println("si ingresa el valor de 1 se convertira el dato a seno");
			System.out.println("si ingresa el valor de 2 se convertira el dato a coseno");
			System.out.println("si ingresa el valor de 3 se convertira el dato a tangente");	
			procedimiento = Integer.parseInt(in.readLine());
			switch (procedimiento) {
			case 1:
				System.out.println("ha elegido la opcion de convertir el angulo a seno");
				System.out.println("ingrese el valor ");
				// El valor se almacenara en esta variable
				angulo1 = Double.parseDouble(in.readLine());
				conversionrads = Math.toRadians(angulo1);
				if (angulo1 % 90 == 0) {
					// Creo una atributo double donde divido el dato ingresado
					// en 90
					// el resultado lo voy a llevar a mi clase par o impar
					double cociente = angulo1 / 90;
					if (valorangulo.isPar(cociente)) {
						// Por deduccion se sabe que si el cociente es par
						// tanto el seno como la tangente son cero
						System.out.println("Seno de " + angulo1 + "�" + " es 0.0");

					} else {

						System.out.println("Seno de " + angulo1 + "�" + " es " + angulo.getSeno(conversionrads));

					}

				} else {
					// Si el valor ingresado no es multiplo de 90 opero todas
					// las funciones
					System.out.println("Seno de " + angulo1 + "�" + " es " + angulo.getSeno(conversionrads));
				}
				break;
			case 2:
				System.out.println("ha elegido la opcion de convertir el angulo a coseno");
				System.out.println("ingrese el valor ");
				// El valor se almacenara en esta variable
				angulo1 = Double.parseDouble(in.readLine());
				conversionrads = Math.toRadians(angulo1);
				if (angulo1 % 90 == 0) {
					// Creo una atributo double donde divido el dato ingresado
					// en 90
					// el resultado lo voy a llevar a mi clase par o impar
					double cociente = angulo1 / 90;
					if (valorangulo.isPar(cociente)) {
						// Por deduccion se sabe que si el cociente es par
						System.out.println("Coseno de " + angulo1 + "�" + " es " + angulo.getCoseno(conversionrads));

					} else {
						// Por deduccion se sabe que si el cociente es par
						// el coseno es cero
						System.out.println("Coseno de " + angulo1 + "�" + " es 0.0 ");

					}

				} else {
					// Si el valor ingresado no es multiplo de 90 opero
					System.out.println("Coseno de " + angulo1 + "�" + " es " + angulo.getCoseno(conversionrads));
				}
				break;
			case 3:
				System.out.println("ha elegido la opcion de convertir el angulo a tangente ");
				System.out.println("ingrese el valor ");
				// El valor se almacenara en esta variable
				angulo1 = Double.parseDouble(in.readLine());
				conversionrads = Math.toRadians(angulo1);
				if (angulo1 % 90 == 0) {
					// Creo una atributo double donde divido el dato ingresado
					// en 90
					// el resultado lo voy a llevar a mi clase par o impar
					double cociente = angulo1 / 90;
					if (valorangulo.isPar(cociente)) {
						// Por deduccion se sabe que si el cociente es par
						// la tangente es cero
						System.out.println("Tangente de " + angulo1 + " es 0.0");
					} else {
						// Por deduccion se sabe que si el cociente es par
						// la tangente es indeterminada
						System.out.println("Tangente de " + angulo1 + "�" + " es Indeterminada ");
					}

				} else {
					// Si el valor ingresado no es multiplo de 90 opero
					System.out.println("Tangente de " + angulo1 + "�" + " es: " + angulo.getTangente(conversionrads));
				}
				break;
			default:
				// si se selecciono una opcion no citada en el menu, salir del switch e imprimir este aviso
				System.out.println("Ha elegido una opcion diferente a las que aparecen en el menu ");
				break;
			}
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = Integer.parseInt(in.readLine());
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = Integer.parseInt(in.readLine());
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
		System.out.println("FIN DEL PROGRAMA");
	}

}
