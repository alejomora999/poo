/*
 *NOMBRE DEL PROGRAMA : FUNCIONES TRIGONOMETRICAS
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 14 SEP DE 2016
 */
package PUNTO1;

public class FuncionesTrigonometricas {
	// CREO LOS ATRIBUTOS DE TIPO PRIVATE
	private double seno;
	private double coseno;
	private double tangente;

	// LLamo los metodos GETT y SETTER para los  atributos
	// tambien se usaran funciones de conversion trigonometricas
	public double getSeno(double radianes) {
		this.seno = Math.sin(radianes);
		return seno;
	}

	public double getCoseno(double radianes) {
		this.coseno = Math.cos(radianes);
		return coseno;
	}

	public double getTangente(double radianes) {
		this.tangente = Math.tan(radianes);
		return tangente;
	}

}
