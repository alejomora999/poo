/*
 *NOMBRE DEL PROGRAMA : MEDIA INCREMENTADA
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 14 SEP DE 2016
 */
package PUNTO2;
import java.io.*;
public class MediaIncrementadaTest {
	public static void main(String arg[]) throws IOException{
		// AQUI SE DEFINE LA VARIABLE PARA SABER SI SE REINICIARA EL PROGRAMA
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int reiniciar;
		int nuevonumero;
		double num;
		double num1;
		double numnuevo;
		//En este contador se almacenaran la cantidad de datos ingresados
		double cont1 = 1;
		//Creo atributos donde almaceno los datos ingresado por el usuario
		
		do{
			//Creo mi objeto pertenecinete a la calse MediaIncrementada
			MediaIncrementada mediaaum = new MediaIncrementada();
			System.out.println("Se calculara la media segun los datos ingresados por el usuario ");
			//pido el primer numero
			System.out.println("Ingrese el primer numero ");
			num=Double.parseDouble(in.readLine());
			//igualo num1 a num para poder tener el numero anterior y asi calcular la nueva media
			num1=num;
			//se imprimira la media actual
			System.out.println("Su media Actual es "+mediaaum.getMedia1(num));
			do{
				//se preguntara si quiero ingresar otro numero
				System.out.println("�desea ingresar otro numero? 1=SI ; 0=NO");
				nuevonumero = Integer.parseInt(in.readLine());
				//Este if determinara si el usuario quiere continuar, si no quiere continuara
				if(nuevonumero==0){
					break;
				}
				//el contador aumenta en uno
				cont1++;
				//se pide ingresar el nuevo numero
				System.out.println("ingrese el nuevo numero ");
				numnuevo=Double.parseDouble(in.readLine());
				//Llamo al metodo setter medianueva, de la clase mediaincremento
				mediaaum.setMedia_nueva(numnuevo, num1, cont1);
				//Llamo al metodo get medianueva, de la clase mediaincremento
				System.out.println("Su media Actual es  "+mediaaum.getMedia_nueva());
				//igualo num1 a num1+numnuevo para tener el numero anterior y poder calcular una posible  media nueva
				num1=num1+numnuevo;
				// El While, acompa�ado de un If permite saber si la
				// opcion ingresada es correcta, si no es asi  exigira nuevamente
				while (nuevonumero != 1) {
					if (nuevonumero != 0) {
						System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no ");
						nuevonumero = Integer.parseInt(in.readLine());
					}else{
						break;
					}
				}
				} while (nuevonumero != 0);
			
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = Integer.parseInt(in.readLine());
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no ");
					reiniciar = Integer.parseInt(in.readLine());
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
		System.out.println("FIN DEL PROGRAMA");
	}
			
	}

