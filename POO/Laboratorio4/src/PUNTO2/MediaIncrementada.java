/*
 *NOMBRE DEL PROGRAMA : MEDIA INCREMENTADA
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 14 SEP DE 2016
 */
package PUNTO2;

public class MediaIncrementada {
	// CREO ATRIBUTOS DE TIPO PRIVADO
	private double media1;
	private double median;

	// Este get almacenara la primera media, correspondiente al primer numero  ingresado
	public double getMedia1(double media) {
		this.media1 = media;
		return media1;

	}

	// El get retornara el valor del setter con el mismo nombre
	public double getMedia_nueva() {

		return median;
	}

	// En el setter se lleva a cabo la media 
	//el resultado lo retornara al get anterior
	public void setMedia_nueva(double nuevonumero, double numeroanterior, double contador) {
		// Este setter toma el numero nuevo, el anterior y el contador que lleve
		// segun la catidad de digitos ingresados
		this.median = (nuevonumero + numeroanterior) / contador;
	}

}
