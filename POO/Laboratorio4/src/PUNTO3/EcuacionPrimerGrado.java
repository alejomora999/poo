/*
 *NOMBRE DEL PROGRAMA : ECUACION DE PRIMER GRADO
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 14 SEP DE 2016
 */
package PUNTO3;

public class EcuacionPrimerGrado {
	//DEFINO EL ATRIBUTO DE TIPO PRIVATE
	private double solucionecuacion;
	//Llamo al getter de mi atributo, en donde se va aretornar la respuesta, a partir de las tres incognitas ingresadas
	public double getSolucionecuacion(double a, double b, double c) {
		solucionecuacion=(c-b)/a;
		//Returno la solucion en el main
		return solucionecuacion;
	}
	

}
