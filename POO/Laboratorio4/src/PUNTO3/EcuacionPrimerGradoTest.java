/*
 *NOMBRE DEL PROGRAMA : ECUCION DE PRIMER GRADO
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 14 SEP DE 2016
 */
package PUNTO3;
import java.io.*;
public class EcuacionPrimerGradoTest {
	public static void main(String arg[]) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		//AQUI SE DEFINE LA VARIABLE PARA SABER SI SE REINICIARA EL PROGRAMA
		int reiniciar;
		double consa;
		double consb;
		double consc;
		do{
			System.out.println("Se va a resolver una ecuacion de la forma AX+B=C");
			//Creo el objeto de tipo EcuacionPrimerGrado
			EcuacionPrimerGrado resultado = new EcuacionPrimerGrado();
			//pido el valor de la  1 constante
			System.out.println("Ingrese el valor de a ");
			consa=Double.parseDouble(in.readLine());
			//El while validara si el dato es correcto o no y si no lo es lo pedira otra vez
			
			while(consa==0){
				System.out.println("Ingrese un valor que sea correcto, diferente de 0  para evitar error nuevamente");
				consa=Double.parseDouble(in.readLine());
			}
			//el valor de b y c no necesitan validarse
			//pido elvalor de la 2 constante 
			System.out.println("Ingrese el valor de b ");
			consb=Double.parseDouble(in.readLine());
			//pido el valor de la 3 constante
			System.out.println("Ingrese el valor de c ");
			consc=Double.parseDouble(in.readLine());
			//LLamo a la clase EcuacionPrimerGrado en cada atributo, la cual por medio del get solucion sera retornada
			System.out.println("El valor de la varaible es x= "+ resultado.getSolucionecuacion(consa, consb, consc));
			//una vez calculada la solucion preguntare si deseo reiniciar o terminar el programa
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = Integer.parseInt(in.readLine());
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = Integer.parseInt(in.readLine());
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
		System.out.println("FIN DEL PROGRAMA");
	}
	}


