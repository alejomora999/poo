package PUNTO2;

public abstract class Figura {
	protected double radio=0;
	protected double altura=0;
	protected double base=0;
	
	public Figura (double radio){
		this.radio=radio;
		
		
	}
	public Figura (double base, double altura){
		this.base=base;
		this.altura=altura;
		
	}
	
	public abstract void area();
	public abstract void perimetro();

}
