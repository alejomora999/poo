package PUNTO2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class Ventana_1 extends JFrame {

	private JPanel contentPane;
	private JTextField radio;
	private  int k=0;
	private int k1=0;
	private  int k2=0;
	private int k3=0;
	private JTextField txtb;
	private JTextField ladoc;
	private JTextField alturar;
	private JTextField baser;
	private JTextField txta;
	private JTextField txtc;
	
	
	public Ventana_1() {
		setTitle("FIGURAS");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setToolTipText("");
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 434, 261);
		contentPane.add(tabbedPane);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.ORANGE);
		tabbedPane.addTab("Circulo", null, panel_1, null);
		panel_1.setLayout(null);
		
		JLabel lblEstudios = new JLabel("RADIO:");
		lblEstudios.setBounds(71, 97, 63, 14);
		panel_1.add(lblEstudios);
		
		radio = new JTextField();
		radio.setColumns(10);
		radio.setBounds(144, 94, 196, 20);
		panel_1.add(radio);
		
		JButton btnCrearDocente = new JButton("\u00C1REA");
		btnCrearDocente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					double a=Double.parseDouble(radio.getText());
					if (a>0){
						Circulo cr= new Circulo(a);
						cr.area();
						
					}else{
						JOptionPane.showMessageDialog(null, "El radio ingresado no es valido");
					}
				}catch(NumberFormatException e1){
					//e1.printStackTrace();
					JOptionPane.showMessageDialog(null, "Dato no valido, no se ingreso un n�mero o el campo est� vacio");

				}
					
				
			}
		});
		btnCrearDocente.setBounds(10, 199, 122, 23);
		panel_1.add(btnCrearDocente);
		
		JButton btnSalir = new JButton("SALIR");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnSalir.setBounds(330, 199, 89, 23);
		panel_1.add(btnSalir);
		
		JButton btnPerimetro = new JButton("PERIMETRO");
		btnPerimetro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					double a=Double.parseDouble(radio.getText());
					if (a>0){
						Circulo cr= new Circulo(a);
						cr.perimetro();
					}else{
						JOptionPane.showMessageDialog(null, "El radio ingresado no es valido");
					}
				}catch(NumberFormatException e1){
					//e1.printStackTrace();
					JOptionPane.showMessageDialog(null, "Dato no valido, no se ingreso un n�mero o el campo est� vacio");

				}
				
			}
		});
		btnPerimetro.setBounds(154, 199, 151, 23);
		panel_1.add(btnPerimetro);
		
		JButton button_11 = new JButton("LIMPIAR");
		button_11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				radio.setText(null);
				
			}
		});
		button_11.setBounds(174, 165, 89, 23);
		panel_1.add(button_11);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.CYAN);
		panel.setToolTipText("");
		tabbedPane.addTab("Rectangulo", null, panel, null);
		panel.setLayout(null);
		
		JButton button = new JButton("\u00C1REA");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					double a=Double.parseDouble(baser.getText());
					double b=Double.parseDouble(alturar.getText());
					if (a>0 && b>0){
						Rectangulo rec = new Rectangulo(a,b);
						rec.area();
					}else if (a>0 && b<=0){
						JOptionPane.showMessageDialog(null, "La base ingrasada no es valida");
					}else if(a<=0&&b>0){
						JOptionPane.showMessageDialog(null, "La altura ingrasada no es valida");
					}else{
						JOptionPane.showMessageDialog(null, "Tanto la Base como la Altura ingrasadas no son validas");

					}
				}catch(NumberFormatException e1){
					//e1.printStackTrace();
					JOptionPane.showMessageDialog(null, "Dato no valido, Alguno o ambos de los campos pueden estar vacios o sin n�meros ");

				}
			}
		});
		button.setBounds(10, 199, 122, 23);
		panel.add(button);
		
		JButton button_2 = new JButton("PERIMETRO");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					double a=Double.parseDouble(baser.getText());
					double b=Double.parseDouble(alturar.getText());
					if (a>0 && b>0){
						Rectangulo rec1=new Rectangulo(a,b);
						rec1.perimetro();
					}else if (a>0 && b<=0){
						JOptionPane.showMessageDialog(null, "La base ingrasada no es valida");
					}else if(a<=0&&b>0){
						JOptionPane.showMessageDialog(null, "La altura ingrasada no es valida");
					}else{
						JOptionPane.showMessageDialog(null, "Tanto la Base como la Altura ingrasadas no son validas");

					}
				}catch(NumberFormatException e1){
					//e1.printStackTrace();
					JOptionPane.showMessageDialog(null, "Dato no valido, Alguno o ambos de los campos pueden estar vacios o sin n�meros ");

				}
				
			}
		});
		button_2.setBounds(154, 199, 151, 23);
		panel.add(button_2);
		
		JButton button_3 = new JButton("SALIR");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		button_3.setBounds(330, 199, 89, 23);
		panel.add(button_3);
		
		JButton button_10 = new JButton("LIMPIAR");
		button_10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				baser.setText(null);
				alturar.setText(null);
			}
		});
		button_10.setBounds(171, 165, 89, 23);
		panel.add(button_10);
		
		alturar = new JTextField();
		alturar.setColumns(10);
		alturar.setBounds(160, 121, 196, 20);
		panel.add(alturar);
		
		JLabel label = new JLabel("ALTURA:");
		label.setBounds(74, 124, 60, 14);
		panel.add(label);
		
		baser = new JTextField();
		baser.setColumns(10);
		baser.setBounds(160, 74, 196, 20);
		panel.add(baser);
		
		JLabel label_1 = new JLabel("BASE:");
		label_1.setBounds(74, 77, 51, 14);
		panel.add(label_1);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.MAGENTA);
		tabbedPane.addTab("Triangulo", null, panel_2, null);
		panel_2.setLayout(null);
		
		JButton button_1 = new JButton("\u00C1REA");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					double a=Double.parseDouble(txta.getText());
					double b=Double.parseDouble(txtb.getText());
					double c=Double.parseDouble(txtc.getText());
					if (a>0 && b>0&& c>0){
						if (a+b<c || b+c<a||a+c<b){
							JOptionPane.showMessageDialog(null, "El triangulo no existe");

						}else{
						Triangulo tr = new Triangulo(a,b);
						tr.area(a,b,c);
						}
						
					}else if (a>0 && b<=0){
						JOptionPane.showMessageDialog(null, "La base ingrasada no es valida");
					}else if(a<=0&&b>0){
						JOptionPane.showMessageDialog(null, "La altura ingrasada no es valida");
					}else{
						JOptionPane.showMessageDialog(null, "Tanto la Base como la Altura ingrasadas no son validas");

					}
				}catch(NumberFormatException e1){
					//e1.printStackTrace();
					JOptionPane.showMessageDialog(null, "Dato no valido, Alguno o ambos de los campos pueden estar vacios o sin n�meros ");

				}
			}
		});
		button_1.setBounds(8, 204, 122, 23);
		panel_2.add(button_1);
		final Ventana_2 window2= new Ventana_2();
		JButton button_4 = new JButton("PERIMETRO");
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				window2.setVisible(true);
				
				
			}
		});
		button_4.setBounds(152, 204, 151, 23);
		panel_2.add(button_4);
		
		JButton button_5 = new JButton("SALIR");
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		button_5.setBounds(328, 204, 89, 23);
		panel_2.add(button_5);
		
		JButton button_9 = new JButton("LIMPIAR");
		button_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtb.setText(null);
				txta.setText(null);
				txtc.setText(null);
			}
		});
		button_9.setBounds(182, 170, 89, 23);
		panel_2.add(button_9);
		
		JLabel lblBase = new JLabel("B");
		lblBase.setBounds(66, 79, 51, 14);
		panel_2.add(lblBase);
		
		txtb = new JTextField();
		txtb.setColumns(10);
		txtb.setBounds(152, 76, 99, 20);
		panel_2.add(txtb);
		
		JLabel lblAltura = new JLabel("C");
		lblAltura.setBounds(66, 126, 60, 14);
		panel_2.add(lblAltura);
		
		txta = new JTextField();
		txta.setColumns(10);
		txta.setBounds(152, 45, 99, 20);
		panel_2.add(txta);
		
		txtc = new JTextField();
		txtc.setColumns(10);
		txtc.setBounds(152, 123, 99, 20);
		panel_2.add(txtc);
		
		JLabel lblA = new JLabel("A");
		lblA.setBounds(66, 48, 51, 14);
		panel_2.add(lblA);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(Color.GREEN);
		tabbedPane.addTab("Cuadrado", null, panel_3, null);
		panel_3.setLayout(null);
		
		JButton button_6 = new JButton("\u00C1REA");
		button_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				try{
					double a=Double.parseDouble(ladoc.getText());
					
					if (a>0 ) {
						Cuadrado cu = new Cuadrado(a);
						cu.area();
					
					}else{
						JOptionPane.showMessageDialog(null, "El lado ingresado no es valido");

					}
				}catch(NumberFormatException e1){
					//e1.printStackTrace();
					JOptionPane.showMessageDialog(null, "Dato no valido, no se ingreso un n�mero o el campo est� vacio");

				}
				
			}
		});
		button_6.setBounds(7, 203, 122, 23);
		panel_3.add(button_6);
		
		JButton button_7 = new JButton("PERIMETRO");
		button_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					double a=Double.parseDouble(ladoc.getText());
					
					if (a>0 ) {
						Cuadrado cu = new Cuadrado(a);
						
						cu.perimetro();
					
					}else{
						JOptionPane.showMessageDialog(null, "El lado ingresado no es valido");

					}
				}catch(NumberFormatException e1){
					
					JOptionPane.showMessageDialog(null, "Dato no valido, no se ingreso un n�mero o el campo est� vacio");

				}
				
			}
		});
		button_7.setBounds(151, 203, 151, 23);
		panel_3.add(button_7);
		
		JButton button_8 = new JButton("SALIR");
		button_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
			
		});
		button_8.setBounds(327, 203, 89, 23);
		panel_3.add(button_8);
		
		JButton btnLimpiar = new JButton("LIMPIAR");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ladoc.setText(null);
			}
		});
		btnLimpiar.setBounds(181, 169, 89, 23);
		panel_3.add(btnLimpiar);
		
		JLabel lblLado = new JLabel("LADO:");
		lblLado.setBounds(75, 86, 36, 14);
		panel_3.add(lblLado);
		
		ladoc = new JTextField();
		ladoc.setColumns(10);
		ladoc.setBounds(133, 83, 196, 20);
		panel_3.add(ladoc);
		
		JTabbedPane tabbedPane_1 = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane_1.setBounds(0, 0, 5, 5);
		contentPane.add(tabbedPane_1);
	}
}
