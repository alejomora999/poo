package PUNTO2;

import javax.swing.JOptionPane;

public class Triangulo extends Figura {
	private double base = 0;
	private double altura = 0;
	private double a = 0;
	private double b = 0;
	private double c = 0;
	private double l = 0;
	
	

	public Triangulo(double base, double altura) {
		super(base, altura);
		this.base = base;
		this.altura = altura;

		// TODO Auto-generated constructor stub
	}
	

	public void area(double a, double b, double c) {
		double perimetro;
		double area;
		//CREO LA "S" DE LA FORMULA DE HERON
		perimetro= (a+b+c)/2;
		//OPERO
		area= ((perimetro)*(perimetro-a)*(perimetro-b)*(perimetro-c));
		area= Math.sqrt(area);
		JOptionPane.showMessageDialog(null, "El �rea del triangulo es: "+area+" unidades cuadradas");

	}

	public void perimetro(double a, double b, double c) {
		this.a=a;
		this.b=b;
		this.c=c;
		JOptionPane.showMessageDialog(null, "El perimetro del triangulo escaleno es: "+(a+b+c)+" unidades");
	}

	public void perimetro(double l, double b){
		this.l=l;
		this.b=b;
		JOptionPane.showMessageDialog(null, "El perimetro del triangulo isoceles es: "+(2*l+b)+" unidades");
	}

	public void perimetro(double l) {
		this.l=l;
		JOptionPane.showMessageDialog(null, "El perimetro del triangulo equil�tero es: "+(3*l)+" unidades");

	}


	@Override
	public void area() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void perimetro() {
		// TODO Auto-generated method stub
		
	}

}
