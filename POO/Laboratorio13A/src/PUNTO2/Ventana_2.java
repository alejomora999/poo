package PUNTO2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class Ventana_2 extends JFrame {

	private JPanel contentPane;
	private JTextField textField_2;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private ButtonGroup botones = new ButtonGroup();


	public Ventana_2() {
		setTitle("PERIMETRO RECTANGULOS");
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.PINK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JRadioButton rdbtnNewRadioButton = new JRadioButton("ESCALENO");
		rdbtnNewRadioButton.setBackground(Color.PINK);
		rdbtnNewRadioButton.setBounds(66, 72, 109, 23);
		contentPane.add(rdbtnNewRadioButton);

		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("ISOSCELES");
		rdbtnNewRadioButton_1.setBackground(Color.PINK);
		rdbtnNewRadioButton_1.setBounds(66, 124, 109, 23);
		contentPane.add(rdbtnNewRadioButton_1);

		JRadioButton rdbtnNewRadioButton_2 = new JRadioButton("EQUIATERO");
		rdbtnNewRadioButton_2.setBackground(Color.PINK);
		rdbtnNewRadioButton_2.setBounds(66, 174, 109, 23);
		contentPane.add(rdbtnNewRadioButton_2);
		botones.add(rdbtnNewRadioButton);
		botones.add(rdbtnNewRadioButton_2);
		botones.add(rdbtnNewRadioButton_1);
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(175, 73, 53, 20);
		contentPane.add(textField_2);

		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(238, 73, 53, 20);
		contentPane.add(textField);

		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(307, 73, 53, 20);
		contentPane.add(textField_1);

		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(205, 125, 53, 20);
		contentPane.add(textField_3);

		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(268, 125, 53, 20);
		contentPane.add(textField_4);

		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(238, 175, 53, 20);
		contentPane.add(textField_5);

		JLabel lblA = new JLabel("A");
		lblA.setBounds(198, 48, 19, 14);
		contentPane.add(lblA);

		JLabel lblB = new JLabel("B");
		lblB.setBounds(257, 48, 19, 14);
		contentPane.add(lblB);

		JLabel lblC = new JLabel("C");
		lblC.setBounds(326, 48, 19, 14);
		contentPane.add(lblC);

		JLabel lblL = new JLabel("l");
		lblL.setBounds(224, 104, 19, 14);
		contentPane.add(lblL);

		JLabel lblB_1 = new JLabel("b");
		lblB_1.setBounds(286, 104, 19, 14);
		contentPane.add(lblB_1);

		JLabel label = new JLabel("l");
		label.setBounds(257, 156, 19, 14);
		contentPane.add(label);

		JButton btnPerimetro = new JButton("PERIMETRO");
		btnPerimetro.setForeground(Color.BLACK);
		btnPerimetro.setBackground(Color.BLUE);
		btnPerimetro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					if (rdbtnNewRadioButton.isSelected()) {

						Double a = Double.parseDouble(textField_1.getText());
						Double b = Double.parseDouble(textField.getText());
						Double c = Double.parseDouble(textField_2.getText());
						if(a>0&& b>0 &&c>0){
							if (a+b<c || b+c<a||a+c<b){
								JOptionPane.showMessageDialog(null, "El triangulo no existe");

							}else{
							Triangulo tr = new Triangulo(a,b);
							tr.perimetro(a,b,c);
							}
						}else if(a>0&& b>0 &&c<=0){
							JOptionPane.showMessageDialog(null, "C es negativo o es cero");
						}else if(a>0&& b<=0 &&c>0){
							JOptionPane.showMessageDialog(null, "B es negativo o es cero");
						}else if(a<=0&& b>0 &&c>0){
							JOptionPane.showMessageDialog(null, "A es negativo o es cero");
						}else{
							JOptionPane.showMessageDialog(null, "Dos o m�s n�meros son negativo o son cero");
						}

					} else if (rdbtnNewRadioButton_1.isSelected()) {
						Double l = Double.parseDouble(textField_3.getText());
						Double b = Double.parseDouble(textField_4.getText());
						if(l>0&& b>0 ){
							if(2l<b){
								JOptionPane.showMessageDialog(null, "El triangulo no existe");

							}else{
							Triangulo tr = new Triangulo(0,b);
							tr.perimetro(l,b);
							}
						}else if(l>0&& b<=0){
							JOptionPane.showMessageDialog(null, "b es negativo o es cero");
						}else if(l<=0&& b>0){
							JOptionPane.showMessageDialog(null, "l es negativo o es cero");
						}else{
							JOptionPane.showMessageDialog(null, "Ambos son negativo o son cero");
						}

					} else if (rdbtnNewRadioButton_2.isSelected()) {
						Double l = Double.parseDouble(textField_5.getText());
						if(l>0 ){
							Triangulo tr = new Triangulo(l,0);
							tr.perimetro(l);
							
						}else{
							JOptionPane.showMessageDialog(null, "El dato es negativo o es cero");
						}
					} else {
						JOptionPane.showMessageDialog(null, "Seleccione alguna opci�n");

					}
				} catch (java.lang.NumberFormatException e1) {
					JOptionPane.showMessageDialog(null, "Datos incorrectos o campos en blanco");

				}

			}
		});
		btnPerimetro.setBounds(152, 204, 116, 23);
		contentPane.add(btnPerimetro);

		JButton btnSalir = new JButton("SALIR");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnSalir.setBounds(326, 227, 89, 23);
		contentPane.add(btnSalir);
		
		JButton btnLimpiar = new JButton("LIMPIAR");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 textField_2.setText(null);
				textField.setText(null);
				 textField_1.setText(null);
				 textField_3.setText(null);
				 textField_4.setText(null);
				 textField_5.setText(null);
			}
		});
		btnLimpiar.setBounds(10, 227, 89, 23);
		contentPane.add(btnLimpiar);
	}
}
