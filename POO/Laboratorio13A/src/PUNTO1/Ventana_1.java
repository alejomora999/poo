package PUNTO1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class Ventana_1 extends JFrame {

	private JPanel contentPane;
	private JTextField EstudianteNombre;
	private JTextField EstudianteA1;
	private JTextField EstudianteA2;
	private JTextField EstudianteEdad;
	private JTextField EstudianteCarrera;
	private JTextField EstudianteCódigo;
	private JTextField DocenteNombre;
	private JTextField DocenteA1;
	private JTextField DocenteA2;
	private JTextField DocenteEdad;
	private JTextField DocenteAsignatura;
	private JTextField DocenteEstudios;
	private  int k=0;
	private int k1=0;
	private  int k2=0;
	private int k3=0;
	ArrayListP a= new ArrayListP();
	
	public Ventana_1() {
		setTitle("REGISTRO DE GRUPOS");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setToolTipText("");
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 434, 261);
		contentPane.add(tabbedPane);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.ORANGE);
		tabbedPane.addTab("Docente", null, panel_1, null);
		panel_1.setLayout(null);
		
		JLabel label = new JLabel("Nombre:");
		label.setBounds(22, 33, 89, 23);
		panel_1.add(label);
		
		DocenteNombre = new JTextField();
		DocenteNombre.setColumns(10);
		DocenteNombre.setBounds(133, 34, 196, 20);
		panel_1.add(DocenteNombre);
		
		JLabel label_1 = new JLabel("Primer Apellido:");
		label_1.setBounds(22, 62, 107, 14);
		panel_1.add(label_1);
		
		DocenteA1 = new JTextField();
		DocenteA1.setColumns(10);
		DocenteA1.setBounds(133, 62, 196, 20);
		panel_1.add(DocenteA1);
		
		JLabel label_2 = new JLabel("Segundo Apellido:");
		label_2.setBounds(22, 90, 107, 14);
		panel_1.add(label_2);
		
		DocenteA2 = new JTextField();
		DocenteA2.setColumns(10);
		DocenteA2.setBounds(133, 87, 196, 20);
		panel_1.add(DocenteA2);
		
		JLabel label_3 = new JLabel("Edad:");
		label_3.setBounds(22, 115, 46, 14);
		panel_1.add(label_3);
		
		DocenteEdad = new JTextField();
		DocenteEdad.setColumns(10);
		DocenteEdad.setBounds(133, 115, 196, 20);
		panel_1.add(DocenteEdad);
		
		JLabel lblAsignatura = new JLabel("Asignatura:");
		lblAsignatura.setBounds(22, 140, 89, 14);
		panel_1.add(lblAsignatura);
		
		DocenteAsignatura = new JTextField();
		DocenteAsignatura.setColumns(10);
		DocenteAsignatura.setBounds(133, 140, 196, 20);
		panel_1.add(DocenteAsignatura);
		
		JLabel lblEstudios = new JLabel("Estudios:");
		lblEstudios.setBounds(22, 165, 89, 14);
		panel_1.add(lblEstudios);
		
		DocenteEstudios = new JTextField();
		DocenteEstudios.setColumns(10);
		DocenteEstudios.setBounds(133, 162, 196, 20);
		panel_1.add(DocenteEstudios);
		
		JButton btnCrearDocente = new JButton("Crear Docente");
		btnCrearDocente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
					if (DocenteNombre.getText().equals("")||DocenteA1.getText().equals("")||DocenteA2.getText().equals("")||DocenteEdad.getText().equals("")||DocenteAsignatura.getText().equals("")||DocenteEstudios.getText().equals("")){
						JOptionPane.showMessageDialog(null, "Campos Vacios, Por favor llenelos");

					}else{
						Persona Docentes = new Persona (DocenteNombre.getText(),DocenteA1.getText(),DocenteA2.getText(),DocenteEdad.getText());
						Docente Docente = new Docente();
						Docente.setAsignatura(DocenteAsignatura.getText());
						Docente.setEstudios(DocenteEstudios.getText());
						k3++;
						k2++;
						String a1= Integer.toString(k3);
						a.datosArrayList( a1 ,DocenteNombre.getText(), DocenteA1.getText(), DocenteA2.getText(), DocenteEdad.getText(), DocenteAsignatura.getText(), DocenteEstudios.getText());
						JOptionPane.showMessageDialog(null, "Docente Creado");

						DocenteNombre.setText(null);
						 DocenteA1.setText(null);
						 DocenteA2.setText(null);
						 DocenteEdad.setText(null);
						 DocenteAsignatura.setText(null);
						 DocenteEstudios.setText(null);
					}
				
				
			}
		});
		btnCrearDocente.setBounds(10, 199, 122, 23);
		panel_1.add(btnCrearDocente);
		
		JButton button_2 = new JButton("Salir");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		button_2.setBounds(330, 199, 89, 23);
		panel_1.add(button_2);
		
		JButton button = new JButton("Limpiar Registro");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 DocenteNombre.setText(null);
				 DocenteA1.setText(null);
				 DocenteA2.setText(null);
				 DocenteEdad.setText(null);
				 DocenteAsignatura.setText(null);
				 DocenteEstudios.setText(null);
			}
		});
		button.setBounds(154, 199, 151, 23);
		panel_1.add(button);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.CYAN);
		panel.setToolTipText("");
		tabbedPane.addTab("Estudiante", null, panel, null);
		panel.setLayout(null);
		
		JButton btnNewButton_1 = new JButton("Salir");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnNewButton_1.setBounds(330, 199, 89, 23);
		panel.add(btnNewButton_1);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(22, 33, 82, 23);
		panel.add(lblNombre);
		
		JLabel lblPrimerApellido = new JLabel("Primer Apellido:");
		lblPrimerApellido.setBounds(22, 62, 107, 14);
		panel.add(lblPrimerApellido);
		
		JLabel lblSegundoApellido = new JLabel("Segundo Apellido:");
		lblSegundoApellido.setBounds(22, 90, 107, 14);
		panel.add(lblSegundoApellido);
		
		JLabel lblEdad = new JLabel("Edad:");
		lblEdad.setBounds(22, 115, 46, 14);
		panel.add(lblEdad);
		
		JLabel lblCarrera = new JLabel("Carrera:");
		lblCarrera.setBounds(22, 140, 71, 14);
		panel.add(lblCarrera);
		
		JLabel lblCdigo = new JLabel("C\u00F3digo:");
		lblCdigo.setBounds(22, 165, 82, 14);
		panel.add(lblCdigo);
		
		EstudianteNombre = new JTextField();
		EstudianteNombre.setBounds(133, 34, 196, 20);
		panel.add(EstudianteNombre);
		EstudianteNombre.setColumns(10);
		
		EstudianteA1 = new JTextField();
		EstudianteA1.setColumns(10);
		EstudianteA1.setBounds(133, 62, 196, 20);
		panel.add(EstudianteA1);
		
		EstudianteA2 = new JTextField();
		EstudianteA2.setColumns(10);
		EstudianteA2.setBounds(133, 87, 196, 20);
		panel.add(EstudianteA2);
		
		EstudianteEdad = new JTextField();
		EstudianteEdad.setColumns(10);
		EstudianteEdad.setBounds(133, 115, 196, 20);
		panel.add(EstudianteEdad);
		
		EstudianteCarrera = new JTextField();
		EstudianteCarrera.setColumns(10);
		EstudianteCarrera.setBounds(133, 140, 196, 20);
		panel.add(EstudianteCarrera);
		
		EstudianteCódigo = new JTextField();
		EstudianteCódigo.setColumns(10);
		EstudianteCódigo.setBounds(133, 162, 196, 20);
		panel.add(EstudianteCódigo);
		
		JButton btnCrearEstudiante = new JButton("Crear Estudiante");
		btnCrearEstudiante.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btnCrearDocente.isEnabled()){
					if (EstudianteNombre.getText().equals("")||EstudianteA1.getText().equals("")||EstudianteA2.getText().equals("")||EstudianteEdad.getText().equals("")||EstudianteCarrera.getText().equals("")||EstudianteCódigo.getText().equals("")){
						JOptionPane.showMessageDialog(null, "Campos Vacios, Por favor llenelos");

					}else{
						Persona Alumno = new Persona (EstudianteNombre.getText(),EstudianteA1.getText(),EstudianteA2.getText(),EstudianteEdad.getText());
						Estudiante Estudiantes = new Estudiante();
						Estudiantes.setCarrera(EstudianteCarrera.getText());
						Estudiantes.setCodigo(EstudianteCódigo.getText());
						//ArrayListP b= new ArrayListP();
						a.datosArrayList("Estudiante",EstudianteNombre.getText(), EstudianteA1.getText(), EstudianteA2.getText(), EstudianteEdad.getText(), EstudianteCarrera.getText(), EstudianteCódigo.getText());
						k2++;
						JOptionPane.showMessageDialog(null, "Estudiante Creado");

						 EstudianteNombre.setText(null);
						 EstudianteA1.setText(null);
						 EstudianteA2.setText(null);
						 EstudianteEdad.setText(null);
						  EstudianteCarrera.setText(null);
						  EstudianteCódigo.setText(null);
					}
				}else{
					JOptionPane.showMessageDialog(null, "Campos Vacios, Por favor llenelos");
				}
				
					
				
				
				
			}
		});
		btnCrearEstudiante.setBounds(10, 199, 140, 23);
		panel.add(btnCrearEstudiante);
		
		JButton btnLimpiarRegistro = new JButton("Limpiar Registro");
		btnLimpiarRegistro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 EstudianteNombre.setText(null);
				 EstudianteA1.setText(null);
				 EstudianteA2.setText(null);
				 EstudianteEdad.setText(null);
				  EstudianteCarrera.setText(null);
				  EstudianteCódigo.setText(null);
			}
		});
		btnLimpiarRegistro.setBounds(160, 199, 148, 23);
		panel.add(btnLimpiarRegistro);
		
		JButton btnCrearGrupo = new JButton("Crear Grupo");
		btnCrearGrupo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btnCrearDocente.isSelected()&& btnCrearEstudiante.isSelected()){
					k++;
					
				
				}else{
					JOptionPane.showMessageDialog(null, "No ha Creado Docentes o Estudiantes Respectivamente");

				}
					
				
			}
		});
		btnCrearGrupo.setBounds(160, 0, 129, 23);
		panel.add(btnCrearGrupo);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.MAGENTA);
		tabbedPane.addTab("Imprimir", null, panel_2, null);
		panel_2.setLayout(null);
		
		JButton btnMostar = new JButton("Mostar");
		btnMostar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
					//ArrayListP d= new ArrayListP();
					//ArrayListP c= new ArrayListP();
					
						
							
							
							a.mostar(k2);
							
						
					
				
			}
		});
		btnMostar.setBounds(163, 104, 89, 23);
		panel_2.add(btnMostar);
		
		JButton button_1 = new JButton("Salir");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		button_1.setBounds(330, 199, 89, 23);
		panel_2.add(button_1);
		
		JTabbedPane tabbedPane_1 = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane_1.setBounds(0, 0, 5, 5);
		contentPane.add(tabbedPane_1);
	}
}
