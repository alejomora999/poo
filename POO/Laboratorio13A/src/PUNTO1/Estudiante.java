package PUNTO1;

//ESTA ES UNA SUBCLASE
public class Estudiante extends Persona {
	// ATRIBUTOS DE TIPO PRIVATE PROPIOS DE LA SUBCLASE
	private String Carrera;
	private String Codigo;

	// CONSTRUCTOR DE LA SUBLCASE CON EL INDICADOR SUPER() EN ELLA
	public Estudiante(String Nombre, String PrimerApellido, String SegundoApellido, String Edad) {
		super(Nombre, PrimerApellido, SegundoApellido, Edad);
		Carrera = ("");
		Codigo = ("");
	}

	public Estudiante() {

	}

	// GETTER AND SETTER
	public String getCarrera() {
		return Carrera;
	}

	public void setCarrera(String carrera) {
		Carrera = carrera;
	}

	public String getCodigo() {
		return Codigo;
	}

	public void setCodigo(String codigo) {
		Codigo = codigo;
	}

}
