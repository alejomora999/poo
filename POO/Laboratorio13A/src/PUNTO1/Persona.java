package PUNTO1;

//ESTA ES LA SUPERCLASE
public class Persona {
	// CREO ATRIBUTOS DE PROTEGIDO
	protected String Nombre;
	protected String PrimerApellido;
	protected String SegundoApellido;
	protected String Edad;

	// CONSTRUCTOR
	public Persona() {
		Nombre = (" ");
		PrimerApellido = (" ");
		SegundoApellido = (" ");
		Edad = ("");
	}

	// SOBRECARGA DE CONSTRUCTORES
	public Persona(String Nombre, String PrimerApellido, String SegundoApellido, String Edad) {
		this.Nombre = Nombre;
		this.PrimerApellido = PrimerApellido;
		this.SegundoApellido = SegundoApellido;
		this.Edad = Edad;
	}
	// GETTERS AND SETTERS

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public String getPrimerApellido() {
		return PrimerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		PrimerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return SegundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		SegundoApellido = segundoApellido;
	}

	public String getEdad() {
		return Edad;
	}

	public void setEdad(String edad) {
		Edad = edad;
	}
	// METODO DONDE IMPRIMO LA INFORMACION DEL PRODUCTO
	// public void imprimirInfo() {
	// System.out.println("Producto:"+getNombreProducto()+"\nFecha Caducidad: "
	// + getFechaCaducidad() + "\nNumero de Lote: " + getNumeroLote());
	// }
}
