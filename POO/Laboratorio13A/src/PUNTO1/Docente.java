package PUNTO1;

//ESTA ES UNA SUBCLASE
public class Docente extends Persona {
	// ATRIBUTOS DE TIPO PRIVATE PROPIOS DE LA SUBCLASE
	private String Asignatura;
	private String Estudios;

	// CONSTRUCTOR DE LA SUBLCASE CON EL INDICADOR SUPER() EN ELLA
	public Docente(String Nombre, String PrimerApellido, String SegundoApellido, String Edad) {
		super(Nombre, PrimerApellido, SegundoApellido, Edad);
		Asignatura = ("");
		Estudios = ("");
	}

	public Docente() {

	}
	// GETTERS AND SETTERS

	public String getAsignatura() {
		return Asignatura;
	}

	public void setAsignatura(String asignatura) {
		Asignatura = asignatura;
	}

	public String getEstudios() {
		return Estudios;
	}

	public void setEstudios(String estudios) {
		Estudios = estudios;
	}

}
