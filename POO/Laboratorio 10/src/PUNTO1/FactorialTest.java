/*
 *NOMBRE DEL PROGRAMA : FACTORIAL RECURSIVO
 * AUTOR:NICOLAS MENESES GUERRERO
 *FECHA: 14  OCT DE 2016
 */
package PUNTO1;

import java.util.Scanner;

public class FactorialTest {
	private static Scanner leer;

	public static void main(String arg[]) {
		// Defino la variable reiniciar, sera la que determine si quiero repetir
		// el programa de nuevo o no
		int reiniciar = 0;
		// Creo atributos donde almaceno los datos ingresado por el usuario
		int numero = 0;

		do {
			leer = new Scanner(System.in);
			System.out.println(
					"este programa calcula el factorial de un n�mero de manera iterativa y recursiva:");
			// PIDO EL N�MERO
			System.out.println("Ingrese el n�mero al cual le desea calcular su factorial");
			numero = leer.nextInt();
			// CREO MI OBJETO PERTENECIENTE A LA CLASE FACTORIAL
			Factorial fact = new Factorial();
			// VERIFICO ALGUN POSIBLE ERROR CON EL DATO INGRESADO
			while (numero < 0) {
				System.out.println("Ingresa un n�mero valido");
				numero = leer.nextInt();
			}
			if (numero == 0) {
				System.out.println("El facotrial es de 0 es 1");
			} else if (numero > 127) {
				System.out.println(
						"El facorial de tu n�mero si existe, pero es muy 1grande y Java no puede calcularlo, intenta con un n�mero menor a 127.");
			} else if (numero > 0 && numero < 127) {
				// LLAMO A LOS OBJETOS DE LA CALSE FACTORIAL CON MI OBJETO
				System.out.println(
						"El factorial de " + numero + " de forma iterativa es: " + fact.FormaIterativa(numero));
				System.out.println(
						"El factorial de " + numero + " de forma recursiva es: " + fact.FormaRecursiva(numero));
			}

			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = leer.nextInt();
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = leer.nextInt();
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
		System.out.println("FIN DEL PROGRAMA");

	}

}
