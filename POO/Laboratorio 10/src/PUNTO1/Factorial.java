/*
 *NOMBRE DEL PROGRAMA : FACTORIAL RECURSIVO
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA:14  OCT DE 2016
 */
package PUNTO1;

public class Factorial {
	// CREO LOS EL ATRIBUTOS QUE VOY A USAR
	private int numero;

	// CREO EL METODO DONDE DARE EL FACTORIAL DE FORMA ITERATIVA
	public int FormaIterativa(int numero) {
		this.numero = numero;
		int contador = 1;
		for (int i = this.numero; i >= 1; i--) {
			contador = (contador * i);
		}
		return contador;
	}

	// CREO EL METODO DONDE DARE EL FACTORIAL DE FORMA RECURSIVA
	public int FormaRecursiva(int numero) {
		this.numero = numero;
		if (numero == 0) {
			return 1;
		} else {
			return numero * FormaRecursiva(numero - 1);
		}
	}

	// GETTER AND SETTER
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

}
