/*
 *NOMBRE DEL PROGRAMA : FIBONACCI RECURSIVO
 * AUTOR:NICOLAS MENESES GUERRERO
 *FECHA: 14 DE OCT DE 2016
 */
package PUNTO2;

import java.util.Scanner;

public class FibonacciTest {
	private static Scanner leer;

	public static void main(String arg[]) {
		// Defino la variable reiniciar, sera la que determine si quiero repetir
		// el programa de nuevo o no
		int reiniciar = 0;
		// Creo atributos donde almaceno los datos ingresado por el usuario
		int numero = 0;

		do {
			leer = new Scanner(System.in);
			// DOY UN SALUDO
			System.out.println(
					"Bienvenido,  este programa calcula la suceci�n de Fibonacci de manera iterativa y recursiva:");
			// PIDO EL N�MERO
			System.out.println("Ingrese el n�mero de elementos que desea ver");
			numero = leer.nextInt();
			// CREO MI OBJETO PERTENECIENTE A LA CLASE FIBONACCI
			Fibonacci fibo = new Fibonacci();
			// VERIFICO ALGUN POSIBLE ERROR CON EL DATO INGRESADO
			while (numero <= 0) {
				System.out.println("Ingresa un n�mero valido");
				numero = leer.nextInt();
			}
			if (numero > 0) {
				// LLAMO A LOS OBJETOS DE LA CALSE FIBONACCI CON MI OBJETO
				System.out.println("La sucesion de fibonacci de manera iterativa mostrando " + numero + " n�meros es:");
				fibo.iteracion(numero);
				System.out.println();
				System.out.println("La sucesion de fibonacci de manera recursiva mostrando " + numero + " n�meros es:");
				fibo.mostrarfibo(numero);
			}
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = leer.nextInt();
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = leer.nextInt();
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
		System.out.println("FIN DEL PROGRAMA");

	}
}
