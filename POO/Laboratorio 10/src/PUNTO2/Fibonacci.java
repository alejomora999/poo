/*
 *NOMBRE DEL PROGRAMA : FIBONACCI RECURSIVO
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 14 DE OCT DE 2016
 */
package PUNTO2;

public class Fibonacci {
	// CREO UN ATRIBUTO DE TIPO PRIVATE
	private int numero = 0;

	// CREO EL METODO DONDE MUESTRO LA SUCECION DE MANERA RECURSIVA
	public void mostrarfibo(int numero) {
		for (int i = 0; i <= numero; i++) {
			System.out.println(fibo(i));
		}
	}

	// CREO EL METODO EN DONDE FORMO LA SUCECION DE MANERA RECURSIVA
	public long fibo(int numero) {
		if (numero == 0) {
			return 0;
		}
		if (numero <= 2) {
			return 1;
		} else {
			long fibo = fibo(numero - 1) + fibo(numero - 2);
			return fibo;
		}

	}

	// CREO EL METODO EN DONDE FORMO LA SUCECION DE FORMA ITERATIVA
	public void iteracion(int numero) {
		// CREO UNOS ATRIBUTOS AUXILIARES
		int limite1 = 0;
		int limite2 = 1;
		int fibo = limite1 + limite2;
		for (int i = 1; i <= numero; i++) {
			// IMPRIMO LA SUCECION
			System.out.println(fibo);
			fibo = (limite1 + limite2);
			limite1 = limite2;
			limite2 = fibo;

		}
	}

	// GETTER AND SETTER
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

}
