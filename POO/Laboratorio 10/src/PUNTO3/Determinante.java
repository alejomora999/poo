/*
 *NOMBRE DEL PROGRAMA : DETERMINANTE RECURSIVO
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 14 DE OCT DE 2016
 */
package PUNTO3;

import java.util.Scanner;

public class Determinante {
	private Scanner leer;

	// CREO UN METODO DONDE LLENO LAS MATRICES POR TECLADO
	public void llenarMatriz(double M1[][]) {
		leer = new Scanner(System.in);
		for (int i = 0; i < M1.length; ++i) {
			for (int j = 0; j < M1.length; ++j) {
				System.out.println(" Elemento " + i + " " + j);
				M1[i][j] = leer.nextDouble();
			}
		}
	}

	// CREO UN METODO DONDE MUESTRO LAS MATRICES
	public void mostrarMatriz(double M1[][]) {
		for (int i = 0; i < M1.length; i++) {
			for (int j = 0; j < M1.length; j++) {
				System.out.print(M1[i][j] + "  ");
			}
			System.out.println();
		}
	}

	// CREO LE METODO DONDE CALCULO EL DETERMNATE DE FORMA RECURSIVA
	// (COFACTORES)

	public double recursivo(int i, double[][] matriz) {
		if (matriz.length == 2) {
			double deter = matriz[0][0] * matriz[1][1] - matriz[0][1] * matriz[1][0];

			return deter;
		}

		else {
			double deter = 0;

			for (int j = 0; j < matriz.length; j++) {
				double[][] temp = this.SubMatriz(i, j, matriz);

				deter = deter + Math.pow(-1, i + j) * matriz[i][j] * this.recursivo(0, temp);
			}
			return deter;
		}
	}

	// CALCULO DE SUBMATRIZ ELIMINADO I,J
	private double[][] SubMatriz(int i, int j, double[][] matriz) {
		double[][] temp = new double[matriz.length - 1][matriz.length - 1];
		int count1 = 0;
		int count2 = 0;
		for (int k = 0; k < matriz.length; k++) {
			if (k != i) {
				count2 = 0;
				for (int l = 0; l < matriz.length; l++) {
					if (l != j) {
						temp[count1][count2] = matriz[k][l];
						count2++;
					}
				}
				count1++;
			}
		}
		return temp;
	}

	// CREO EL METODO DONDE CALCULO EL DETERMIANTEN DE FORMA ITERATIVA
	public void iterativa(double M[][]) {

		for (int k = 0; k < M.length; k++) {
			for (int i = k + 1; i < M.length; i++) {
				for (int j = k + 1; j < M.length; j++) {
					M[i][j] -= M[i][k] * M[k][j] / M[k][k];
				}
			}
		}
		double deter = 1.0;
		for (int i = 0; i < M.length; i++) {
			deter *= M[i][i];
		}
		System.out.println(deter);
	}

}
