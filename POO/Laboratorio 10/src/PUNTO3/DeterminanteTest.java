/*
 *NOMBRE DEL PROGRAMA : DETERMINANTE RECURSIVO
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 14  OCT DE 2016
 */
package PUNTO3;

import java.util.Scanner;

public class DeterminanteTest {
	private static Scanner leer;

	public static void main(String arg[]) {
		// Defino la variable reiniciar, sera la que determine si quiero repetir
		// el programa de nuevo o no
		int reiniciar = 0;
		// Creo atributos donde almaceno los datos ingresado por el usuario
		double determinate = 0;
		do {
			leer = new Scanner(System.in);
			System.out.println(
					"este programa calcula el determiante de una matriz 3x3 de forma recursiva e iterativa :");
			// CREO MI MATRIZ DE 3x3
			double[][] matriz = new double[3][3];
			// CREO EL OBJETO DE LA CLASE DETERMINANTE
			Determinante matr = new Determinante();
			// LLENO LA MATRIZ
			matr.llenarMatriz(matriz);
			// MUESTRO LA MATRIZ
			matr.mostrarMatriz(matriz);
			// LLAMO EL DETERMINANTE DE FORMA RECURSIVA
			determinate = matr.recursivo(0, matriz);
			System.out.println("El  determinante en forma recursiva es: ");
			System.out.println(determinate);
			System.out.println("El  determinante en forma iterativa es: ");
			// LLAMO EL DETERMINANTE DE FORMA ITERATIVA
			matr.iterativa(matriz);
			System.out.print("\n ");
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = leer.nextInt();
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = leer.nextInt();
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
		System.out.println("FIN DEL PROGRAMA");

	}
}
