/*
 * NOMBRE: AREA DEL CUADRADO, ROMBO, CIRCULO 
 * AUTOR: NICOLAS MENESES GUERRERO
 * FECHA: 31 AGO 2026
 */
package punto1;
import java.io.*;
public class AreaCuadradoRomboCirculo {
	public static void main (String arg[]) throws IOException{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Ingresando el numero 1 se calculara el area de un cuadrado  ");
		System.out.println("Ingresando el numero 2 se calculara el area de un  rombo ");
		System.out.println("Ingresando el numero 3 se calculara el area de un circulo");
		System.out.println("digitando un numero superior a 4  el programa se terminara ");
		int opcion;
		do  {
			//con este valor se sabra que area se desea saber
			opcion =Integer.parseInt(in.readLine( ));
			double areacua, base, altura, diagmayor, diagmenor, arearombo, radio, areacir; 
			switch (opcion){
			 case 1: 
             System.out.println("ingrese la base del cuadrado");
             base=Double.parseDouble(in.readLine());
             if (base<0){
            	 System.out.println("El dato no puede ser negativo digite uno positivo la proxima vez, el programa se reiniciara:");
            	continue;
                 /* no procesar el valor negativo de base pedir valor positivo*/
             } else {
            	 base=base;
             }
             System.out.println("ingrese la altura del cuadrado");
             altura=Double.parseDouble(in.readLine());
             if (altura<0){
            	 System.out.println("El dato no puede ser negativo digite uno positivo la proxima vez, el programa se reiniciara:");
            	 continue;
                 /* no procesar el valor negativo de altura pedir el valor positivo*/
             }else{
            	 altura=altura;
             }
             areacua=(base*altura);
             System.out.println("El area del cuadrado es "+ areacua);
             break;
             case 2:
             System.out.println("Ingrese la diagonal mayor del rombo");
             diagmayor=Double.parseDouble(in.readLine());
             if (diagmayor<0){
            	 System.out.println("El dato no puede ser negativo digite uno positivo la proxima vez, el programa se reiniciara:");
            	 continue;
                 /* no procesar el valor negativo de diagonal mayor pedir el valor positivo*/
             }else{
            	 diagmayor=diagmayor ;
             }
             System.out.println("Ingrese la diagonal menor del rombo");
             diagmenor=Double.parseDouble(in.readLine());
             if (diagmenor<0){
            	 System.out.println("El dato no puede ser negativo digite uno positivo la proxima vez, el programa se reiniciara:");
            	 continue;
                 /* no procesar el valor negativo de diagonal menor pedir el valor positivo*/
             }else{
            	 diagmenor=diagmenor;
             }
             arearombo=(diagmayor*diagmenor)/2;
             System.out.println("El area del rombo es "+ arearombo);
             break;
             case 3:
             System.out.println("ingrese el radio del circulo");
             radio=Double.parseDouble(in.readLine());
             if (radio<0){
            	 System.out.println("El dato no puede ser negativo digite uno positivo la proxima vez, el programa se reiniciara:");
            	 continue;
                 /* no procesar el valor negativo de radio y pedir el valor positivo*/ 
             }else{
            	 radio=radio;
             }
             areacir= (Math.PI*(Math.pow(radio, 2)));
             System.out.println("El area del circulo es "+ areacir);
             break;
             }
             } while (opcion<4);
		System.out.println("El Programa Ha Terminado");
			}
		}
		