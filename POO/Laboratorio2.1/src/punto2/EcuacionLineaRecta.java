/*
 * NOMBRE: AREA DEL ECUACION LINEA RECTA 
 * AUTOR: NICOLAS MENESES GUERRERO
 * FECHA: 1 SEP 2026
 */
package punto2;
import java.io.*;
public class EcuacionLineaRecta {
	public static void main (String arg[]) throws IOException{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("digita el valor para saber si continua o termina el programa");
		int  valor;
		do{
			valor =Integer.parseInt(in.readLine( ));
		switch  (valor){
		case 1:
			// defino las coordenadas y la pendiente para haalar la ecuacion de la recta 
			double x, y;
			float m;
			System.out.println("digita la coordenada en x del punto");
			x=Double.parseDouble(in.readLine());
			System.out.println("digita la coordenada en y del punto");
			y=Double.parseDouble(in.readLine());
			System.out.println("digita la pendiente  de la recta");
			m=Float.parseFloat(in.readLine());
			System.out.println("La ecuacion de la recta se dara gracias al punto y pendiente dados");
			System.out.println(" y2 - "+y+ " = "+ m +(" x2 - "+x));
			
			break;
		case 2:	
			System.out.println("Fin");
			break;
		}
			
			
		}while (valor<2);
		System.out.println("El Programa Ha recibido la orden de no continuar");
	
			
		  
	}
}
