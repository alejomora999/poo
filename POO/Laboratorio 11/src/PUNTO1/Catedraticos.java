/*
 *NOMBRE DEL PROGRAMA : SALARIOS DE DOCENTES
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 19 DE OCT DE 2016
 */
package PUNTO1;

//ESTA ES UNA SUBCLASE DE LA CLASE DOCENTE
public class Catedraticos extends Docente {
	// ATRIBUTOS DE TIPO PRIVATE PROPIOS DE LA SUBCLASE
	private double SalarioCatedraticos;
	private double TotalSalarioCatedraticos;

	// CONSTRUCTOR DE LA SUBLCASE CON EL INDICADOR SUPER() EN ELLA
	public Catedraticos(String Nombre, String Apellido, String NivelEstudios, String TipoContratacion,
			int CantidadHoras) {
		super(Nombre, Apellido, NivelEstudios, TipoContratacion, CantidadHoras);
		// this.SalarioCatedraticos=0;
		// this.TotalSalarioCatedraticos=0;
	}

	// METODO PROPIO DE LA SUBLASE DONDE SE CALCULAN LOS SALARIOS DE LOS
	// DOCENTES CATEDRATICOS
	double SalariosCatedra() {

		if (NivelEstudios.equals("Doctorado")) {
			this.SalarioCatedraticos = (7000000 * 0.10) * CantidadHoras;
		} else if (NivelEstudios.equals("Maestría")) {
			this.SalarioCatedraticos = (5000000 * 0.10) * CantidadHoras;
		} else if (NivelEstudios.equals("Especialización")) {
			this.SalarioCatedraticos = (4000000 * 0.10) * CantidadHoras;
		} else if (NivelEstudios.equals("Sin Postgrado")) {
			this.SalarioCatedraticos = (3000000 * 0.10) * CantidadHoras;
		}
		this.TotalSalarioCatedraticos = this.SalarioCatedraticos;
		// RETORNO EL SALRIO DEL DOCENTE
		return this.TotalSalarioCatedraticos;
	}

	public double getSalarioCatedraticos() {
		return SalarioCatedraticos;
	}

	// GETTERS AND SETTERS
	public void setSalarioCatedraticos(double salarioCatedraticos) {
		SalarioCatedraticos = salarioCatedraticos;
	}

	public double getTotalSalarioCatedraticos() {
		return TotalSalarioCatedraticos;
	}

	public void setTotalSalarioCatedraticos(double totalSalarioCatedraticos) {
		TotalSalarioCatedraticos = totalSalarioCatedraticos;
	}
}
