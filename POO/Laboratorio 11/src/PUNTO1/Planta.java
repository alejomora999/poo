/*
 *NOMBRE DEL PROGRAMA : SALARIOS DE DOCENTES
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 19 DE OCT DE 2016
 */
package PUNTO1;

//ESTA ES UNA SUBCLASE DE LA CLASE DOCENTE
public class Planta extends Docente {
	// ATRIBUTOS DE TIPO PRIVATE PROPIOS DE LA SUBCLASE
	private double SalarioPlanta;
	private double TotalSalarioPlanta;

	// CONSTRUCTOR DE LA SUBLCASE CON EL INDICADOR SUPER() EN ELLA
	public Planta(String Nombre, String Apellido, String NivelEstudios, String TipoContratacion, int CantidadHoras) {
		super(Nombre, Apellido, NivelEstudios, TipoContratacion, CantidadHoras);

	}

	// METODO PROPIO DE LA SUBLASE DONDE SE CALCULAN LOS SALARIOS DE LOS
	// DOCENTES DE PLANTA
	double SalariosPLanta() {

		if (NivelEstudios.equals("Doctorado")) {
			this.SalarioPlanta = 7000000;
		} else if (NivelEstudios.equals("Maestría")) {
			this.SalarioPlanta = 5000000;
		} else if (NivelEstudios.equals("Especialización")) {
			this.SalarioPlanta = 4000000;
		} else if (NivelEstudios.equals("Sin Postgrado")) {
			this.SalarioPlanta = 3000000;
		}
		this.TotalSalarioPlanta = this.SalarioPlanta;
		// RETORNO EL SALRIO DEL DOCENTE
		return this.TotalSalarioPlanta;
	}

	// GETTERS AND SETTERS
	public double getSalarioPlanta() {
		return SalarioPlanta;
	}

	public void setSalarioPlanta(double salarioPlanta) {
		SalarioPlanta = salarioPlanta;
	}

	public double getTotalSalarioPlanta() {
		return TotalSalarioPlanta;
	}

	public void setTotalSalarioPlanta(double totalSalarioPlanta) {
		TotalSalarioPlanta = totalSalarioPlanta;
	}
}
