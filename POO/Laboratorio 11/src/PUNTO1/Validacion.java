/*
 *NOMBRE DEL PROGRAMA : SALARIOS DE DOCENTES
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 19 DE OCT DE 2016
 */
package PUNTO1;

//ESTA CALSE NO ES SUBCLASE DE DOCENTE
public class Validacion {
	// METODO PARA VALIDAR SI EL NIVEL DE ESTUDIOS DEL DOCENTE ES VALIDO O NO
	public boolean validar(String NivelEstudios) {
		if (NivelEstudios.equals("Doctorado") || NivelEstudios.equals("Maestría")
				|| NivelEstudios.equals("Especialización") || NivelEstudios.equals("Sin Postgrado")) {
			return false;
		} else {
			return true;
		}
	}

	// METODO PARA VALIDAR SI EL TIPO DE CONTRATACIÓN DEL DOCENTE ES VALIDO O NO
	public boolean validar1(String TipoContratacion) {
		if (TipoContratacion.equals("Planta") || TipoContratacion.equals("Vinculacion Especial")
				|| TipoContratacion.equals("Vinculación Especial") || TipoContratacion.equals("Catedratico")
				|| TipoContratacion.equals("Catedrático")) {
			return false;
		} else {
			return true;
		}
	}

	// METODO PARA VALIDAR SI LA CANTIDAD DE HORAS DEL DOCENTE CATEDRATICO ES
	// VALIDA O NO
	public boolean hora(int CantidadHoras) {
		if (CantidadHoras <= 0) {
			return true;

		} else {
			return false;
		}
	}
}
