/*
 *NOMBRE DEL PROGRAMA : SALARIOS DE DOCENTES
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 19 DE OCT DE 2016
 */
package PUNTO1;

//ESTA ES UNA SUBCLASE DE LA CLASE DOCENTE
public class VinculacionEspecial extends Docente {
	// ATRIBUTOS DE TIPO PRIVATE PROPIOS DE LA SUBCLASE
	private double SalarioVinculacionEspecial;
	private double TotalSalarioVinculacionEspecial;

	// CONSTRUCTOR DE LA SUBLCASE CON EL INDICADOR SUPER() EN ELLA
	public VinculacionEspecial(String Nombre, String Apellido, String NivelEstudios, String TipoContratacion,
			int CantidadHoras) {
		super(Nombre, Apellido, NivelEstudios, TipoContratacion, CantidadHoras);
		// this.SalarioVinculacionEspecial=0;
		// this.TotalSalarioVinculacionEspecial=0;
	}

	// METODO PROPIO DE LA SUBLASE DONDE SE CALCULAN LOS SALARIOS DE LOS
	// DOCENTES DE VINCULACION ESPECIAL
	double SalariosVinculacion() {

		if (NivelEstudios.equals("Doctorado")) {
			this.SalarioVinculacionEspecial = (7000000 * 0.85);
		} else if (NivelEstudios.equals("Maestría")) {
			this.SalarioVinculacionEspecial = (5000000 * 0.85);
		} else if (NivelEstudios.equals("Especialización")) {
			this.SalarioVinculacionEspecial = (4000000 * 0.85);
		} else if (NivelEstudios.equals("Sin Postgrado")) {
			this.SalarioVinculacionEspecial = (3000000 * 0.85);
		}
		this.TotalSalarioVinculacionEspecial = this.SalarioVinculacionEspecial;
		// RETORNO EL SALRIO DEL DOCENTE
		return this.TotalSalarioVinculacionEspecial;
	}

	// GETTERS AND SETTERS
	public double getSalarioVinculacionEspecial() {
		return SalarioVinculacionEspecial;
	}

	public void setSalarioVinculacionEspecial(double salarioVinculacionEspecial) {
		SalarioVinculacionEspecial = salarioVinculacionEspecial;
	}

	public double getTotalSalarioVinculacionEspecial() {
		return TotalSalarioVinculacionEspecial;
	}

	public void setTotalSalarioVinculacionEspecial(double totalSalarioVinculacionEspecial) {
		TotalSalarioVinculacionEspecial = totalSalarioVinculacionEspecial;
	}

}
