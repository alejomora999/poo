/*
 *NOMBRE DEL PROGRAMA : SALARIOS DE DOCENTES
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 19 DE OCT DE 2016
 */
package PUNTO1;
//LLAMO A SCANNER
import java.util.Scanner;

public class Ejecutar {
	private static Scanner leer;
	//CREO EL MAIN DEL PROGRAMA
	public static void main(String[] args) {
		// Defino la variable reiniciar, sera la que determine si quiero repetir
		// el programa de nuevo o no
		int reiniciar = 0;
		// Creo atributos donde almaceno los datos ingresado por el usuario
		int CantidadDocentes = 0;
		String Nombre = (" ");
		String Apellido = (" ");
		String NivelEstudios = (" ");
		String TipoContratacion = (" ");
		int CantidadHoras = 0;
		double SalarioPLanta = 0;
		double SalarioVinculacionEspecial = 0;
		double SalarioCatedraticos = 0;
		double SalarioTotal = 0;
		double SalarioPLanta1 = 0;
		double SalarioVinculacionEspecial1 = 0;
		double SalarioCatedraticos1 = 0;
		do {
			leer = new Scanner(System.in);
			// DOY UN SALUDO
			System.out
					.println("Bienvenido, le dire la cantidad de COP que debe pagar una Universidad a sus Docentes :");
			System.out.println("Cuantos Docentes desea ingresar :");
			CantidadDocentes = leer.nextInt();
			if (CantidadDocentes > 0) {
				// CREO UN FOR DONDE PEDIRE LOS DATOS
				for (int i = 0; i < CantidadDocentes; i++) {
					Validacion validar = new Validacion();
					leer = new Scanner(System.in);
					System.out.print("Ingrese el Docente n�mero " + (i + 1) + ":");
					System.out.println();
					System.out.println("Ingrese el Nombre");
					Nombre = leer.nextLine();
					System.out.println("Ingrese el Apellido");
					Apellido = leer.nextLine();
					// PIDO Y VALIDO EL NIVEL DE ESTUDIOS
					System.out.println("Ingrese el Nivel de Estudios");
					NivelEstudios = leer.nextLine();
					while (validar.validar(NivelEstudios)) {
						System.out.println("Ingrese un Nivel de Estudio Valido");
						NivelEstudios = leer.nextLine();
					}
					// PIDO Y VALIDO EL TIPO DE CONTRATACI�N
					System.out.println("Ingrese el Tipo de Contrataci�n");
					TipoContratacion = leer.nextLine();
					while (validar.validar1(TipoContratacion)) {
						System.out.println("Ingrese un Tipo de Contratci�n Valido");
						NivelEstudios = leer.nextLine();

					}
					// VALIDO DE QUE TIPO DE DOCENTE SE HA INGRESADO INFORMACION

					if (TipoContratacion.equals("Catedratico") || TipoContratacion.equals("Catedr�tico")) {
						// SI EL DOCENTE ES CATEDRATICO PIDO INGRESAR N�MERO DE
						// HORAS Y LAS VERIFICO
						System.out.println("Su docente es catedr�tico, ingrese el n�mero de horas dictadas al mes");
						CantidadHoras = leer.nextInt();
						if (validar.hora(CantidadHoras)) {
							System.out.println("Ingrese un n�mero de horas valida");
							CantidadHoras = leer.nextInt();
						}
					}
					// SI EL DOCENTE ES CATEDRATICO CREO EL OBJETO DE LA CLASE
					// CATERATICOS
					if (TipoContratacion.equals("Catedratico") || TipoContratacion.equals("Catedr�tico")) {
						// SI EL DOCENTE ES CATEDRATICO CREO EL OBJETO DE LA
						// CLASE CATERATICOS
						Catedraticos catedratico = new Catedraticos(Nombre, Apellido, NivelEstudios, TipoContratacion,
								CantidadHoras);
						// POR MEDIO DEL OBJETO LLAMO AL METODO QUE CONTIENE EL
						// SALARIO DEL DOCENTE
						SalarioCatedraticos = catedratico.SalariosCatedra();
						// ESTE ATRIBUTO FUNCIONA COMO ACUMULADOR, ALMACENA LOS
						// SALARIOS DE TODOS LOS DOCENTES CATEDRATICOS
						SalarioCatedraticos1 = SalarioCatedraticos1 + SalarioCatedraticos;
					} else if (TipoContratacion.equals("Planta")) {
						// SI EL DOCENTE ES DE PLANTA CREO EL OBJETO DE LA CLASE
						// PLANTA
						Planta planta = new Planta(Nombre, Apellido, NivelEstudios, TipoContratacion, CantidadHoras);
						// POR MEDIO DEL OBJETO LLAMO AL METODO QUE CONTIENE EL
						// SALARIO DEL DOCENTE
						SalarioPLanta = planta.SalariosPLanta();
						// ESTE ATRIBUTO FUNCIONA COMO ACUMULADOR, ALMACENA LOS
						// SALARIOS DE TODOS LOS DOCENTES DE PLANTA
						SalarioPLanta1 = SalarioPLanta1 + SalarioPLanta;
					} else if (TipoContratacion.equals("Vinculaci�n Especial")) {
						// SI EL DOCENTE ES DE VINCULACION ESPECIAL CREO EL
						// OBJETO DE LA CLASE VINCULACION ESPECIAL
						VinculacionEspecial vinculacionespecial = new VinculacionEspecial(Nombre, Apellido,
								NivelEstudios, TipoContratacion, CantidadHoras);
						// POR MEDIO DEL OBJETO LLAMO AL METODO QUE CONTIENE EL
						// SALARIO DEL DOCENTE
						SalarioVinculacionEspecial = vinculacionespecial.SalariosVinculacion();
						// ESTE ATRIBUTO FUNCIONA COMO ACUMULADOR, ALMACENA LOS
						// SALARIOS DE TODOS LOS DOCENTES DE VINCULACION
						// ESPECIAL
						SalarioVinculacionEspecial1 = SalarioVinculacionEspecial1 + SalarioVinculacionEspecial;
					}
					// CIERRO EL FOR
				}
				// CREO UN ATRIBUTO DONDE SUMO LOS ACUMULADORES DE LOS SALARIOS
				// DE LOS DIFERENTES TIPOS DE DOCENTES
				SalarioTotal = SalarioPLanta1 + SalarioVinculacionEspecial1 + SalarioCatedraticos1;
				// IMPRIMO LOS SALARIOS DE LOS DOCENTES Y EL TOTAL A PAGAR
				System.out.println("El salario (Subtotal) que debe pagar la Universidad por los Docentes de Planta en un determinado Mes es: "
						+ SalarioPLanta1 + " COP");
				System.out.println(
						"El salario (Subtotal)  que debe pagar la Universidad por los Docentes de Vinculaci�n Especial en un determinado Mes  es: "
								+ SalarioVinculacionEspecial1 + " COP");
				System.out.println(
						"El salario (Subtotal)  que debe pagar la Universidad por los Docentes Catedr�ticos en un determinado Mes   es: "
								+ SalarioCatedraticos1 + "COP");
				System.out.println("El salario que debe pagar la Universidad por TODOS los Docentescentes en un determinado Mes  es : "
						+ SalarioTotal + " COP");
			} else {
				System.out.println("Ha ingresado un n�mero invalido de docentes.");
			}

			System.out.print("\n ");
			System.out.println("�desea repetir el programa? 1=SI ; 0=NO");
			reiniciar = leer.nextInt();
			// El siguiente While, acompa�ado de un If me permite saber si la
			// opcion ingresada es correcta, de no serlo la exigira nuevamente
			while (reiniciar != 1) {
				if (reiniciar != 0) {
					System.out.println("Ingrese un dato valido para saber si quiere repetir el programa o no :");
					reiniciar = leer.nextInt();
				} else {
					break;
				}
			}
		} while (reiniciar != 0);
		System.out.println("FIN DEL PROGRAMA");
	}

}
