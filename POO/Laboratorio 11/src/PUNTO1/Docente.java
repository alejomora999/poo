/*
 *NOMBRE DEL PROGRAMA : SALARIOS DE DOCENTES
 * AUTOR: NICOLAS MENESES GUERRERO
 *FECHA: 19 DE OCT DE 2016
 */
package PUNTO1;

//ESTA ES LA SUPERCLASE
public class Docente {
	// CREO ATRIBUTOS DE TIPO PRIVADO Y PROTEGIDO
	private String Nombre;
	private String Apellido;
	protected String NivelEstudios;
	protected String TipoContratacion;
	protected int CantidadHoras;

	// CONSTRUCTOR
	public Docente() {
		Nombre = (" ");
		Apellido = (" ");
		NivelEstudios = (" ");
		TipoContratacion = (" ");
		CantidadHoras = 0;
	}

	// SOBRECARGA DE CONSTRUCTORES
	public Docente(String Nombre, String Apellido, String NivelEstudios, String TipoContratacion, int CantidadHoras) {
		this.Nombre = Nombre;
		this.Apellido = Apellido;
		this.NivelEstudios = NivelEstudios;
		this.TipoContratacion = TipoContratacion;
		this.CantidadHoras = CantidadHoras;
	}

	// GETTERS AND SETTERS

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public String getApellido() {
		return Apellido;
	}

	public void setApellido(String apellido) {
		Apellido = apellido;
	}

	public String getNivelEstudios() {
		return NivelEstudios;
	}

	public void setNivelEstudios(String nivelEstudios) {
		NivelEstudios = nivelEstudios;
	}

	public String getTipoContratacion() {
		return TipoContratacion;
	}

	public void setTipoContratacion(String tipoContratacion) {
		TipoContratacion = tipoContratacion;
	}

	public int getCantidadHoras() {
		return CantidadHoras;
	}

	public void setCantidadHoras(int cantidadHoras) {
		CantidadHoras = cantidadHoras;
	}

}
